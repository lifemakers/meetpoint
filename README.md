PROJECT HAS BEEN MIGRATED TO https://dev.azure.com/ternovyi18/MeetPoint

### Project team:
1. Filatov Kyrylo
2. Ternovyi Ivan
3. Myroshnykova Lena
4. Plyaskin Yuriy
5. Skvortsov Mikhail

### Project resources
- [architecture diagram](https://miro.com/app/board/o9J_lMgQrcM=/)
- [additional information](https://docs.google.com/document/d/1AU_Ny3wwOMvonzhfaVTKXA_mczCfwgXlWWj1_YOkcho/edit)
- [board](https://gitlab.com/lifemakers/meetpoint/-/boards/2459268)
- [roslynator ruleset](http://pihrt.net/Roslynator/Analyzers)
- [dotNetAnalyzers ruleset](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/blob/master/StyleCop.Analyzers/StyleCop.Analyzers.CodeFixes/rulesets/StyleCopAnalyzersDefault.ruleset)
