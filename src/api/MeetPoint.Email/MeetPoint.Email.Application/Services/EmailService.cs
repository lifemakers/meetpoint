using System.Threading.Tasks;
using MeetPoint.Email.Core.Domain.Interfaces;
using MeetPoint.Email.Core.Domain.Mappings;
using MeetPoint.Email.Core.Domain.Models;
using MeetPoint.Email.Core.Domain.Models.Contracts;

namespace MeetPoint.Email.Application.Services
{
    public class EmailService : IEmailService
    {
        private readonly ISmtpClient smtpClient;
        
        public EmailService(ISmtpClient smtpClient)
        {
            this.smtpClient = smtpClient;
        }
        
        public async Task SendEmail(SendEmailDto sendEmailDto)
        {
            var sendEmailDtoValid = new ValidModel<SendEmailDto>(sendEmailDto).Value;

            await this.smtpClient.SendEmail(sendEmailDtoValid.ToSendEmailToSmtpDto());
        }
    }
}