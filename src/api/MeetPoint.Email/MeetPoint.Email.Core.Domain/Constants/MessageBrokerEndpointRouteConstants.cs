namespace MeetPoint.Email.Core.Domain.Constants
{
    public class MessageBrokerEndpointRouteConstants
    {
        public const string SendEmail = "send-email";
    }
}