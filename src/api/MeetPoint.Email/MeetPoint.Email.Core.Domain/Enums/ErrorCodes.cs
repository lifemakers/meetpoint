namespace MeetPoint.Email.Core.Domain.Enums
{
    namespace MeetPoint.Core.Domain.Enums
    {
        // TODO Change grouping of enums for http status codes.
        public static class ErrorCodes
        {
            // range from 4000 to 4099
            public enum Validation
            {
                ValidationError = 4000,
                FieldIsRequired = 4001,
            }

            // range from 5000 to 5099
            public enum Global
            {
                Unknown = 5000,
            }
        }
    }
}