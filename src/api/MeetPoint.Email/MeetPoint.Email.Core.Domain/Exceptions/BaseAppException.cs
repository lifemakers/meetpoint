using System;
using MeetPoint.Email.Core.Domain.Enums.MeetPoint.Core.Domain.Enums;
using MeetPoint.Email.Core.Domain.Extensions;

namespace MeetPoint.Email.Core.Domain.Exceptions
{
    public class BaseAppException : Exception
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.Unknown;

        public virtual int ErrorCode => (int)ErrorCodeValue;

        public BaseAppException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}