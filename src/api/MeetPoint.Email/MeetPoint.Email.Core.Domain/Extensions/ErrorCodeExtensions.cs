using MeetPoint.Email.Core.Domain.Enums.MeetPoint.Core.Domain.Enums;
using MeetPoint.Email.Core.Domain.Resources;

namespace MeetPoint.Email.Core.Domain.Extensions
{
    public static class ErrorCodeExtensions
    {
        public static string GetErrorMessage(this int errorCode)
        {
            return ErrorMessages.ResourceManager.GetString($"{errorCode}");
        }

        public static string GetErrorMessage(this int? errorCode)
        {
            return errorCode.HasValue ? ErrorMessages.ResourceManager.GetString($"{errorCode}") : string.Empty;
        }

        public static string GetErrorMessage(this ErrorCodes.Validation errorCode)
        {
            return ErrorMessages.ResourceManager.GetString($"{(int)errorCode}");
        }

        public static string GetErrorMessage(this ErrorCodes.Global errorCode)
        {
            return ErrorMessages.ResourceManager.GetString($"{(int)errorCode}");
        }
    }
}