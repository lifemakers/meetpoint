using System;

namespace MeetPoint.Email.Core.Domain.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}