using System.Threading.Tasks;
using MeetPoint.Email.Core.Domain.Models.Contracts;

namespace MeetPoint.Email.Core.Domain.Interfaces
{
    public interface IEmailService
    {
        Task SendEmail(SendEmailDto sendEmailDto);
    }
}