using System.Threading.Tasks;
using MeetPoint.Email.Core.Domain.Models.Dtos.IntegratedServicesDto;

namespace MeetPoint.Email.Core.Domain.Interfaces
{
    public interface ISmtpClient
    {
        Task SendEmail(SendEmailToSmtpDto sendEmailToSmtpDto);
    }
}