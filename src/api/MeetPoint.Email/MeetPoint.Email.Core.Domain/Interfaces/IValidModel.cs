namespace MeetPoint.Email.Core.Domain.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}