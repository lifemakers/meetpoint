using MeetPoint.Contract;
using MeetPoint.Email.Core.Domain.Models.Contracts;
using MeetPoint.Email.Core.Domain.Models.Dtos.IntegratedServicesDto;

namespace MeetPoint.Email.Core.Domain.Mappings
{
    public static class SendEmailToSmtpDtoMapping
    {
        public static SendEmailToSmtpDto ToSendEmailToSmtpDto(this SendEmailDto model)
        {
            return new SendEmailToSmtpDto
            {
                EmailsTo = model.EmailsTo,
                Subject = model.Subject,
                Text = model.Text,
            };
        }

        public static SendEmailToSmtpDto ToSendEmailToSmtpDto(this ISendEmailContract model)
        {
            return new SendEmailToSmtpDto
            {
                EmailsTo = model.EmailsTo,
                Subject = model.Subject,
                Text = model.Text,
            };
        }
    }
}