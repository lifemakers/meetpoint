using System.ComponentModel.DataAnnotations;
using MeetPoint.Contract;
using MeetPoint.Email.Core.Domain.Constants;

namespace MeetPoint.Email.Core.Domain.Models.Contracts
{
    public class SendEmailDto : ISendEmailContract
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string[] EmailsTo { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Subject { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Text { get; set; }
    }
}