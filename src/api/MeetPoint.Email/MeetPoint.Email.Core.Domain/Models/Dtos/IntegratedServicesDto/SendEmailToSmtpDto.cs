namespace MeetPoint.Email.Core.Domain.Models.Dtos.IntegratedServicesDto
{
    public class SendEmailToSmtpDto
    {
        public string[] EmailsTo { get; set; }

        public string Subject { get; set; }

        public string Text { get; set; }
    }
}