using System.Collections.Generic;

namespace MeetPoint.Email.Core.Domain.Models
{
    public class ErrorResponse
    {
        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public IEnumerable<ValidationError> ValidationErrors { get; set; }

        public string ErrorDescription { get; set; }

        public string StackTrace { get; set; }
    }
}