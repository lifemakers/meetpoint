using System;
using MeetPoint.Email.Core.Domain.Interfaces;

namespace MeetPoint.Email.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}