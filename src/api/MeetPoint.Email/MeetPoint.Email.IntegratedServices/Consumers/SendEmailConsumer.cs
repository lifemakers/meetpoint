using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Email.Core.Domain.Interfaces;
using MeetPoint.Email.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Email.IntegratedServices.Consumers
{
    public class SendEmailConsumer : IConsumer<ISendEmailContract>
    {
        private readonly ISmtpClient smtpClient;
        private readonly ILogger<SendEmailConsumer> logger;

        public SendEmailConsumer(ISmtpClient smtpClient,
            ILogger<SendEmailConsumer> logger)
        {
            this.smtpClient = smtpClient;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<ISendEmailContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@ISendEmailContract}",
                nameof(ISendEmailContract));

            var message = context.Message;

            await smtpClient.SendEmail(message.ToSendEmailToSmtpDto());
        }
    }
}