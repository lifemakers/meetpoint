using System.Threading.Tasks;
using MailKit.Security;
using MeetPoint.Email.Core.Domain.Interfaces;
using MeetPoint.Email.Core.Domain.Models.Dtos.IntegratedServicesDto;
using MeetPoint.Email.IntegratedServices.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using Serilog;

namespace MeetPoint.Email.IntegratedServices.Services
{
    public class SmtpClient : ISmtpClient
    {
        private readonly ILogger<SmtpClient> logger;
        private readonly SmtpSettings settings;
        
        public SmtpClient(IOptions<SmtpSettings> settings,
            ILogger<SmtpClient> logger)
        {
            this.settings = settings.Value;
            this.logger = logger;
        }

        public async Task SendEmail(SendEmailToSmtpDto sendEmailToSmtpDto)
        {
            // create email message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(this.settings.EmailFrom));
            email.Subject = sendEmailToSmtpDto.Subject;
            email.Body = new TextPart(TextFormat.Text) { Text = sendEmailToSmtpDto.Text };
            foreach (var emailTo in sendEmailToSmtpDto.EmailsTo)
            {
                email.To.Add(MailboxAddress.Parse(emailTo));
            }

            // send email
            using var smtp = new MailKit.Net.Smtp.SmtpClient();
            
            await smtp.ConnectAsync(this.settings.Host, this.settings.Port, SecureSocketOptions.StartTls);
            await smtp.AuthenticateAsync(this.settings.Username, this.settings.Password);
            await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
            
            this.logger
                .LogInformation("Email with subject: {@Subject} has been sent successfully to: {@EmailsTo}",
                    sendEmailToSmtpDto.Subject,
                    sendEmailToSmtpDto.EmailsTo);
        }
    }
}