namespace MeetPoint.Email.IntegratedServices.Settings
{
    public class SmtpSettings
    {
        public const string Position = "SmtpSettings";

        public string Host { get; set; }
        
        public int Port { get; set; }

        public string EmailFrom { get; set; }

        public string Username { get; set; }
        
        public string Password { get; set; }
    }
}