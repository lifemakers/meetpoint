using System.Linq;
using System.Threading.Tasks;
using MeetPoint.Email.Core.Domain.Interfaces;
using MeetPoint.Email.Core.Domain.Models;
using MeetPoint.Email.Core.Domain.Models.Contracts;
using MeetPoint.Email.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Email.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class EmailsController : Controller
    {
        private readonly ILogger<EmailsController> logger;
        private readonly IEmailService emailService;
        
        public EmailsController(ILogger<EmailsController> logger,
            IEmailService emailService)
        {
            this.logger = logger;
            this.emailService = emailService;
        }
        
        /// <summary> Send email with specified html body. For view emails, see: https://ethereal.email/login </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SendEmail(SendEmailDto sendEmailDto)
        {
            await this.emailService.SendEmail(sendEmailDto);

            return Ok();
        }
    }
}