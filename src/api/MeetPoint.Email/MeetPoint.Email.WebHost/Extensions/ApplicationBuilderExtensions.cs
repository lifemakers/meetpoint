

using System;
using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Email.Core.Domain.Constants;
using MeetPoint.Email.IntegratedServices.Consumers;
using MeetPoint.Email.IntegratedServices.Options;
using MeetPoint.Email.WebHost.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace MeetPoint.Email.WebHost.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}