using System;
using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Email.Application.Services;
using MeetPoint.Email.Core.Domain.Constants;
using MeetPoint.Email.Core.Domain.Interfaces;
using MeetPoint.Email.Core.Domain.Services;
using MeetPoint.Email.IntegratedServices.Consumers;
using MeetPoint.Email.IntegratedServices.Options;
using MeetPoint.Email.IntegratedServices.Services;
using MeetPoint.Email.IntegratedServices.Settings;
using MeetPoint.Email.WebHost.Filters;
using MeetPoint.Email.WebHost.Options;
using MeetPoint.Contract;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace MeetPoint.Email.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<SmtpSettings>(configuration.GetSection(SmtpSettings.Position));
            services.Configure<MessageBrokerOptions>(configuration.GetSection(MessageBrokerOptions.Position));
        }

        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IEmailService, EmailService>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentDateProvider, CurrentDateProvider>();
        }
        
        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddScoped<ISmtpClient, SmtpClient>();
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint Email API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = jwtAuthenticationOptions.Authority;
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                    };
                });
        }
        
        public static void AddJwtAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api.email");
                });
            });
        }

        public static void AddMassTransitServices(this IServiceCollection services)
        {
            var messageBrokerOptions = services.GetOptions<MessageBrokerOptions>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<SendEmailConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(messageBrokerOptions.ConnectionString, h =>
                    {
                        h.Username(messageBrokerOptions.Username);
                        h.Password(messageBrokerOptions.Password);
                    });

                    cfg.ReceiveEndpoint(MessageBrokerEndpointRouteConstants.SendEmail, e =>
                    {
                        e.ConfigureConsumer<SendEmailConsumer>(context);
                    });
                });
            });

            services.AddMassTransitHostedService();
        }
        
        public static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                .BuildServiceProvider()
                .GetService<IOptions<TOptions>>()
                    ?? throw new NullReferenceException(nameof(TOptions)))
                .Value;
        }
    }
}