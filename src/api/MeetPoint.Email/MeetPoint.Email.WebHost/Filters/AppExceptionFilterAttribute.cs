using System;
using MeetPoint.Email.Core.Domain.Constants;
using MeetPoint.Email.Core.Domain.Enums.MeetPoint.Core.Domain.Enums;
using MeetPoint.Email.Core.Domain.Exceptions;
using MeetPoint.Email.Core.Domain.Extensions;
using MeetPoint.Email.Core.Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Email.WebHost.Filters
{
    public class AppExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        private readonly ILogger<AppExceptionFilterAttribute> logger;
        private readonly IWebHostEnvironment hostEnvironment;

        public AppExceptionFilterAttribute(ILogger<AppExceptionFilterAttribute> logger,
            IWebHostEnvironment hostEnvironment)
        {
            this.logger = logger;
            this.hostEnvironment = hostEnvironment;
        }

        public void OnException(ExceptionContext context)
        {
            int statusCode;
            var errorResponse = new ErrorResponse();
            var isDevelopment = this.hostEnvironment.IsDevelopment();
            var exception = context.Exception;

            // TODO Add new logic for Base Exception hierarchy.
            if (exception.GetType().IsSubclassOf(typeof(BaseAppException)))
            {
                errorResponse.ErrorCode = (exception as BaseAppException).ErrorCode;
                errorResponse.ErrorMessage = exception.Message;

                if (exception is ValidationException validationException)
                {
                    errorResponse.ValidationErrors = validationException.ValidationErrors;
                }

                statusCode = StatusCodes.Status400BadRequest;
            }
            else
            {
                this.logger.LogError(exception, exception.Message);

                errorResponse.ErrorCode = (int)ErrorCodes.Global.Unknown;

                if (isDevelopment)
                {
                    errorResponse.StackTrace = exception.StackTrace;
                    errorResponse.ErrorMessage = exception.InnerException == null
                        ? exception.Message
                        : exception.Message +
                          $"{ExceptionSeparatorConstants.ErrorMessageSeparator}{exception.GetFullInnerExceptionMessage()}";
                }
                else
                {
                    errorResponse.ErrorMessage = errorResponse.ErrorCode.GetErrorMessage();
                }

                statusCode = StatusCodes.Status500InternalServerError;
            }

            context.Result = new ObjectResult(errorResponse) { StatusCode = statusCode };
            context.ExceptionHandled = true;
        }
    }
}