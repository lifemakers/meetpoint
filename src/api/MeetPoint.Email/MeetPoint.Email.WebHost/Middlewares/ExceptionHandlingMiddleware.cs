using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using MeetPoint.Email.Core.Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MeetPoint.Email.WebHost.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context,
            ILogger<ExceptionHandlingMiddleware> logger,
            IWebHostEnvironment hostEnvironment)
        {
            try
            {
                await this.next(context);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    logger.LogWarning(ex,
                        "The response has already started, the http status code middleware will not be executed.");

                    throw;
                }

                context.Response.Clear();
                context.Response.ContentType = "application/json";

                var errorResponse = new ErrorResponse();
                int statusCode;

                if (ex is ValidationException validation)
                {
                    statusCode = StatusCodes.Status422UnprocessableEntity;
                    errorResponse.ErrorMessage = validation.ValidationResult.ErrorMessage;
                }
                else
                {
                    statusCode = StatusCodes.Status500InternalServerError;

                    var isDevelopment = hostEnvironment.IsDevelopment();

                    errorResponse.ErrorMessage = isDevelopment ? ex.Message : "An error occurred during processing your request";
                    errorResponse.StackTrace = isDevelopment ? ex.StackTrace : null;
                }

                context.Response.StatusCode = statusCode;

                var serializeSettings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore,
                };
                var errorResponseText = JsonConvert.SerializeObject(errorResponse, serializeSettings);

                await context.Response.WriteAsync(errorResponseText);
            }
        }
    }
}