using MassTransit;
using MeetPoint.Email.IntegratedServices.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MeetPoint.Email.WebHost.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Serilog;

namespace MeetPoint.Email.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration,
            IHostEnvironment environment)
        {
            this.Configuration = configuration;
            this.Environment = environment;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            if (Environment.IsDevelopment())
            {
                IdentityModelEventSource.ShowPII = true;
            }

            services.AddAppConfigurations(this.Configuration);
            services.AddDomainServices();
            services.AddIntegratedServices();
            services.AddApplicationServices();
            services.AddFilters();
            services.AddOpenApiDocument();

            services.AddControllers()
                .AddMvcOptions(x => x.SuppressAsyncSuffixInActionNames = false)
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            services.AddJwtAuthentication();
            services.AddJwtAuthorization();

            services.AddMvc()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                });
            
            services.AddMassTransitServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBusControl busControl)
        {
            // app.UseAllElasticApm(this.Configuration);

            app.UseExceptionHandlingMiddleware();

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireAuthorization("ApiScope");
            });
        }
    }
}