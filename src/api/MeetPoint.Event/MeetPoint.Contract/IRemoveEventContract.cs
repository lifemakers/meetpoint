using System;

namespace MeetPoint.Contract
{
    public interface IRemoveEventContract
    {
        public Guid EventId { get; set; }
    }
}