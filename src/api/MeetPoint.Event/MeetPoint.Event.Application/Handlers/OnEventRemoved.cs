using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MeetPoint.Event.Core.Domain.DomainEvents;
using MeetPoint.Event.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Event.Core.Domain.Models.Contracts;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.Application.Handlers
{
    public class OnEventRemoved
    {
        // TODO investigate pipeline, for each db, separate handler
        public class Handler : INotificationHandler<DomainEventNotification<EventRemovedDomainEvent>>
        {
            private readonly IRemoveEventProducer removeEventProducer;
            private readonly ILogger<Handler> logger;

            public Handler(ILogger<Handler> logger,
                IRemoveEventProducer removeEventProducer)
            {
                this.logger = logger;
                this.removeEventProducer = removeEventProducer;
            }

            public async Task Handle(DomainEventNotification<EventRemovedDomainEvent> notification, CancellationToken cancellationToken)
            {
                var domainEvent = notification.DomainEvent;
                var eventId = domainEvent.EventId;

                try
                {
                    this.logger.LogDebug($"Handling Domain Event. {domainEvent}");

                    // send integration event for event removing
                    var removeEventContract = new RemoveEventContract
                    {
                        EventId = eventId,
                    };
                    await removeEventProducer.RemoveEvent(removeEventContract);
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, $"Error Handling Domain Event. {domainEvent}");
                    throw;
                }
            }
        }
    }
}