using System;
using System.Threading.Tasks;
using MediatR;
using MeetPoint.Event.Core.Domain.Interfaces;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.Application
{
    public class MediatrDomainEventDispatcher : IDomainEventDispatcher
    {
        private readonly IMediator mediator;
        private readonly ILogger<MediatrDomainEventDispatcher> logger;

        public MediatrDomainEventDispatcher(IMediator mediator, ILogger<MediatrDomainEventDispatcher> logger)
        {
            this.mediator = mediator;
            this.logger = logger;
        }

        public async Task Dispatch(IDomainEvent domainEvent)
        {
            var domainEventNotification = this.CreateDomainEventNotification(domainEvent);
            logger.LogDebug("Dispatching Domain Event as MediatR notification.  EventType: {eventType}", domainEvent.GetType());
            await mediator.Publish(domainEventNotification);
        }

        private INotification CreateDomainEventNotification(IDomainEvent domainEvent)
        {
            var genericDispatcherType = typeof(DomainEventNotification<>).MakeGenericType(domainEvent.GetType());
            return (INotification)Activator.CreateInstance(genericDispatcherType, domainEvent);
        }
    }
}