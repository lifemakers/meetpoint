using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Aggregates.Event;
using MeetPoint.Event.Core.Domain.Exceptions.Event;
using MeetPoint.Event.Core.Domain.Interfaces;
using MeetPoint.Event.Core.Domain.Interfaces.Repositories;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Mappings;
using MeetPoint.Event.Core.Domain.Models;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Application.Services
{
    public class EventService : IEventService
    {
        private readonly IEventRepository eventRepository;
        private readonly ICurrentDateProvider currentDateProvider;

        public EventService(IEventRepository eventRepository,
            ICurrentDateProvider currentDateProvider)
        {
            this.eventRepository = eventRepository;
            this.currentDateProvider = currentDateProvider;
        }

        public async Task<IEnumerable<EventForListDto>> GetAllEventsAsync()
        {
            // TODO implement pagination model (probably with pagination token)
            return await this.eventRepository.GetAllEventsAsync();
        }

        public async Task<EventDto> CreateEventAsync(CreateEventDto createEventDto, Guid userId)
        {
            var @event = new Core.Domain.Aggregates.Event.Event(new ValidModel<CreateEventDto>(createEventDto),
                this.currentDateProvider.GetDate());

            await this.eventRepository.CreateAsync(@event);

            await this.eventRepository.SaveChangesAsync();

            var eventDtoResponse = @event.ToEventDto();

            return eventDtoResponse;
        }

        public async Task<EventDto> GetEventByIdAsync(Guid id)
        {
            var @event = await this.eventRepository.GetEventByIdAsync(id);

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            var eventDtoResponse = @event.ToEventDto();

            return eventDtoResponse;
        }

        public async Task EditEventAsync(Guid eventId, EditEventDto editEventDto)
        {
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            @event.Edit(new ValidModel<EditEventDto>(editEventDto),
                this.currentDateProvider.GetDate());

            await this.eventRepository.UpdateAsync(@event);

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task DeleteEventAsync(Guid eventId)
        {
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            await this.eventRepository.DeleteAsync(@event);

            @event.PublishEventRemovedDomainEvent();

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task AddTagAsync(Guid eventId, AddEventTagDto addEventTagDto)
        {
            var tagId = addEventTagDto.TagId;
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);
            var eventTag = new EventTag(new ValidModel<AddEventTagDto>(addEventTagDto),
                this.currentDateProvider.GetDate());

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            if (@event.ContainsTag(tagId))
            {
                throw new EventTagAlreadyExistsException();
            }

            @event.AddTag(eventTag);

            await this.eventRepository.UpdateAsync(@event);

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task RemoveTagAsync(Guid eventId, Guid tagId)
        {
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            var removed = !@event.RemoveTag(tagId);

            if (!removed)
            {
                throw new EventTagsHaveNotBeenDeletedException();
            }

            await this.eventRepository.UpdateAsync(@event);

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task AddParticipationAsync(CreateEventParticipationDto createEventParticipationDto)
        {
            var eventId = createEventParticipationDto.EventId;
            var participationId = createEventParticipationDto.ParticipationId;
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);
            var eventParticipation = new EventParticipation(new ValidModel<CreateEventParticipationDto>(createEventParticipationDto),
                this.currentDateProvider.GetDate());

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            if (@event.ContainsParticipation(participationId))
            {
                throw new EventParticipationsAlreadyExistsException();
            }

            @event.AddParticipation(eventParticipation);

            await this.eventRepository.UpdateAsync(@event);

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task RemoveParticipationAsync(Guid eventId, Guid participationId)
        {
            var @event = await this.eventRepository.GetEventByIdAsync(eventId);

            if (@event == null)
            {
                throw new EventWasNotFoundException();
            }

            var removed = !@event.RemoveParticipation(participationId);

            if (!removed)
            {
                throw new EventParticipationHasNotBeenDeletedException();
            }

            await this.eventRepository.UpdateAsync(@event);

            await this.eventRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<EventTagForListDto>> GetAllTagsAsync(Guid eventId)
        {
            return await this.eventRepository.GetAllTagsAsync(eventId);
        }

        public async Task<IEnumerable<EventParticipationForListDto>> GetAllParticipationsAsync(Guid eventId)
        {
            return await this.eventRepository.GetAllParticipationsAsync(eventId);
        }
    }
}