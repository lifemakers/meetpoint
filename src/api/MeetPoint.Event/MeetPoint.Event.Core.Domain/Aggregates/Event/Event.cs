﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeetPoint.Event.Core.Domain.DomainEvents;
using MeetPoint.Event.Core.Domain.Interfaces;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using Newtonsoft.Json;

namespace MeetPoint.Event.Core.Domain.Aggregates.Event
{
    public class Event : DomainEntity, IAggregateRoot
    {
        private readonly List<EventTag> eventTags = new List<EventTag>();

        private readonly List<EventParticipation> eventParticipations = new List<EventParticipation>();

        public string Title { get; private set; }

        public string Description { get; private set; }

        public DateTime BeginDate { get; private set; }

        public DateTime? EndDate { get; private set; }

        [JsonIgnore]
        public virtual IEnumerable<EventTag> EventTags => this.eventTags;

        [JsonIgnore]
        public virtual IEnumerable<EventParticipation> EventParticipations => this.eventParticipations;

        public Event(IValidModel<CreateEventDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = this.CreateNewId();
            this.Title = validModel.Value.Title;
            this.Description = validModel.Value.Description;
            this.BeginDate = validModel.Value.BeginDate;
            this.EndDate = validModel.Value.EndDate;
            this.DatePosted = currentDate;
            this.DateLastUpdated = currentDate;
        }

        protected Event()
        {
        }

        public void Edit(IValidModel<EditEventDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Title = validModel.Value.Title;
            this.Description = validModel.Value.Description;
            this.BeginDate = validModel.Value.BeginDate;
            this.EndDate = validModel.Value.EndDate;
            this.DateLastUpdated = currentDate;
        }

        public void AddTag(EventTag eventTag)
        {
            this.eventTags.Add(eventTag);
        }

        public bool ContainsTag(Guid tagId)
        {
            return this.eventTags
                .FirstOrDefault(x => x.Id == tagId) != null;
        }

        public bool RemoveTag(Guid tagId)
        {
            var count = this.eventTags
                .RemoveAll(x => x.Id == tagId);

            return count == 1;
        }

        public void AddParticipation(EventParticipation eventParticipation)
        {
            this.eventParticipations.Add(eventParticipation);
        }

        public bool ContainsParticipation(Guid participationId)
        {
            return this.eventParticipations
                .FirstOrDefault(x => x.Id == participationId) != null;
        }

        public bool RemoveParticipation(Guid participationId)
        {
            var count = this.eventParticipations
                .RemoveAll(x => x.Id == participationId);

            return count == 1;
        }

        public void PublishEventRemovedDomainEvent()
        {
            this.PublishEvent(new EventRemovedDomainEvent(this.Id));
        }
    }
}