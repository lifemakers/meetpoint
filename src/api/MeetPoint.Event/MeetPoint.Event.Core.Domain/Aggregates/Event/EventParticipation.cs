using System;
using MeetPoint.Event.Core.Domain.Interfaces;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Aggregates.Event
{
    public class EventParticipation : DomainEntity
    {
        public Guid EventId { get; private set; }

        public Guid UserId { get; private set; }

        public ParticipationRole ParticipationRole { get; private set; }

        public EventParticipation(IValidModel<CreateEventParticipationDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = validModel.Value.ParticipationId;
            this.EventId = validModel.Value.EventId;
            this.ParticipationRole = Enum.Parse<ParticipationRole>(validModel.Value.ParticipationRole);
            this.UserId = validModel.Value.UserId;

            this.DatePosted = currentDate;
            this.DateLastUpdated = currentDate;
        }

        protected EventParticipation()
        {
        }
    }
}