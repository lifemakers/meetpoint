using System;
using MeetPoint.Event.Core.Domain.Interfaces;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using Newtonsoft.Json;

namespace MeetPoint.Event.Core.Domain.Aggregates.Event
{
    public class EventTag : DomainEntity
    {
        public Guid EventId { get; set; }

        public string TagName { get; private set; }

        public EventTag(IValidModel<AddEventTagDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = validModel.Value.TagId;
            this.TagName = validModel.Value.TagName;

            this.DatePosted = currentDate;
            this.DateLastUpdated = currentDate;
        }

        protected EventTag()
        {
        }
    }
}