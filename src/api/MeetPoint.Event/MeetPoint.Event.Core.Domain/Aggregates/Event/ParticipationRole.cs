namespace MeetPoint.Event.Core.Domain.Aggregates.Event
{
    public enum ParticipationRole
    {
        Participant = 0,
        Moderator = 1,
        Owner = 2,
    }
}