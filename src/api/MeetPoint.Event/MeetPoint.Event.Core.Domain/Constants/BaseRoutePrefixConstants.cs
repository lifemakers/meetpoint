namespace MeetPoint.Event.Core.Domain.Constants
{
    public static class BaseRoutePrefixConstants
    {
        public const string Events = "api/v1/events";

        public const string Users = "api/v1/users";
    }
}