namespace MeetPoint.Event.Core.Domain.Constants
{
    public static class CacheKeyConstants
    {
        private const string Separator = ":";
        private const string ServiceName = "MeetPoint.Event";

        public const string GetEventById = ServiceName + Separator + "GetEventById";
        public const string GetAllEventTags = ServiceName + Separator + "GetAllEventTags";
        public const string GetAllParticipations = ServiceName + Separator + "GetAllParticipations";
    }
}