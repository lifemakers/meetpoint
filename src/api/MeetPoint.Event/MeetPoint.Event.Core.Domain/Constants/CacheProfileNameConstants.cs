namespace MeetPoint.Event.Core.Domain.Constants
{
    public static class CacheProfileNameConstants
    {
        public const string GetAllEventTags = "GetAllEventTags";
        public const string GetEventById = "GetEventById";
        public const string GetAllParticipations = "GetAllParticipations";
    }
}