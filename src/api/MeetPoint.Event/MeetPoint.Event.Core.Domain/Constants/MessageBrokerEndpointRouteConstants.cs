namespace MeetPoint.Event.Core.Domain.Constants
{
    public class MessageBrokerEndpointRouteConstants
    {
        public const string SendEmail = "send-email";
        public const string CreateEventParticipation = "create-event-participation";
        public const string CreateEventTag = "create-event-tag";
        public const string CreateUserImage = "create-user-image";
        public const string CreateUserParticipation = "create-user-participation";
        public const string RemoveEventParticipation = "remove-event-participation";
        public const string RemoveEventTag = "remove-event-tag";
        public const string RemoveEvent = "remove-event";
        public const string RemoveUserImage = "remove-user-image";
        public const string RemoveUserParticipation = "remove-user-participation";
    }
}