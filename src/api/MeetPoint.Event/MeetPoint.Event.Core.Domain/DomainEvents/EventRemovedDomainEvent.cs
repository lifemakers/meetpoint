using System;
using MeetPoint.Event.Core.Domain.Interfaces;

namespace MeetPoint.Event.Core.Domain.DomainEvents
{
    public class EventRemovedDomainEvent : IDomainEvent
    {
        public Guid EventId { get; private set; }

        public EventRemovedDomainEvent(Guid eventId)
        {
            this.EventId = eventId;
        }

        public override string ToString()
        {
            return $"Type: {this.GetType().Name}," +
                   $"EventId: {this.EventId}";
        }
    }
}