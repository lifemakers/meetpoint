using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventParticipationHasNotBeenDeletedException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventParticipationHasNotBeenDeleted;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventParticipationHasNotBeenDeletedException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventParticipationHasNotBeenDeletedException(string message)
            : base(message)
        {
        }
    }
}