using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventParticipationsAlreadyExistsException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventParticipationAlreadyExists;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventParticipationsAlreadyExistsException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventParticipationsAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}