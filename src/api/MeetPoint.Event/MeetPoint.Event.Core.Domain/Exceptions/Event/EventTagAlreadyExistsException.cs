using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventTagAlreadyExistsException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventTagAlreadyExists;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventTagAlreadyExistsException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventTagAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}