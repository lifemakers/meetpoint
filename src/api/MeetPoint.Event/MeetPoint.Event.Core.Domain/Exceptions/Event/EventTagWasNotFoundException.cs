using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventTagWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventTagWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventTagWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventTagWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}