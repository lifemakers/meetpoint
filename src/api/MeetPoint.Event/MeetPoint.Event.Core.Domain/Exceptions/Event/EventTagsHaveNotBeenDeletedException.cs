using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventTagsHaveNotBeenDeletedException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventTagsHaveNotBeenDeleted;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventTagsHaveNotBeenDeletedException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventTagsHaveNotBeenDeletedException(string message)
            : base(message)
        {
        }
    }
}