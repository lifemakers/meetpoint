using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class EventWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Event ErrorCodeValue = ErrorCodes.Event.EventWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public EventWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public EventWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}