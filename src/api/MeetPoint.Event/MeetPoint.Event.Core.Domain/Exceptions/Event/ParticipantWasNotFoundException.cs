using MeetPoint.Event.Core.Domain.Enums;
using MeetPoint.Event.Core.Domain.Extensions;

namespace MeetPoint.Event.Core.Domain.Exceptions.Event
{
    public class ParticipationWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Participation ErrorCodeValue = ErrorCodes.Participation.ParticipationWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public ParticipationWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public ParticipationWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}