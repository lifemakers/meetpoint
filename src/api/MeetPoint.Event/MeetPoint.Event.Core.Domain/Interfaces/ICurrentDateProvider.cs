using System;

namespace MeetPoint.Event.Core.Domain.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}