using System;
using System.Collections.Concurrent;

namespace MeetPoint.Event.Core.Domain.Interfaces
{
    public interface IDomainEntity
    {
        Guid Id { get; }

        DateTime DatePosted { get; }

        DateTime DateLastUpdated { get; }

        IProducerConsumerCollection<IDomainEvent> DomainEvents { get; }
    }
}