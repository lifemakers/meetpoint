using System.Threading.Tasks;

namespace MeetPoint.Event.Core.Domain.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}