namespace MeetPoint.Event.Core.Domain.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}