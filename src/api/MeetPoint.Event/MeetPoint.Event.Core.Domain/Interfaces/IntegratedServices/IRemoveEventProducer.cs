using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Models.Contracts;

namespace MeetPoint.Event.Core.Domain.Interfaces.IntegratedServices
{
    public interface IRemoveEventProducer
    {
        Task RemoveEvent(RemoveEventContract removeEventContract);
    }
}