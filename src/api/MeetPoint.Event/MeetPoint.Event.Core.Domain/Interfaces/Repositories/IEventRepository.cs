using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Aggregates.Event;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Interfaces.Repositories
{
    public interface IEventRepository : IRepository<Aggregates.Event.Event>
    {
        Task<IEnumerable<EventForListDto>> GetAllEventsAsync();

        Task<IEnumerable<EventParticipationForListDto>> GetAllParticipationsAsync(Guid eventId);

        Task<IEnumerable<EventTagForListDto>> GetAllTagsAsync(Guid eventId);
    }
}