using System;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Aggregates;

namespace MeetPoint.Event.Core.Domain.Interfaces.Repositories
{
    public interface IRepository<T>
        where T : DomainEntity
    {
        Task<T> GetEventByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task SaveChangesAsync();
    }
}