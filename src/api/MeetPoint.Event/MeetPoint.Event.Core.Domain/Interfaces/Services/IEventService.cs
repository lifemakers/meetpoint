using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Interfaces.Services
{
    public interface IEventService
    {
        Task<IEnumerable<EventForListDto>> GetAllEventsAsync();

        Task<EventDto> CreateEventAsync(CreateEventDto createEventDto, Guid userId);

        Task<EventDto> GetEventByIdAsync(Guid id);

        Task EditEventAsync(Guid eventId, EditEventDto editEventDto);

        Task DeleteEventAsync(Guid eventId);

        Task AddTagAsync(Guid eventId, AddEventTagDto addEventTagDto);

        Task RemoveTagAsync(Guid eventId, Guid tagId);

        Task AddParticipationAsync(CreateEventParticipationDto createEventParticipationDto);

        Task RemoveParticipationAsync(Guid eventId, Guid participationId);

        Task<IEnumerable<EventTagForListDto>> GetAllTagsAsync(Guid eventId);

        Task<IEnumerable<EventParticipationForListDto>> GetAllParticipationsAsync(Guid eventId);
    }
}