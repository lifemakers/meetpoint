using MeetPoint.Contract;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Mappings
{
    public static class CreateEventParticipationDtoMapping
    {
        public static CreateEventParticipationDto ToCreateEventParticipationDto(this ICreateEventParticipationContract model)
        {
            return new CreateEventParticipationDto
            {
                EventId = model.EventId,
                ParticipationId = model.ParticipationId,
                ParticipationRole = model.ParticipationRole,
                UserId = model.UserId,
            };
        }
    }
}