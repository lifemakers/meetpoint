using System.Collections.Generic;
using System.Linq;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Mappings
{
    public static class EventDtoMapping
    {
        public static IEnumerable<EventDto> ToEventDto(this List<Aggregates.Event.Event> events)
        {
            var eventDtos = new List<EventDto>();

            foreach (var @event in events)
            {
                eventDtos.Add(@event.ToEventDto());
            }

            return eventDtos;
        }

        public static EventDto ToEventDto(this Aggregates.Event.Event @event)
        {
            return new EventDto
            {
                Id = @event.Id,
                Title = @event.Title,
                Description = @event.Description,
                BeginDate = @event.BeginDate,
                EndDate = @event.EndDate,
                TagIds = @event.EventTags.Select(x => x.Id),
            };
        }
    }
}