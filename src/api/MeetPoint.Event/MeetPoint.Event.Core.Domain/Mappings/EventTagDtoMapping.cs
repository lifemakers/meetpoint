using System.Collections.Generic;
using MeetPoint.Event.Core.Domain.Aggregates.Event;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Mappings
{
    public static class EventTagDtoMapping
    {
        public static IEnumerable<EventTagForListDto> ToEventTagToListDto(this IEnumerable<EventTag> eventTags)
        {
            var tagDtos = new List<EventTagForListDto>();

            foreach (var eventTag in eventTags)
            {
                tagDtos.Add(eventTag.ToEventTagToListDto());
            }

            return tagDtos;
        }

        public static EventTagForListDto ToEventTagToListDto(this EventTag eventTag)
        {
            return new EventTagForListDto
            {
                TagName = eventTag.TagName,
            };
        }
    }
}