using System.Collections.Generic;
using MeetPoint.Event.Core.Domain.Aggregates.Event;
using MeetPoint.Event.Core.Domain.Models.Dtos;

namespace MeetPoint.Event.Core.Domain.Mappings
{
    public static class ParticipationDtoMapping
    {
        public static List<EventParticipationDto> ToEventParticipationDto(this List<EventParticipation> eventParticipations)
        {
            var eventParticipationDtos = new List<EventParticipationDto>();

            foreach (var eventParticipation in eventParticipations)
            {
                eventParticipationDtos.Add(eventParticipation.ToEventParticipationDto());
            }

            return eventParticipationDtos;
        }

        public static EventParticipationDto ToEventParticipationDto(this EventParticipation eventParticipation)
        {
            return new EventParticipationDto
            {
                EventId = eventParticipation.EventId,
                UserId = eventParticipation.UserId,
                ParticipationId = eventParticipation.Id,
                ParticipationRole = eventParticipation.ParticipationRole.ToString(),
                DatePosted = eventParticipation.DatePosted,
            };
        }
    }
}