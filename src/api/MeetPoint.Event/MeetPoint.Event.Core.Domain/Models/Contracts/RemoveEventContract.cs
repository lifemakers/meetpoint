using System;

namespace MeetPoint.Event.Core.Domain.Models.Contracts
{
    public class RemoveEventContract : Contract.IRemoveEventContract
    {
        public Guid EventId { get; set; }
    }
}