using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.Event.Core.Domain.Constants;

namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class AddEventTagDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid TagId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string TagName { get; set; }
    }
}