using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.Event.Core.Domain.Constants;

namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class CreateEventParticipationDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid EventId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid ParticipationId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid UserId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string ParticipationRole { get; set; }
    }
}