using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.Event.Core.Domain.Constants;

namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class EditEventDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Title { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Description { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public DateTime BeginDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}