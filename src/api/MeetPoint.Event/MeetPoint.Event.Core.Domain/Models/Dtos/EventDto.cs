using System;
using System.Collections.Generic;

namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class EventDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public IEnumerable<Guid> TagIds { get; set; }
    }
}