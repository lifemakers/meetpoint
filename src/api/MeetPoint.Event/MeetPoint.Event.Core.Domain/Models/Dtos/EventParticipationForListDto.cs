using System;

namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class EventParticipationForListDto
    {
        public Guid ParticipationId { get; set; }

        public Guid UserId { get; set; }

        public Guid EventId { get; set; }

        public DateTime DatePosted { get; set; }

        public string ParticipationRole { get; set; }
    }
}