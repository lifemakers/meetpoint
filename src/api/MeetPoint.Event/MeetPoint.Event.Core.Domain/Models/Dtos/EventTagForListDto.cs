namespace MeetPoint.Event.Core.Domain.Models.Dtos
{
    public class EventTagForListDto
    {
        public string TagName { get; set; }
    }
}