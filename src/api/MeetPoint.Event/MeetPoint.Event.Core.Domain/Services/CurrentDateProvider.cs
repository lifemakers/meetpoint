using System;
using MeetPoint.Event.Core.Domain.Interfaces;

namespace MeetPoint.Event.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}