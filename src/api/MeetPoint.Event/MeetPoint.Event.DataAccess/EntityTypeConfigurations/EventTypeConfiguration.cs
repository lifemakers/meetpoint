﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MeetPoint.Event.DataAccess.EntityTypeConfigurations
{
    public class EventTypeConfiguration : IEntityTypeConfiguration<Core.Domain.Aggregates.Event.Event>
    { // TODO to end db schema configuration
        public void Configure(EntityTypeBuilder<Core.Domain.Aggregates.Event.Event> builder)
        {
            builder
                .Property(c => c.Title)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(c => c.Description)
                .HasMaxLength(400)
                .IsRequired();

            builder
                .HasMany(x => x.EventTags)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany(x => x.EventParticipations)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}