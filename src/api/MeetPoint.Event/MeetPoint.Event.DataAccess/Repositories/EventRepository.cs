using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Interfaces.Repositories;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.Event.DataAccess.Repositories
{
    public class EventRepository : Repository<Core.Domain.Aggregates.Event.Event>, IEventRepository
    {
        public EventRepository(DataContext dataContext, ICacheService cacheService)
            : base(dataContext, cacheService)
        {
        }

        public override async Task<Core.Domain.Aggregates.Event.Event> GetEventByIdAsync(Guid id)
        {
            var cacheKey = CacheKeyConstants.GetEventById;
            var cached = this.CacheService.Get<Core.Domain.Aggregates.Event.Event>(cacheKey);

            if (cached != null)
            {
                return cached;
            }
            else
            {
                var result = await this.DataContext.Set<Core.Domain.Aggregates.Event.Event>().FirstOrDefaultAsync(x => x.Id == id);

                return this.CacheService
                    .Set(cacheKey, result, CacheProfileNameConstants.GetEventById);
            }
        }

        public async Task<IEnumerable<EventForListDto>> GetAllEventsAsync()
        {
            return await this.DataContext
                .Events
                .AsNoTracking()
                .Select(x => new EventForListDto
                {
                    Id = x.Id,
                    Title = x.Title,
                    BeginDate = x.BeginDate,
                    EndDate = x.EndDate,
                    TagIds = x.EventTags.Select(x => x.Id),
                })
                .OrderByDescending(x => x.Id)
                .ToListAsync();
        }

        public async Task<IEnumerable<EventParticipationForListDto>> GetAllParticipationsAsync(Guid eventId)
        {
            var cacheKey = CacheKeyConstants.GetAllParticipations;
            var cached = this.CacheService.Get<IEnumerable<EventParticipationForListDto>>(cacheKey);

            if (cached != null)
            {
                return cached;
            }
            else
            {
                var result = await this.DataContext
                    .EventParticipations
                    .AsNoTracking()
                    .Where(x => x.EventId == eventId)
                    .Select(x => new EventParticipationForListDto
                    {
                        ParticipationId = x.Id,
                        UserId = x.UserId,
                        EventId = x.EventId,
                        DatePosted = x.DatePosted,
                        ParticipationRole = x.ParticipationRole.ToString(),
                    })
                    .OrderByDescending(x => x.DatePosted)
                    .ToListAsync();

                return this.CacheService
                    .Set<IEnumerable<EventParticipationForListDto>>(cacheKey, result, CacheProfileNameConstants.GetAllParticipations);
            }
        }

        public async Task<IEnumerable<EventTagForListDto>> GetAllTagsAsync(Guid eventId)
        {
            var cacheKey = CacheKeyConstants.GetAllEventTags;
            var cached = this.CacheService.Get<IEnumerable<EventTagForListDto>>(cacheKey);

            if (cached != null)
            {
                return cached;
            }
            else
            {
                var result = await this.DataContext
                    .EventTags
                    .AsNoTracking()
                    .Where(x => x.EventId == eventId)
                    .Select(x => new EventTagForListDto
                    {
                        TagName = x.TagName,
                    })
                    .ToListAsync();

                return this.CacheService
                    .Set<IEnumerable<EventTagForListDto>>(cacheKey, result, CacheProfileNameConstants.GetAllEventTags);
            }
        }
    }
}