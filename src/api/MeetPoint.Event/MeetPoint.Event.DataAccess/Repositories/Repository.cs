using System;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Aggregates;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Interfaces.Repositories;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.Event.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : DomainEntity
    {
        protected DataContext DataContext { get; private set; }

        protected ICacheService CacheService { get; private set; }

        public Repository(DataContext dataContext, ICacheService cacheService)
        {
            this.DataContext = dataContext;
            this.CacheService = cacheService;
        }

        public virtual async Task<T> GetEventByIdAsync(Guid id)
        {
            return await this.DataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task CreateAsync(T item)
        {
            await this.DataContext.Set<T>().AddAsync(item);
        }

        public async Task UpdateAsync(T item)
        {
            this.DataContext.Set<T>().Update(item);
        }

        public async Task DeleteAsync(T item)
        {
            this.DataContext.Set<T>().Remove(item);
        }

        public async Task SaveChangesAsync()
        {
            await this.DataContext.SaveChangesAsync();
        }
    }
}