using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.IntegratedServices.Consumers
{
    public class CreateEventParticipationConsumer : IConsumer<ICreateEventParticipationContract>
    {
        private readonly IEventService eventService;
        private readonly ILogger<CreateEventParticipationConsumer> logger;

        public CreateEventParticipationConsumer(IEventService eventService,
            ILogger<CreateEventParticipationConsumer> logger)
        {
            this.eventService = eventService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<ICreateEventParticipationContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@ICreateEventParticipationContract}",
                nameof(ICreateEventParticipationContract));

            var message = context.Message;

            await this.eventService.AddParticipationAsync(message.ToCreateEventParticipationDto());
        }
    }
}