using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.IntegratedServices.Consumers
{
    public class RemoveEventParticipationConsumer : IConsumer<IRemoveEventParticipationContract>
    {
        private readonly IEventService eventService;
        private readonly ILogger<RemoveEventParticipationConsumer> logger;

        public RemoveEventParticipationConsumer(IEventService eventService,
            ILogger<RemoveEventParticipationConsumer> logger)
        {
            this.eventService = eventService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<IRemoveEventParticipationContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@IRemoveEventParticipationContract}",
                nameof(IRemoveEventParticipationContract));

            var message = context.Message;

            await this.eventService.RemoveParticipationAsync(message.EventId, message.ParticipationId);
        }
    }
}