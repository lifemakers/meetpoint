using System;
using System.Linq;
using MeetPoint.Event.IntegratedServices.Options;

namespace MeetPoint.Event.IntegratedServices.Extensions
{
    public static class RedisOptionsExtensions
    {
        public static CacheProfileOptions GetCacheProfileOptions(this RedisOptions options, string name) =>
            options
                .CacheProfiles
                .SingleOrDefault(x => x.Name.Equals(name))
            ?? throw new ArgumentException(nameof(name));
    }
}