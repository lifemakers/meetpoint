namespace MeetPoint.Event.IntegratedServices.Options
{
    public class CacheProfileOptions
    {
        public string Name { get; set; }

        public int? AbsoluteExpirationSeconds { get; set; }

        public int? SlidingExpiration { get; set; }
    }
}