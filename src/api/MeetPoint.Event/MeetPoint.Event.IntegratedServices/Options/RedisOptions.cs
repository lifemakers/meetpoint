using System.Collections.Generic;

namespace MeetPoint.Event.IntegratedServices.Options
{
    public class RedisOptions
    {
        public const string Position = "Redis";

        public string ConnectionString { get; set; }

        public IEnumerable<CacheProfileOptions> CacheProfiles { get; set; }
    }
}