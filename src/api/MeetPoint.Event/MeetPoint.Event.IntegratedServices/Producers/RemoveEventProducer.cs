using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Event.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using RemoveEventContract = MeetPoint.Event.Core.Domain.Models.Contracts.RemoveEventContract;

namespace MeetPoint.Event.IntegratedServices.Producers
{
    public class RemoveEventProducer : IRemoveEventProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<RemoveEventProducer> logger;

        public RemoveEventProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<RemoveEventProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task RemoveEvent(RemoveEventContract removeEventContract)
        {
            this.logger.LogInformation("Sending command for removing event");

            await sendEndpointProvider.Send<Contract.IRemoveEventContract>(removeEventContract);

            this.logger.LogInformation("Sent command successfully for removing event");
        }
    }
}