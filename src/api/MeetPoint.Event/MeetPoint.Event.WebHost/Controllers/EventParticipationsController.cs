using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Models;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using MeetPoint.Event.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.WebHost.Controllers
{
    /// <summary> Event Participations Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Events)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class EventParticipationsController : Controller
    {
        private readonly IEventService eventService;

        public EventParticipationsController(IEventService eventService)
        {
            this.eventService = eventService;
        }

        [HttpGet("{eventId:Guid}/participations")]
        [ProducesResponseType(typeof(IEnumerable<EventParticipationForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<EventParticipationForListDto>>> GetAllParticipationsAsync(Guid eventId)
        {
            return Ok(await this.eventService.GetAllParticipationsAsync(eventId));
        }
    }
}