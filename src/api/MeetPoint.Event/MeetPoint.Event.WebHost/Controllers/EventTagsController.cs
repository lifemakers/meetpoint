using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Exceptions.Event;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Models;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using MeetPoint.Event.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.WebHost.Controllers
{
    /// <summary> Event Tags Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Events)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class EventTagsController : Controller
    {
        private readonly IEventService eventService;
        private readonly ILogger<EventTagsController> logger;

        public EventTagsController(IEventService eventService,
            ILogger<EventTagsController> logger)
        {
            this.eventService = eventService;
            this.logger = logger;
        }

        [HttpGet("{eventId:Guid}/tags")]
        [ProducesResponseType(typeof(IEnumerable<EventTagForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<EventTagForListDto>>> GetAllEventTagsAsync(Guid eventId)
        {
            var tags = await this.eventService.GetAllTagsAsync(eventId);

            return Ok(tags);
        }

        /// <summary> Add tag to event. </summary>
        [HttpPost("{eventId:Guid}/tags")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<EventDto>> AddEventTagAsync(Guid eventId, AddEventTagDto request)
        {
            this.logger.LogInformation("eventId: {@EventId} AddEventTagDto: {@AddEventTagDto} requested for event tag adding",
                eventId,
                request);

            try
            {
                await this.eventService.AddTagAsync(eventId, request);
            }
            catch (EventWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.Ok();
        }

        /// <summary> Remove tag from event. </summary>
        [HttpDelete("{eventId:Guid}/tags/{tagId:Guid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RemoveEventTagAsync(Guid eventId, Guid tagId)
        {
            this.logger.LogInformation("eventId: {@EventId}, tagId: {@TagId} requested for event tag removing",
                eventId,
                tagId);

            try
            {
                await this.eventService.RemoveTagAsync(eventId, tagId);
            }
            catch (EventWasNotFoundException)
            {
                return NotFound();
            }

            return this.NoContent();
        }
    }
}