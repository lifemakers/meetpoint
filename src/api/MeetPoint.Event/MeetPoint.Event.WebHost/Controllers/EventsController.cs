using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Exceptions.Event;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Models;
using MeetPoint.Event.Core.Domain.Models.Dtos;
using MeetPoint.Event.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Event.WebHost.Controllers
{
    /// <summary> Events Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Events)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class EventsController : Controller
    {
        private readonly IEventService eventService;
        private readonly ILogger<EventsController> logger;

        public EventsController(IEventService eventService,
            ILogger<EventsController> logger)
        {
            this.eventService = eventService;
            this.logger = logger;
        }

        /// <summary> Get all events. </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<EventForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<EventForListDto>>> GetAllEventsAsync()
        {
            var response = await this.eventService.GetAllEventsAsync();

            return this.Ok(response);
        }

        /// <summary> Get event by id. </summary>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(typeof(EventDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EventDto>> GetEventByIdAsync(Guid id)
        {
            try
            {
                return this.Ok(await this.eventService.GetEventByIdAsync(id));
            }
            catch (EventWasNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary> Create new event. </summary>
        [HttpPost]
        [ProducesResponseType(typeof(EventDto), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<EventDto>> CreateEventAsync([FromForm] CreateEventDto request)
        {
            this.logger.LogInformation("CreateEventDto: {@CreateEventDto} requested for event creating", request);

            var userId = Guid.NewGuid();

            var response = await this.eventService.CreateEventAsync(request, userId);

            this.logger.LogInformation("EventDto: {@EventDto} response after event created", response);

            return CreatedAtAction(nameof(GetEventByIdAsync), new { id = response.Id }, response.Id);
        }

        /// <summary> Update event. </summary>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EditEventAsync(Guid id, [FromForm] EditEventDto request)
        {
            this.logger.LogInformation("EditEventDto: {@EditEventDto} requested for event editing", request);

            try
            {
                await this.eventService.EditEventAsync(id, request);
            }
            catch (EventWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.Ok();
        }

        /// <summary> Delete event. </summary>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteEventAsync(Guid id)
        {
            this.logger.LogInformation("DeleteEvent ParticipationId: {@id} requested for event deleting", id);

            try
            {
                await this.eventService.DeleteEventAsync(id);
            }
            catch (EventWasNotFoundException)
            {
                return NotFound();
            }

            return this.NoContent();
        }
    }
}