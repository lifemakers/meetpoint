using MeetPoint.Event.WebHost.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace MeetPoint.Event.WebHost.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}