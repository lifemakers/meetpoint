using System;
using System.Reflection;
using MassTransit;
using MediatR;
using MeetPoint.Contract;
using MeetPoint.Event.Application;
using MeetPoint.Event.Application.Services;
using MeetPoint.Event.Core.Domain.Constants;
using MeetPoint.Event.Core.Domain.Interfaces;
using MeetPoint.Event.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Event.Core.Domain.Interfaces.Repositories;
using MeetPoint.Event.Core.Domain.Interfaces.Services;
using MeetPoint.Event.Core.Domain.Services;
using MeetPoint.Event.DataAccess;
using MeetPoint.Event.DataAccess.Data;
using MeetPoint.Event.DataAccess.Repositories;
using MeetPoint.Event.IntegratedServices.Consumers;
using MeetPoint.Event.IntegratedServices.Options;
using MeetPoint.Event.IntegratedServices.Producers;
using MeetPoint.Event.IntegratedServices.Services;
using MeetPoint.Event.WebHost.Filters;
using MeetPoint.Event.WebHost.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace MeetPoint.Event.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccessServices(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>((provider, builder) =>
            {
                var options = provider.GetRequiredService<IOptions<DatabaseOptions>>().Value;

                builder.UseNpgsql(options.ConnectionString);
                builder.UseLazyLoadingProxies();
            });

            services.AddScoped<IEfDbInitializer, EfDbInitializer>();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IEventRepository, EventRepository>();
        }

        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IDomainEventDispatcher, MediatrDomainEventDispatcher>();
            services.AddMediatR(typeof(MediatrDomainEventDispatcher).GetTypeInfo().Assembly);

            services.AddScoped<IEventService, EventService>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentDateProvider, CurrentDateProvider>();
        }

        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddTransient<IRemoveEventProducer, RemoveEventProducer>();
            services.AddScoped<ICacheService, CacheService>();
        }

        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStringDefaultName = AppSettingsConstants.ConnectionStrings.DefaultConnection;
            var connectionStringDefault = configuration.GetConnectionString(connectionStringDefaultName);

            services.Configure<DatabaseOptions>(x => x.ConnectionString = connectionStringDefault);
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<MessageBrokerOptions>(configuration.GetSection(MessageBrokerOptions.Position));
            services.Configure<RedisOptions>(configuration.GetSection(RedisOptions.Position));
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint Event API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = jwtAuthenticationOptions.Authority;
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                    };
                });
        }

        public static void AddJwtAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api.event");
                });
            });
        }

        public static void AddMassTransitServices(this IServiceCollection services)
        {
            var messageBrokerOptions = services.GetOptions<MessageBrokerOptions>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CreateEventParticipationConsumer>();
                x.AddConsumer<RemoveEventParticipationConsumer>();

                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(messageBrokerOptions.ConnectionString, h =>
                    {
                        h.Username(messageBrokerOptions.Username);
                        h.Password(messageBrokerOptions.Password);
                    });

                    var removeEventUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.RemoveEvent}");

                    EndpointConvention.Map<IRemoveEventContract>(removeEventUri);

                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        public static void AddRedisDistributedCache(this IServiceCollection services)
        {
            var redisOptions = services.GetOptions<RedisOptions>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = redisOptions.ConnectionString;
            });
        }

        private static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                .BuildServiceProvider()
                .GetService<IOptions<TOptions>>()
                    ?? throw new NullReferenceException(nameof(TOptions)))
                .Value;
        }
    }
}