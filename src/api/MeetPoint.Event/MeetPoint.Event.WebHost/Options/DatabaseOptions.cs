namespace MeetPoint.Event.WebHost.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}