using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Aggregates.Image;
using MeetPoint.File.Core.Domain.Exceptions;
using MeetPoint.File.Core.Domain.Extensions.PostConfigureActions;
using MeetPoint.File.Core.Domain.Interfaces;
using MeetPoint.File.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.File.Core.Domain.Interfaces.Repositories;
using MeetPoint.File.Core.Domain.Interfaces.Services;
using MeetPoint.File.Core.Domain.Mappings;
using MeetPoint.File.Core.Domain.Models;
using MeetPoint.File.Core.Domain.Models.Dtos;

namespace MeetPoint.File.Application.Services
{
    public class ImageService : IImageService
    {
        private readonly IImageRepository imageRepository;
        private readonly IImageUploadService imageUploadService;
        private readonly ICurrentDateProvider currentDateProvider;

        public ImageService(IImageRepository imageRepository,
            IImageUploadService imageUploadService,
            ICurrentDateProvider currentDateProvider)
        {
            this.imageRepository = imageRepository;
            this.imageUploadService = imageUploadService;
            this.currentDateProvider = currentDateProvider;
        }

        /// <summary> Create a new image. </summary>
        public async Task<ImageDto> CreateImageAsync(CreateImageDto createImageDto)
        {
            var image = new Image(new ValidModel<CreateImageDto>(createImageDto),
                this.currentDateProvider.GetDate());

            // create image
            await this.imageRepository.CreateAsync(image);

            var imageForUploadDto = createImageDto.ToImageForUploadDto();
            await this.imageUploadService.UploadImageAsync(imageForUploadDto);

            // commit transaction
            await this.imageRepository.SaveChangesAsync();

            // prepare response
            var imageResponse = image
                .ToImageDto()
                .PostConfigure(i => i.Url = this.imageUploadService.BuildUrl(i.FileName));

            return imageResponse;
        }

        public async Task<ImageDto> GetImageByIdAsync(Guid id)
        {
            // get user by id
            var image = await this.imageRepository.GetByIdAsync(id);

            if (image == null)
            {
                throw new ImageWasNotFoundException();
            }

            // prepare response
            var imageResponse = image
                .ToImageDto()
                .PostConfigure(i => i.Url = this.imageUploadService.BuildUrl(i.FileName));

            return imageResponse;
        }

        public async Task DeleteImageAsync(Guid imageId)
        {
            var image = await this.imageRepository.GetByIdAsync(imageId);

            if (image == null)
            {
                throw new ImageWasNotFoundException();
            }

            await this.imageRepository.DeleteAsync(image);

            await this.imageRepository.SaveChangesAsync();
        }
    }
}