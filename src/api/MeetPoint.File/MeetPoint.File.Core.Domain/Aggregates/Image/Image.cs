using System;
using System.IO;
using MeetPoint.File.Core.Domain.Interfaces;
using MeetPoint.File.Core.Domain.Models.Dtos;

namespace MeetPoint.File.Core.Domain.Aggregates.Image
{
    public class Image : DomainEntity
    {
        public string FileName { get; private set; }

        public string Extension { get; private set; }

        public string ContentType { get; private set; }

        public long SizeByte { get; private set; }

        public DateTime DatePosted { get; private set; }

        public Image(IValidModel<CreateImageDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            var image = validModel.Value.Image;

            this.Id = this.CreateNewId();
            this.FileName = image.FileName;
            this.ContentType = image.ContentType;
            this.SizeByte = image.Length;
            this.DatePosted = currentDate;
            this.Extension = Path.GetExtension(image.FileName);
        }

        protected Image()
        {
        }
    }
}