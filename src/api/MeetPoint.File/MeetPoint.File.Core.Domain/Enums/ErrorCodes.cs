namespace MeetPoint.File.Core.Domain.Enums
{
    // TODO Change grouping of enums for http status codes.
    public static class ErrorCodes
    {
        // range from 4000 to 4099
        public enum Validation
        {
            ValidationError = 4000,
            FieldIsRequired = 4001,
        }

        // range from 4100 to 4199
        public enum User
        {
            UserWasNotFound = 4100,
        }

        // range from 4200 to 4299
        public enum Tag
        {
            TagWasNotFound = 4200,
        }

        // range from 4300 to 4399
        public enum Event
        {
            EventWasNotFound = 4300,
            EventTagWasNotFound = 4301,
            EventTagsHaveNotBeenDeleted = 4302,
            ParticipationWasNotFound = 4303,
            EventTagAlreadyExists = 4304,
        }

        // range from 4400 to 4499
        public enum Image
        {
            ImageWasNotFound = 4400,
        }

        // range from 5000 to 5099
        public enum Global
        {
            Unknown = 5000,
            InvalidTakeCount = 5001,
        }
    }
}