using MeetPoint.File.Core.Domain.Enums;
using MeetPoint.File.Core.Domain.Extensions;

namespace MeetPoint.File.Core.Domain.Exceptions
{
    public class ImageWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Image ErrorCodeValue = ErrorCodes.Image.ImageWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public ImageWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public ImageWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}