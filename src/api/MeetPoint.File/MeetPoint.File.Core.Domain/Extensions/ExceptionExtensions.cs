using System;
using System.Collections.Generic;
using MeetPoint.File.Core.Domain.Constants;

namespace MeetPoint.File.Core.Domain.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetFullInnerExceptionMessage(this Exception exception)
        {
            var errorMessages = new List<string>();
            var innerException = exception;

            while (innerException.InnerException != null)
            {
                errorMessages.Add(innerException.InnerException.Message);
                innerException = innerException.InnerException;
            }

            return string.Join(ExceptionSeparatorConstants.ErrorMessageSeparator, errorMessages);
        }
    }
}