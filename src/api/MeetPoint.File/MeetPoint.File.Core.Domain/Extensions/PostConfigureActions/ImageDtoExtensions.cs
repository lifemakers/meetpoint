using System;
using MeetPoint.File.Core.Domain.Models.Dtos;

namespace MeetPoint.File.Core.Domain.Extensions.PostConfigureActions
{
    public static class ImageDtoExtensions
    {
        public static ImageDto PostConfigure(this ImageDto imageDto,
            Action<ImageDto> configureAction)
        {
            configureAction(imageDto);

            return imageDto;
        }
    }
}