using System;

namespace MeetPoint.File.Core.Domain.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}