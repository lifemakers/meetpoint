using System;
using System.Collections.Concurrent;

namespace MeetPoint.File.Core.Domain.Interfaces
{
    public interface IDomainEntity
    {
        Guid Id { get; }

        DateTime DatePost { get; }

        DateTime DateLastUpdated { get; }

        IProducerConsumerCollection<IDomainEvent> DomainEvents { get; }
    }
}