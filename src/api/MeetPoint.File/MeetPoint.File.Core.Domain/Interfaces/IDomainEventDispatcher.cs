using System.Threading.Tasks;

namespace MeetPoint.File.Core.Domain.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}