namespace MeetPoint.File.Core.Domain.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}