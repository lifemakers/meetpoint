using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Models.Dtos.IntegratedServicesDto;

namespace MeetPoint.File.Core.Domain.Interfaces.IntegratedServices
{
    public interface IImageUploadService
    {
        Task UploadImagesAsync(IEnumerable<ImageForUploadDto> images);

        Task UploadImageAsync(ImageForUploadDto image);

        string BuildUrl(string fileName);
    }
}