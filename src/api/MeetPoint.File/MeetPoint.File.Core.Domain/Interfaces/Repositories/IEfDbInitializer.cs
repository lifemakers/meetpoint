namespace MeetPoint.File.Core.Domain.Interfaces.Repositories
{
    public interface IEfDbInitializer
    {
        void Initialize();

        void Clean();
    }
}