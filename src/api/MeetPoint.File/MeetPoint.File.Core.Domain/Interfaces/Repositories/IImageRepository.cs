using MeetPoint.File.Core.Domain.Aggregates.Image;

namespace MeetPoint.File.Core.Domain.Interfaces.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {
    }
}