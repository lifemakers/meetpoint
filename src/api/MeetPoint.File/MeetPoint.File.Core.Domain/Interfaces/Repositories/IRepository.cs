using System;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Aggregates;

namespace MeetPoint.File.Core.Domain.Interfaces.Repositories
{
    public interface IRepository<T>
        where T : DomainEntity
    {
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task SaveChangesAsync();
    }
}