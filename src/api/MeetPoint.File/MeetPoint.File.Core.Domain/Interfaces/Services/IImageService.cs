using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Models.Dtos;

namespace MeetPoint.File.Core.Domain.Interfaces.Services
{
    public interface IImageService
    {
        Task<ImageDto> CreateImageAsync(CreateImageDto createImageDto);

        Task<ImageDto> GetImageByIdAsync(Guid id);

        Task DeleteImageAsync(Guid imageId);
    }
}