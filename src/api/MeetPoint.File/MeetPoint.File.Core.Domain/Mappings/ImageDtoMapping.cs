using System.Collections.Generic;
using MeetPoint.File.Core.Domain.Aggregates.Image;
using MeetPoint.File.Core.Domain.Models.Dtos;

namespace MeetPoint.File.Core.Domain.Mappings
{
    public static class ImageDtoMapping
    {
        public static ImageDto ToImageDto(this Image image)
        {
            return new ImageDto
            {
                Id = image.Id,
                FileName = image.FileName,
            };
        }

        public static List<ImageDto> ToImageDto(this IEnumerable<Image> userImages)
        {
            var result = new List<ImageDto>();

            foreach (var userImage in userImages)
            {
                result.Add(userImage.ToImageDto());
            }

            return result;
        }
    }
}