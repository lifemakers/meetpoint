using System.Collections.Generic;
using MeetPoint.File.Core.Domain.Models.Dtos;
using MeetPoint.File.Core.Domain.Models.Dtos.IntegratedServicesDto;
using Microsoft.AspNetCore.Http;

namespace MeetPoint.File.Core.Domain.Mappings
{
    public static class ImageForUploadDtoMapping
    {
        public static ImageForUploadDto ToImageForUploadDto(this CreateImageDto createImageDto)
        {
            return new ImageForUploadDto
            {
                FileName = createImageDto.Image.FileName,
                ContentType = createImageDto.Image.ContentType,
                File = createImageDto.Image.OpenReadStream(),
            };
        }
    }
}