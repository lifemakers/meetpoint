using System.ComponentModel.DataAnnotations;
using MeetPoint.File.Core.Domain.Constants;
using Microsoft.AspNetCore.Http;

namespace MeetPoint.File.Core.Domain.Models.Dtos
{
    public class CreateImageDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public IFormFile Image { get; set; }
    }
}