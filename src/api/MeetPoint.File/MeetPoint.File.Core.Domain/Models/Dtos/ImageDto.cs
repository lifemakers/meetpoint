using System;

namespace MeetPoint.File.Core.Domain.Models.Dtos
{
    public class ImageDto
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public string Url { get; set; }
    }
}