using System.IO;

namespace MeetPoint.File.Core.Domain.Models.Dtos.IntegratedServicesDto
{
    public class ImageForUploadDto
    {
        public Stream File { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }
    }
}