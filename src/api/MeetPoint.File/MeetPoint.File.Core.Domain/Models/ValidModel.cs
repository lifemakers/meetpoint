using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using MeetPoint.File.Core.Domain.Enums;
using MeetPoint.File.Core.Domain.Extensions;
using MeetPoint.File.Core.Domain.Interfaces;
using ValidationException = MeetPoint.File.Core.Domain.Exceptions.ValidationException;

namespace MeetPoint.File.Core.Domain.Models
{
    /// <summary>
    /// Throws ValidationException if TModel does not pass validation when initiate.
    /// </summary>
    public class ValidModel<TModel> : IValidModel<TModel>
    {
        private readonly List<ValidationError> validationErrors = new List<ValidationError>();

        public TModel Value { get; private set; }

        public ValidModel(TModel model)
        {
            this.validationErrors.AddRange(this.Validate(model));

            this.ThrowIfNotValid();

            this.Value = model;
        }

        /// <summary>
        /// Validate model by data annotations attributes.
        /// </summary>
        private IEnumerable<ValidationError> Validate(object obj)
        {
            var errors = new List<ValidationError>();
            var results = new List<ValidationResult>();

            if (!Validator.TryValidateObject(obj, new ValidationContext(obj), results, true))
            {
                results.ForEach(i =>
                {
                    var errorCodePattern = string.Join("|", Enum.GetNames(typeof(ErrorCodes.Validation)));
                    var errorCodeMatch = Regex.Match(i.ErrorMessage ?? string.Empty, errorCodePattern, RegexOptions.IgnoreCase);

                    var errorCode = errorCodeMatch.Success
                        ? (int)(ErrorCodes.Validation)Enum.Parse(typeof(ErrorCodes.Validation), errorCodeMatch.Value, true)
                        : default(int?);

                    var errorMessage = errorCodeMatch.Success
                        ? errorCode.GetErrorMessage()
                        : i.ErrorMessage;

                    errors.Add(new ValidationError(errorCode, i.MemberNames.First(), errorMessage));
                });
            }

            return errors;
        }

        /// <summary>
        /// Throws ValidationException in case if have any validation errors.
        /// </summary>
        private void ThrowIfNotValid()
        {
            if (this.validationErrors.Any())
            {
                throw new ValidationException(this.validationErrors);
            }
        }
    }
}