using System;
using MeetPoint.File.Core.Domain.Interfaces;

namespace MeetPoint.File.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}