using MeetPoint.File.Core.Domain.Interfaces.Repositories;

namespace MeetPoint.File.DataAccess.Data
{
    public class EfDbInitializer : IEfDbInitializer
    {
        private readonly DataContext dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Initialize()
        {
            this.dataContext.Database.EnsureDeleted();
            this.dataContext.Database.EnsureCreated();

            this.dataContext.SaveChanges();
        }

        public void Clean()
        {
            this.dataContext.Database.EnsureDeleted();
        }
    }
}