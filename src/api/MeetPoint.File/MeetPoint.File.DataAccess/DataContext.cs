using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Aggregates.Image;
using MeetPoint.File.Core.Domain.Interfaces;
using MeetPoint.File.DataAccess.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.File.DataAccess
{
    public class DataContext : DbContext
    {
        private readonly IDomainEventDispatcher dispatcher;

        public DbSet<Image> Images { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options,
            IDomainEventDispatcher dispatcher)
            : base(options)
        {
            this.dispatcher = dispatcher;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ImageTypeConfiguration());
        }

        public override int SaveChanges()
        {
            this.PreSaveChanges().GetAwaiter().GetResult();
            var res = base.SaveChanges();
            return res;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await this.PreSaveChanges();
            var res = await base.SaveChangesAsync(cancellationToken);
            return res;
        }

        private async Task PreSaveChanges()
        {
            await this.DispatchDomainEvents();
        }

        private async Task DispatchDomainEvents()
        {
            var domainEventEntities = ChangeTracker.Entries<IDomainEntity>()
                .Select(po => po.Entity)
                .Where(po => po.DomainEvents.Any())
                .ToArray();

            foreach (var entity in domainEventEntities)
            {
                IDomainEvent dev;
                while (entity.DomainEvents.TryTake(out dev))
                {
                    await dispatcher.Dispatch(dev);
                }
            }
        }
    }
}