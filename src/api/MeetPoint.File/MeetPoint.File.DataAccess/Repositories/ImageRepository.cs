using MeetPoint.File.Core.Domain.Aggregates.Image;
using MeetPoint.File.Core.Domain.Interfaces.Repositories;

namespace MeetPoint.File.DataAccess.Repositories
{
    public class ImageRepository : Repository<Image>, IImageRepository
    {
        protected DataContext DataContext { get; private set; }

        public ImageRepository(DataContext dataContext) : base(dataContext)
        {
            this.DataContext = dataContext;
        }
    }
}