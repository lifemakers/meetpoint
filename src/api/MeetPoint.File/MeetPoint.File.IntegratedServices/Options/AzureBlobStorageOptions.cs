namespace MeetPoint.File.IntegratedServices.Options
{
    public class AzureBlobStorageOptions
    {
        public const string Position = "AzureBlobStorage";

        public string BaseUrl { get; set; }

        public string ContainerName { get; set; }

        public string ConnectionString { get; set; }
    }
}