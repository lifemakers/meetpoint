using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using MeetPoint.File.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.File.Core.Domain.Models.Dtos.IntegratedServicesDto;
using MeetPoint.File.IntegratedServices.Options;
using Microsoft.Extensions.Options;
using Microsoft.VisualBasic;

namespace MeetPoint.File.IntegratedServices.Services
{
    public class ImageUploadService : IImageUploadService
    {
        private readonly AzureBlobStorageOptions options;

        public ImageUploadService(IOptions<AzureBlobStorageOptions> options)
        {
            this.options = options.Value;
        }

        public async Task UploadImagesAsync(IEnumerable<ImageForUploadDto> images)
        {
            var container = new BlobContainerClient(this.options.ConnectionString, this.options.ContainerName);
            await container.CreateIfNotExistsAsync(PublicAccessType.Blob);

            foreach (var image in images)
            {
                await this.UploadImageAsync(image, container);
            }
        }

        public async Task UploadImageAsync(ImageForUploadDto image)
        {
            await this.UploadImagesAsync(new List<ImageForUploadDto> { image });
        }

        private async Task UploadImageAsync(ImageForUploadDto imageFor, BlobContainerClient container)
        {
            var blobName = imageFor.FileName;
            var blob = container.GetBlobClient(blobName);
            var blobUploadOptions = new BlobUploadOptions()
            {
                HttpHeaders = new BlobHttpHeaders
                {
                    ContentType = imageFor.ContentType,
                },
            };

            await blob.UploadAsync(imageFor.File, blobUploadOptions);
        }

        public string BuildUrl(string fileName)
        {
            return Path.Combine(this.options.BaseUrl, this.options.ContainerName, fileName);
        }
    }
}