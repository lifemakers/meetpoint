using System;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Constants;
using MeetPoint.File.Core.Domain.Exceptions;
using MeetPoint.File.Core.Domain.Interfaces.Services;
using MeetPoint.File.Core.Domain.Models;
using MeetPoint.File.Core.Domain.Models.Dtos;
using MeetPoint.File.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.File.WebHost.Controllers
{
    /// <summary> Images Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Files)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class ImagesController : Controller
    {
        private readonly IImageService imageService;
        private readonly ILogger<ImagesController> logger;

        public ImagesController(IImageService imageService,
            ILogger<ImagesController> logger)
        {
            this.imageService = imageService;
            this.logger = logger;
        }

        /// <summary> Get image by id. </summary>
        [HttpGet("images/{id:Guid}")]
        [ProducesResponseType(typeof(ImageDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ImageDto>> GetImageByIdAsync(Guid id)
        {
            var response = await this.imageService.GetImageByIdAsync(id);

            return this.Ok(response);
        }

        /// <summary> Create new image. </summary>
        [HttpPost("images")]
        [ProducesResponseType(typeof(ImageDto), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ImageDto>> CreateImageAsync([FromForm] CreateImageDto request)
        {
            this.logger.LogInformation("CreateImageDto: {@CreateImageDto} requested for image creating", request);

            var response = await this.imageService.CreateImageAsync(request);

            this.logger.LogInformation("ImageDto: {@ImageDto} response after image created", response);

            return this.CreatedAtAction(nameof(this.GetImageByIdAsync), new { id = response.Id }, response.Id);
        }

        /// <summary> Delete image. </summary>
        [HttpDelete("images/{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteImageAsync(Guid id)
        {
            this.logger.LogInformation("DeleteImage Id: {@id} requested for image deleting", id);

            try
            {
                await this.imageService.DeleteImageAsync(id);
            }
            catch (ImageWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.NoContent();
        }
    }
}