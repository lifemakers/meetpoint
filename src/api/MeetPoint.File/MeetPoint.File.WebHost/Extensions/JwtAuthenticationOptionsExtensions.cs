using System;
using System.Linq;
using MeetPoint.File.WebHost.Options;

namespace MeetPoint.File.WebHost.Extensions
{
    public static class JwtAuthenticationOptionsExtensions
    {
        public static ClientOptions GetClientOptions(this JwtAuthenticationOptions options, string clientName) =>
            options
                .Clients
                .SingleOrDefault(x => x.ClientName.Equals(clientName))
            ?? throw new ArgumentException(nameof(clientName));
    }
}