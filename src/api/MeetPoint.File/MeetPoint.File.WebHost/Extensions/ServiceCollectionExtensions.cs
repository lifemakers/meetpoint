using System;
using System.Reflection;
using MassTransit;
using MediatR;
using MeetPoint.File.Application;
using MeetPoint.File.Application.Services;
using MeetPoint.File.Core.Domain.Constants;
using MeetPoint.File.Core.Domain.Interfaces;
using MeetPoint.File.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.File.Core.Domain.Interfaces.Repositories;
using MeetPoint.File.Core.Domain.Interfaces.Services;
using MeetPoint.File.Core.Domain.Services;
using MeetPoint.File.DataAccess;
using MeetPoint.File.DataAccess.Data;
using MeetPoint.File.DataAccess.Repositories;
using MeetPoint.File.IntegratedServices.Options;
using MeetPoint.File.IntegratedServices.Services;
using MeetPoint.File.WebHost.Filters;
using MeetPoint.File.WebHost.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace MeetPoint.File.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccessServices(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>((provider, builder) =>
            {
                var options = provider.GetRequiredService<IOptions<DatabaseOptions>>().Value;

                builder.UseNpgsql(options.ConnectionString);
                builder.UseLazyLoadingProxies();
            });

            services.AddScoped<IEfDbInitializer, EfDbInitializer>();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IImageRepository, ImageRepository>();
        }

        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IDomainEventDispatcher, MediatrDomainEventDispatcher>();
            services.AddMediatR(typeof(MediatrDomainEventDispatcher).GetTypeInfo().Assembly);

            services.AddScoped<IImageService, ImageService>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentDateProvider, CurrentDateProvider>();
        }

        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddScoped<IImageUploadService, ImageUploadService>();
        }

        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStringDefaultName = AppSettingsConstants.ConnectionStrings.DefaultConnection;
            var connectionStringDefault = configuration.GetConnectionString(connectionStringDefaultName);

            services.Configure<DatabaseOptions>(x => x.ConnectionString = connectionStringDefault);
            services.Configure<AzureBlobStorageOptions>(configuration.GetSection(AzureBlobStorageOptions.Position));
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<MessageBrokerOptions>(configuration.GetSection(MessageBrokerOptions.Position));
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint File API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = jwtAuthenticationOptions.Authority;
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                    };
                });
        }

        public static void AddJwtAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api.file");
                });
            });
        }

        public static void AddMassTransitServices(this IServiceCollection services)
        {
            var messageBrokerOptions = services.GetOptions<MessageBrokerOptions>();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(messageBrokerOptions.ConnectionString, h =>
                    {
                        h.Username(messageBrokerOptions.Username);
                        h.Password(messageBrokerOptions.Password);
                    });

                    cfg.ConfigureEndpoints(context);

                    // var sendEmailUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.SendEmail}");

                    // EndpointConvention.Map<SendEmailContract>(sendEmailUri);
                });
            });

            services.AddMassTransitHostedService();
        }

        private static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                .BuildServiceProvider()
                .GetService<IOptions<TOptions>>()
                    ?? throw new NullReferenceException(nameof(TOptions)))
                .Value;
        }
    }
}