namespace MeetPoint.File.WebHost.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}