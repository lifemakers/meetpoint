using System.Collections.Generic;

namespace MeetPoint.File.WebHost.Options
{
    public class JwtAuthenticationOptions
    {
        public const string Position = "JwtAuthentication";

        public string Authority { get; set; }

        public IEnumerable<ClientOptions> Clients { get; set; }
    }
}