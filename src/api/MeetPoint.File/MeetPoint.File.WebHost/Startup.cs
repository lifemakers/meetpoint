using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.File.Core.Domain.Interfaces.Repositories;
using MeetPoint.File.WebHost.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Serilog;

namespace MeetPoint.File.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration,
            IHostEnvironment environment)
        {
            this.Configuration = configuration;
            this.Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAppConfigurations(this.Configuration);
            services.AddDataAccessServices();
            services.AddDomainServices();
            services.AddApplicationServices();
            services.AddIntegratedServices();
            services.AddFilters();
            services.AddOpenApiDocument();

            services.AddControllers()
                .AddMvcOptions(x => x.SuppressAsyncSuffixInActionNames = false)
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            services.AddJwtAuthentication();
            services.AddJwtAuthorization();

            services.AddMvc()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                });

            // services.AddMassTransitServices();
        }

        public void Configure(IApplicationBuilder app, IEfDbInitializer efDbInitializer)
        {
            // app.UseAllElasticApm(this.Configuration);

            if (this.Environment.IsDevelopment())
            {
                IdentityModelEventSource.ShowPII = true;
            }

            app.UseExceptionHandlingMiddleware();

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseStaticFiles();

            app.UseRouting();

            // TODO move out to the separate extension method
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(_ => true)
                .AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireAuthorization("ApiScope");
            });

            efDbInitializer.Initialize();
        }
    }
}