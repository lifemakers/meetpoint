namespace MeetPoint.Gateway.Core.Domain.Constants
{
    public static class ClientNameConstants
    {
        public const string Interactive = "Interactive";
        public const string M2MEmail = "Machine2Machine.Email";
        public const string M2MFile = "Machine2Machine.File";
        public const string M2MTag = "Machine2Machine.Tag";
        public const string M2MEvent = "Machine2Machine.Event";
        public const string M2MUser = "Machine2Machine.User";
        public const string M2MParticipation = "Machine2Machine.Participation";
    }
}