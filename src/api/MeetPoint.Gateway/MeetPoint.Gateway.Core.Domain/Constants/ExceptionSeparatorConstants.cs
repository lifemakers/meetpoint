namespace MeetPoint.Gateway.Core.Domain.Constants
{
    public static class ExceptionSeparatorConstants
    {
        public const string ErrorMessageSeparator = " --> ";
    }
}