namespace MeetPoint.Gateway.Core.Domain.Constants
{
    public static class HttpClientNameConstants
    {
        public const string EmailHttpClient = "EmailHttpClient";
        public const string FileHttpClient = "FileHttpClient";
        public const string TagHttpClient = "TagHttpClient";
        public const string EventHttpClient = "EventHttpClient";
        public const string UserHttpClient = "UserHttpClient";
        public const string ParticipationHttpClient = "ParticipationHttpClient";
    }
}