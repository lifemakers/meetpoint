namespace MeetPoint.Gateway.Core.Domain.Constants
{
    public static class RouteSegmentConstants
    {
        public const string File = "/api/v1/files";
        public const string Tag = "/api/v1/tags";
        public const string Event = "/api/v1/events";
        public const string User = "/api/v1/users";
        public const string Participation = "/api/v1/participations";
    }
}