using System;
using MeetPoint.Gateway.Core.Domain.Enums;
using MeetPoint.Gateway.Core.Domain.Extensions;

namespace MeetPoint.Gateway.Core.Domain.Exceptions
{
    public class BaseAppException : Exception
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.Unknown;

        public virtual int ErrorCode => (int)ErrorCodeValue;

        public BaseAppException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}