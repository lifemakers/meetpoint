using System.Collections.Generic;

namespace MeetPoint.Gateway.Core.Domain.Models
{
    public class ErrorResponseExtended : ErrorResponse
    {
        public IEnumerable<ValidationError> ValidationErrors { get; set; }
    }
}