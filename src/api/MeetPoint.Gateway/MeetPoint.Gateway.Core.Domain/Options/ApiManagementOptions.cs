using System.Collections.Generic;

namespace MeetPoint.Gateway.Core.Domain.Options
{
    public class ApiManagementOptions
    {
        public const string Position = "ApiManagement";

        public IEnumerable<HttpClientOptions> HttpClients { get; set; }
    }
}