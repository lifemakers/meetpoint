namespace MeetPoint.Gateway.Core.Domain.Options
{
    public class ClientOptions
    {
        public string ClientName { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string SignedOutCallbackPath { get; set; }
    }
}