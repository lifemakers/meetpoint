using System.Collections.Generic;

namespace MeetPoint.Gateway.Core.Domain.Options
{
    public class JwtAuthenticationOptions
    {
        public const string Position = "JwtAuthentication";

        public string Authority { get; set; }

        public IEnumerable<ClientOptions> Clients { get; set; }
    }
}