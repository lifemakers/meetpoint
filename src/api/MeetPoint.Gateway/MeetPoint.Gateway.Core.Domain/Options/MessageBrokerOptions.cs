namespace MeetPoint.Gateway.Core.Domain.Options
{
    public class MessageBrokerOptions
    {
        public const string Position = "MessageBroker";

        public string MessageBrokerConnectionString { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}