using System.Threading.Tasks;
using MeetPoint.Gateway.Core.Domain.Models;
using MeetPoint.Gateway.WebHost.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MeetPoint.Gateway.WebHost.Controllers
{
    [ApiController]
    [Route("")]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class AccountController : Controller
    {
        /// <summary> Logout user. </summary>
        [HttpGet("logout")]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Logout()
        {
            return SignOut(CookieAuthenticationDefaults.AuthenticationScheme, OpenIdConnectDefaults.AuthenticationScheme);
        }
    }
}