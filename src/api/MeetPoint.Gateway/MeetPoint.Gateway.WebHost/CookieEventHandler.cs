using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using MeetPoint.Gateway.Core.Domain.Constants;
using MeetPoint.Gateway.Core.Domain.Options;
using MeetPoint.Gateway.WebHost.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ClientOptions = MeetPoint.Gateway.Core.Domain.Options.ClientOptions;

namespace MeetPoint.Gateway.WebHost
{
    public class CookieEventHandler : CookieAuthenticationEvents
    {
        private readonly JwtAuthenticationOptions jwtAuthenticationOptions;
        private readonly ClientOptions clientOptions;
        private readonly ILogger<CookieEventHandler> logger;

        public CookieEventHandler(IOptions<JwtAuthenticationOptions> options,
            ILogger<CookieEventHandler> logger)
        {
            this.jwtAuthenticationOptions = options.Value;
            this.clientOptions = this.jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.Interactive);
            this.logger = logger;
        }

        // After the auth cookie has been validated, this event is called.
        // In it we see if the access token is close to expiring.  If it is
        // then we use the refresh token to get a new access token and save them.
        // If the refresh token does not work for some reason then we redirect to
        // the login screen.
        public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            var now = DateTimeOffset.UtcNow;
            var expiresAt = context.Properties.GetTokenValue("expires_at");
            var accessTokenExpiration = DateTimeOffset.Parse(expiresAt);
            var timeRemaining = accessTokenExpiration.Subtract(now);

            if (timeRemaining.TotalMilliseconds < 0)
            {
                var refreshToken = context.Properties.GetTokenValue("refresh_token");

                // TODO: Get this HttpClientOptions from a factory
                var response = await new HttpClient().RequestRefreshTokenAsync(new RefreshTokenRequest
                {
                    Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                    ClientId = clientOptions.ClientId,
                    ClientSecret = clientOptions.ClientSecret,
                    RefreshToken = refreshToken,
                });

                if (!response.IsError)
                {
                    var expiresInSeconds = response.ExpiresIn;
                    var updatedExpiresAt = DateTimeOffset.UtcNow.AddSeconds(expiresInSeconds);
                    context.Properties.UpdateTokenValue("expires_at", updatedExpiresAt.ToString());
                    context.Properties.UpdateTokenValue("access_token", response.AccessToken);
                    context.Properties.UpdateTokenValue("refresh_token", response.RefreshToken);

                    // Indicate to the cookie middleware that the cookie should be remade (since we have updated it)
                    context.ShouldRenew = true;
                }
                else
                {
                    context.RejectPrincipal();
                    await context.HttpContext.SignOutAsync();
                }
            }
        }

        public override async Task SigningOut(CookieSigningOutContext context)
        {
            // TODO as part of token management lib. Does not work with this code
            // revoke refresh token on sign-out
            // await context.HttpContext.RevokeUserRefreshTokenAsync();
        }
    }
}