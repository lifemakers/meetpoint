using System;
using System.Linq;
using MeetPoint.Gateway.Core.Domain.Options;

namespace MeetPoint.Gateway.WebHost.Extensions
{
    public static class ApiManagementOptionsExtensions
    {
        public static HttpClientOptions GetHttpClientOptions(this ApiManagementOptions options, string clientName) =>
            options
                .HttpClients
                .SingleOrDefault(x => x.ClientName.Equals(clientName))
            ?? throw new ArgumentException(nameof(clientName));
    }
}