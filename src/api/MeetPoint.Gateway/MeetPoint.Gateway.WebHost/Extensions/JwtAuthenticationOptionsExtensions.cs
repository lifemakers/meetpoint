using System;
using System.Linq;
using MeetPoint.Gateway.Core.Domain.Options;

namespace MeetPoint.Gateway.WebHost.Extensions
{
    public static class JwtAuthenticationOptionsExtensions
    {
        public static ClientOptions GetClientOptions(this JwtAuthenticationOptions options, string clientName) =>
            options
                .Clients
                .SingleOrDefault(x => x.ClientName.Equals(clientName))
            ?? throw new ArgumentException(nameof(clientName));
    }
}