using System;
using System.IdentityModel.Tokens.Jwt;
using IdentityModel;
using IdentityModel.Client;
using MeetPoint.Gateway.Core.Domain.Constants;
using MeetPoint.Gateway.Core.Domain.Options;
using MeetPoint.Gateway.WebHost.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Polly;

namespace MeetPoint.Gateway.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddCookieEventHandler(this IServiceCollection services)
        {
            services.AddTransient<CookieEventHandler>();
        }

        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<ApiManagementOptions>(configuration.GetSection(ApiManagementOptions.Position));
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint Gateway API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();
            var clientInteractiveOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.Interactive);
            var clientM2MFileOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.M2MFile);
            var clientM2MTagOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.M2MTag);
            var clientM2MEventOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.M2MEvent);
            var clientM2MUserOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.M2MUser);
            var clientM2MParticipationOptions = jwtAuthenticationOptions.GetClientOptions(ClientNameConstants.M2MParticipation);

            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;

            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    options.EventsType = typeof(CookieEventHandler);
                })
                .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
                {
                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.AuthenticationMethod = OpenIdConnectRedirectBehavior.FormPost;
                    options.Authority = jwtAuthenticationOptions.Authority;
                    options.ClientId = clientInteractiveOptions.ClientId;
                    options.ClientSecret = clientInteractiveOptions.ClientSecret;
                    options.ResponseType = OidcConstants.ResponseTypes.Code;
                    options.SignedOutCallbackPath = new PathString(clientInteractiveOptions.SignedOutCallbackPath);

                    // Only for local development
                    options.RequireHttpsMetadata = false;

                    options.SaveTokens = true;
                    options.UsePkce = true;

                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.Scope.Add("offline_access");

                    // This aligns the life of the cookie with the life of the token.
                    // Note this is not the actual expiration of the cookie as seen by the browser.
                    // It is an internal value stored in "expires_at".
                    options.UseTokenLifetime = false;

                    options.GetClaimsFromUserInfoEndpoint = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = JwtClaimTypes.Name,
                        RoleClaimType = JwtClaimTypes.Role,
                    };
                });

            // adds user and client access token management
            services.AddAccessTokenManagement(options =>
                {
                    options.Client.Clients.Add(clientM2MFileOptions.ClientName, new ClientCredentialsTokenRequest
                    {
                        Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                        ClientId = clientM2MFileOptions.ClientId,
                        ClientSecret = clientM2MFileOptions.ClientSecret,
                    });
                    options.Client.Clients.Add(clientM2MTagOptions.ClientName, new ClientCredentialsTokenRequest
                    {
                        Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                        ClientId = clientM2MTagOptions.ClientId,
                        ClientSecret = clientM2MTagOptions.ClientSecret,
                    });
                    options.Client.Clients.Add(clientM2MEventOptions.ClientName, new ClientCredentialsTokenRequest
                    {
                        Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                        ClientId = clientM2MEventOptions.ClientId,
                        ClientSecret = clientM2MEventOptions.ClientSecret,
                    });
                    options.Client.Clients.Add(clientM2MUserOptions.ClientName, new ClientCredentialsTokenRequest
                    {
                        Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                        ClientId = clientM2MUserOptions.ClientId,
                        ClientSecret = clientM2MUserOptions.ClientSecret,
                    });
                    options.Client.Clients.Add(clientM2MParticipationOptions.ClientName, new ClientCredentialsTokenRequest
                    {
                        Address = $"{jwtAuthenticationOptions.Authority}/connect/token",
                        ClientId = clientM2MParticipationOptions.ClientId,
                        ClientSecret = clientM2MParticipationOptions.ClientSecret,
                    });
                })
                .ConfigureBackchannelHttpClient()
                .AddTransientHttpErrorPolicy(policy => policy.WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(3),
                }));
        }

        public static void AddHttpClients(this IServiceCollection services)
        {
            var httpClientsOptions = services.GetOptions<ApiManagementOptions>();
            var fileHttpClientOptions = httpClientsOptions.GetHttpClientOptions(HttpClientNameConstants.FileHttpClient);
            var tagHttpClientOptions = httpClientsOptions.GetHttpClientOptions(HttpClientNameConstants.TagHttpClient);
            var eventHttpClientOptions = httpClientsOptions.GetHttpClientOptions(HttpClientNameConstants.EventHttpClient);
            var userHttpClientOptions = httpClientsOptions.GetHttpClientOptions(HttpClientNameConstants.UserHttpClient);
            var participationHttpClientOptions = httpClientsOptions.GetHttpClientOptions(HttpClientNameConstants.ParticipationHttpClient);

            services.AddHttpClient(fileHttpClientOptions.ClientName)
                .AddClientAccessTokenHandler(fileHttpClientOptions.IdentityClientName);

            services.AddHttpClient(tagHttpClientOptions.ClientName)
                .AddClientAccessTokenHandler(tagHttpClientOptions.IdentityClientName);

            services.AddHttpClient(eventHttpClientOptions.ClientName)
                .AddClientAccessTokenHandler(eventHttpClientOptions.IdentityClientName);

            services.AddHttpClient(userHttpClientOptions.ClientName)
                .AddClientAccessTokenHandler(userHttpClientOptions.IdentityClientName);

            services.AddHttpClient(participationHttpClientOptions.ClientName)
                .AddClientAccessTokenHandler(participationHttpClientOptions.IdentityClientName);
        }

        private static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                .BuildServiceProvider()
                .GetService<IOptions<TOptions>>()
                    ?? throw new NullReferenceException(nameof(TOptions)))
                .Value;
        }
    }
}