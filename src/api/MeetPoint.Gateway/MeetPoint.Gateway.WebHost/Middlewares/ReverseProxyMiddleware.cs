using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MeetPoint.Gateway.Core.Domain.Constants;
using MeetPoint.Gateway.Core.Domain.Exceptions;
using MeetPoint.Gateway.Core.Domain.Options;
using MeetPoint.Gateway.WebHost.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace MeetPoint.Gateway.WebHost.Middlewares
{
    public class ReverseProxyMiddleware
    {
        private readonly RequestDelegate nextMiddleware;
        private readonly IHttpClientFactory factory;
        private readonly ApiManagementOptions options;

        public ReverseProxyMiddleware(RequestDelegate nextMiddleware,
            IHttpClientFactory factory,
            IOptions<ApiManagementOptions> options)
        {
            this.nextMiddleware = nextMiddleware;
            this.factory = factory;
            this.options = options.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            var targetUri = BuildTargetUriOrDefault(context.Request);

            if (targetUri != null)
            {
                var targetRequestMessage = CreateTargetMessage(context, targetUri);
                var targetHttpClientName = GetHttpClientNameOfFailure(context.Request);

                using var httpClient = factory.CreateClient(targetHttpClientName);
                using var responseMessage = await httpClient.SendAsync(targetRequestMessage, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted);

                context.Response.StatusCode = (int)responseMessage.StatusCode;

                CopyFromTargetResponseHeaders(context, responseMessage);

                await ProcessResponseContent(context, responseMessage);

                return;
            }

            await nextMiddleware(context);
        }

        private static HttpMethod GetMethod(string method)
        {
            if (HttpMethods.IsDelete(method))
            {
                return HttpMethod.Delete;
            }

            if (HttpMethods.IsGet(method))
            {
                return HttpMethod.Get;
            }

            if (HttpMethods.IsHead(method))
            {
                return HttpMethod.Head;
            }

            if (HttpMethods.IsOptions(method))
            {
                return HttpMethod.Options;
            }

            if (HttpMethods.IsPost(method))
            {
                return HttpMethod.Post;
            }

            if (HttpMethods.IsPut(method))
            {
                return HttpMethod.Put;
            }

            if (HttpMethods.IsTrace(method))
            {
                return HttpMethod.Trace;
            }

            return new HttpMethod(method);
        }

        private async Task ProcessResponseContent(HttpContext context, HttpResponseMessage responseMessage)
        {
            var content = await responseMessage.Content.ReadAsByteArrayAsync();

            await context.Response.Body.WriteAsync(content);
        }

        private HttpRequestMessage CreateTargetMessage(HttpContext context, Uri targetUri)
        {
            var requestMessage = new HttpRequestMessage();
            CopyFromOriginalRequestContentAndHeaders(context, requestMessage);

            targetUri = new Uri(targetUri.OriginalString);

            requestMessage.RequestUri = targetUri;
            requestMessage.Headers.Host = targetUri.Host;
            requestMessage.Method = GetMethod(context.Request.Method);

            return requestMessage;
        }

        private void CopyFromOriginalRequestContentAndHeaders(HttpContext context, HttpRequestMessage requestMessage)
        {
            var requestMethod = context.Request.Method;

            if (!HttpMethods.IsGet(requestMethod) &&
                !HttpMethods.IsHead(requestMethod) &&
                !HttpMethods.IsDelete(requestMethod) &&
                !HttpMethods.IsTrace(requestMethod))
            {
                var streamContent = new StreamContent(context.Request.Body);
                requestMessage.Content = streamContent;
            }

            foreach (var header in context.Request.Headers)
            {
                requestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
            }
        }

        private void CopyFromTargetResponseHeaders(HttpContext context, HttpResponseMessage responseMessage)
        {
            foreach (var header in responseMessage.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }

            foreach (var header in responseMessage.Content.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }

            context.Response.Headers.Remove("transfer-encoding");
        }

        private Uri BuildTargetUriOrDefault(HttpRequest request)
        {
            PathString remainingPath;

            if (request.Path.StartsWithSegments(RouteSegmentConstants.File, out remainingPath))
            {
                var fileHttpClientOptions = options.GetHttpClientOptions(HttpClientNameConstants.FileHttpClient);
                return new Uri($"{fileHttpClientOptions.BaseAddress}{RouteSegmentConstants.File}" + remainingPath);
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Tag, out remainingPath))
            {
                var tagHttpClientOptions = options.GetHttpClientOptions(HttpClientNameConstants.TagHttpClient);
                return new Uri($"{tagHttpClientOptions.BaseAddress}{RouteSegmentConstants.Tag}" + remainingPath);
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Event, out remainingPath))
            {
                var eventHttpClientOptions = options.GetHttpClientOptions(HttpClientNameConstants.EventHttpClient);
                return new Uri($"{eventHttpClientOptions.BaseAddress}{RouteSegmentConstants.Event}" + remainingPath);
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.User, out remainingPath))
            {
                var userHttpClientOptions = options.GetHttpClientOptions(HttpClientNameConstants.UserHttpClient);
                return new Uri($"{userHttpClientOptions.BaseAddress}{RouteSegmentConstants.User}" + remainingPath);
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Participation, out remainingPath))
            {
                var participationHttpClientOptions = options.GetHttpClientOptions(HttpClientNameConstants.ParticipationHttpClient);
                return new Uri($"{participationHttpClientOptions.BaseAddress}{RouteSegmentConstants.Participation}" + remainingPath);
            }

            return null;
        }

        private string GetHttpClientNameOfFailure(HttpRequest request)
        {
            if (request.Path.StartsWithSegments(RouteSegmentConstants.File))
            {
                return HttpClientNameConstants.FileHttpClient;
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Tag))
            {
                return HttpClientNameConstants.TagHttpClient;
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Event))
            {
                return HttpClientNameConstants.EventHttpClient;
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.User))
            {
                return HttpClientNameConstants.UserHttpClient;
            }

            if (request.Path.StartsWithSegments(RouteSegmentConstants.Participation))
            {
                return HttpClientNameConstants.ParticipationHttpClient;
            }

            throw new BaseAppException();
        }
    }
}