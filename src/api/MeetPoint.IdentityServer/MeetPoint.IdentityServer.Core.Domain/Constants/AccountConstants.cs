namespace MeetPoint.IdentityServer.Core.Domain.Constants
{
    public class AccountConstants
    {
        public const bool AllowLocalLogin = true;
        public const bool AllowRememberLogin = true;
        public const bool ShowLogoutPrompt = false;
        public const bool AutomaticRedirectAfterSignOut = true;
        public const string InvalidCredentialsErrorMessage = "Invalid username or password";
    }
}
