namespace MeetPoint.IdentityServer.Core.Domain.Constants
{
    public static class AppSettingsConstants
    {
        public static class ConnectionStrings
        {
            public const string DefaultConnection = "DefaultConnection";
        }

        public static class ExternalProviders
        {
            public const string Google = "Google";
        }
    }
}