namespace MeetPoint.IdentityServer.Core.Domain.Constants
{
    public class MessageBrokerEndpointRouteConstants
    {
        public const string SendEmail = "send-email";
        public const string CreateUser = "create-user";
    }
}