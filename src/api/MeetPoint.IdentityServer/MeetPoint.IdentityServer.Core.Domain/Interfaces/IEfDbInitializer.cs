namespace MeetPoint.IdentityServer.Core.Domain.Interfaces
{
    public interface IEfDbInitializer
    {
        void Initialize();

        void Clean();
    }
}