using System.Collections.Generic;
using IdentityServer4.Models;

namespace MeetPoint.IdentityServer.Core.Domain.Interfaces
{
    public interface IIdentityConfigService
    {
        IEnumerable<IdentityResource> GetIdentityResources();

        IEnumerable<ApiScope> GetApiScopes();

        IEnumerable<Client> GetClients();
    }
}