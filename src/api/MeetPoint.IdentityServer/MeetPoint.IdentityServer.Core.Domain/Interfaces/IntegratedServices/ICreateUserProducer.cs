using System.Threading.Tasks;
using MeetPoint.IdentityServer.Core.Domain.Models.Contracts;

namespace MeetPoint.IdentityServer.Core.Domain.Interfaces.IntegratedServices
{
    public interface ICreateUserProducer
    {
        Task CreateUser(CreateUserContract createUserContract);
    }
}