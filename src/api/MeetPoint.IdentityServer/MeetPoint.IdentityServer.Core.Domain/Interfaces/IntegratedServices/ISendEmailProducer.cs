using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Models.Contracts;

namespace MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices
{
    public interface ISendEmailProducer
    {
        Task SendEmail(SendEmailContract sendEmailContract);
    }
}