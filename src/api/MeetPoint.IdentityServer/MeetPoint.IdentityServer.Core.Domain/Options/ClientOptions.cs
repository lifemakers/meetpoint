using System.Collections.Generic;

namespace MeetPoint.IdentityServer.Core.Domain.Options
{
    public class ClientOptions
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string ClientUri { get; set; }

        public string GrantType { get; set; }

        public string RedirectUri { get; set; }

        public string PostLogoutRedirectUri { get; set; }

        public bool FrontChannelLogoutSessionRequired { get; set; }

        public string FrontChannelLogoutUri { get; set; }

        public bool AllowOfflineAccess { get; set; }

        public List<string> AllowedScopes { get; set; }

        public int AccessTokenLifetime { get; set; }

        public int AbsoluteRefreshTokenLifetime { get; set; }

        public bool RefreshTokenOneTimeOnly { get; set; }
    }
}