namespace MeetPoint.IdentityServer.Core.Domain.Options
{
    public class CorsOptions
    {
        public const string Position = "Cors";

        public string AllowedOrigins { get; set; }
    }
}