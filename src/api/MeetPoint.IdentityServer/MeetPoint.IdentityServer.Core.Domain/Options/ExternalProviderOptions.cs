namespace MeetPoint.IdentityServer.Core.Domain.Options
{
    public class ExternalProviderOptions
    {
        public string Name { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }
    }
}