using System.Collections.Generic;

namespace MeetPoint.IdentityServer.Core.Domain.Options
{
    public class JwtAuthenticationOptions
    {
        public const string Position = "JwtAuthentication";

        public string IssuerUri { get; set; }

        public IEnumerable<string> ApiScopes { get; set; }

        public IEnumerable<ClientOptions> Clients { get; set; }

        public IEnumerable<ExternalProviderOptions> ExternalProviders { get; set; }
    }
}