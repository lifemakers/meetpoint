﻿using System;
using System.Collections.Generic;
using System.Linq;
using IdentityServer4.Models;
using MeetPoint.IdentityServer.Core.Domain.Interfaces;
using MeetPoint.IdentityServer.Core.Domain.Options;
using Microsoft.Extensions.Options;

namespace MeetPoint.IdentityServer.Core.Domain.Services
{
    public class IdentityConfigService : IIdentityConfigService
    {
        private readonly JwtAuthenticationOptions options;

        public IdentityConfigService(IOptions<JwtAuthenticationOptions> options)
        {
            this.options = options.Value;
        }

        public IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public IEnumerable<ApiScope> GetApiScopes()
        {
            var apiScopes = this.options.ApiScopes;

            return apiScopes.Select(x => new ApiScope(x));
        }

        public IEnumerable<Client> GetClients()
        {
            var clientsFromSettings = this.options.Clients;
            var clients = new List<Client>();

            foreach (var clientFromSettings in clientsFromSettings)
            {
                var client = new Client();
                client.ClientId = clientFromSettings.ClientId;
                client.ClientSecrets = new List<Secret> { new Secret(clientFromSettings.ClientSecret.Sha256()) };
                client.AllowedScopes = clientFromSettings.AllowedScopes;

                switch (clientFromSettings.GrantType)
                {
                    case "ClientCredentials":
                    {
                        client.AllowedGrantTypes = GrantTypes.ClientCredentials;

                        break;
                    }

                    // interactive client using code flow + pkce.
                    case "Code":
                    {
                        client.AllowedGrantTypes = GrantTypes.Code;
                        client.ClientUri = clientFromSettings.ClientUri;
                        client.RedirectUris = new List<string> { clientFromSettings.RedirectUri };
                        client.PostLogoutRedirectUris = new List<string> { clientFromSettings.PostLogoutRedirectUri };
                        client.FrontChannelLogoutSessionRequired = clientFromSettings.FrontChannelLogoutSessionRequired;
                        client.FrontChannelLogoutUri = clientFromSettings.FrontChannelLogoutUri;
                        client.AllowOfflineAccess = clientFromSettings.AllowOfflineAccess;
                        client.AccessTokenLifetime = clientFromSettings.AccessTokenLifetime;
                        client.AbsoluteRefreshTokenLifetime = clientFromSettings.AbsoluteRefreshTokenLifetime;
                        client.RefreshTokenUsage = clientFromSettings.RefreshTokenOneTimeOnly ? TokenUsage.OneTimeOnly : TokenUsage.ReUse;

                        break;
                    }

                    default:
                        throw new InvalidOperationException(nameof(clientFromSettings.GrantType));
                }

                clients.Add(client);
            }

            return clients;
        }
    }
}