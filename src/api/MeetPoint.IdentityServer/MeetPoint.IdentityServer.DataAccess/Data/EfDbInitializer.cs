using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using MeetPoint.IdentityServer.Core.Domain.Interfaces;
using MeetPoint.IdentityServer.Core.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MeetPoint.IdentityServer.DataAccess.Data
{
    public class EfDbInitializer : IEfDbInitializer
    {
        private readonly PersistedGrantDbContext persistedGrantDbContext;
        private readonly ConfigurationDbContext configurationDbContext;
        private readonly DataContext dataContext;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly ILogger<EfDbInitializer> logger;
        private readonly IIdentityConfigService identityConfigService;

        public EfDbInitializer(PersistedGrantDbContext persistedGrantDbContext,
            ConfigurationDbContext configurationDbContext,
            DataContext dataContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ILogger<EfDbInitializer> logger,
            IIdentityConfigService identityConfigService)
        {
            this.persistedGrantDbContext = persistedGrantDbContext;
            this.configurationDbContext = configurationDbContext;
            this.dataContext = dataContext;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.logger = logger;
            this.identityConfigService = identityConfigService;
        }

        public void Initialize()
        {
            this.Clean();
            this.InitializePersistedGrantDbContext();
            this.InitializeConfigurationDbContext();
            this.InitializeDataContext();
        }

        public void Clean()
        {
            this.dataContext.Database.EnsureDeleted();
            this.persistedGrantDbContext.Database.EnsureDeleted();
            this.configurationDbContext.Database.EnsureDeleted();
        }

        private void InitializePersistedGrantDbContext()
        {
            this.persistedGrantDbContext.Database.Migrate();
        }

        private void InitializeConfigurationDbContext()
        {
            this.configurationDbContext.Database.Migrate();

            if (!this.configurationDbContext.Clients.Any())
            {
                foreach (var client in this.identityConfigService.GetClients())
                {
                    this.configurationDbContext.Clients.Add(client.ToEntity());
                }

                this.configurationDbContext.SaveChanges();
            }

            if (!this.configurationDbContext.IdentityResources.Any())
            {
                foreach (var resource in this.identityConfigService.GetIdentityResources())
                {
                    this.configurationDbContext.IdentityResources.Add(resource.ToEntity());
                }

                this.configurationDbContext.SaveChanges();
            }

            if (!this.configurationDbContext.ApiScopes.Any())
            {
                foreach (var resource in this.identityConfigService.GetApiScopes())
                {
                    this.configurationDbContext.ApiScopes.Add(resource.ToEntity());
                }

                this.configurationDbContext.SaveChanges();
            }
        }

        private void InitializeDataContext()
        {
            this.dataContext.Database.Migrate();

            var alice = this.userManager.FindByNameAsync("alice").Result;
            if (alice == null)
            {
                alice = new ApplicationUser
                {
                    UserName = "alice",
                    Email = "AliceSmith@email.com",
                    EmailConfirmed = true,
                    FirstName = "Alice",
                    LastName = "Alice",
                };
                var result = this.userManager.CreateAsync(alice, "Pass123$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = this.userManager.AddClaimsAsync(alice, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, "Alice Smith"),
                    new Claim(JwtClaimTypes.GivenName, "Alice"),
                    new Claim(JwtClaimTypes.FamilyName, "Smith"),
                    new Claim(JwtClaimTypes.WebSite, "http://alice.com"),
                }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                this.logger.LogDebug("alice created");
            }
            else
            {
                this.logger.LogDebug("alice already exists");
            }

            var bob = this.userManager.FindByNameAsync("bob").GetAwaiter().GetResult();

            if (bob == null)
            {
                bob = new ApplicationUser
                {
                    UserName = "bob",
                    Email = "BobSmith@email.com",
                    EmailConfirmed = true,
                    FirstName = "Bob",
                    LastName = "Bob",
                };
                var result = this.userManager.CreateAsync(bob, "Pass123$").GetAwaiter().GetResult();

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = this.userManager.AddClaimsAsync(bob, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, "Bob Smith"),
                    new Claim(JwtClaimTypes.GivenName, "Bob"),
                    new Claim(JwtClaimTypes.FamilyName, "Smith"),
                    new Claim(JwtClaimTypes.WebSite, "http://bob.com"),
                    new Claim("location", "somewhere"),
                }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                this.logger.LogDebug("bob created");

                result = this.roleManager.CreateAsync(new IdentityRole("admin")).GetAwaiter().GetResult();
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                this.userManager.AddToRoleAsync(alice, "admin").GetAwaiter().GetResult();
            }
        }
    }
}