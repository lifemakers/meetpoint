using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.IdentityServer.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.IdentityServer.Core.Domain.Models.Contracts;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class CreateUserProducer : ICreateUserProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<CreateUserProducer> logger;

        public CreateUserProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<CreateUserProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task CreateUser(CreateUserContract createUserContract)
        {
            this.logger.LogInformation("Sending command for creating user");

            await sendEndpointProvider.Send<ICreateUserContract>(createUserContract);

            this.logger.LogInformation("Sent command successfully for creating user");
        }
    }
}