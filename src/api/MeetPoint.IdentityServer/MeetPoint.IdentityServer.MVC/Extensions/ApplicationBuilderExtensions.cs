using System;
using MeetPoint.IdentityServer.Core.Domain.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeetPoint.IdentityServer.MVC.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseAllowedCors(this IApplicationBuilder builder)
        {
            var corsOptions = builder.ApplicationServices
                .GetService<IOptions<CorsOptions>>()
                ?.Value;

            var corsAllowedOrigins = corsOptions
                                         ?.AllowedOrigins
                                         ?.Split(";")
                                     ?? throw new ArgumentException(nameof(CorsOptions));

            builder.UseCors(x => x
                .WithOrigins(corsAllowedOrigins)
                .AllowAnyMethod()
                .AllowCredentials()
                .AllowAnyHeader());

            return builder;
        }
    }
}