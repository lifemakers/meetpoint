using System;
using System.Linq;
using MeetPoint.IdentityServer.Core.Domain.Options;

namespace MeetPoint.IdentityServer.MVC.Extensions
{
    public static class JwtAuthenticationOptionsExtensions
    {
        public static ExternalProviderOptions GetExternalProviderOptions(
            this JwtAuthenticationOptions jwtAuthenticationOptions,
            string externalProviderOptionName)
        {
            return jwtAuthenticationOptions
                       ?.ExternalProviders
                       ?.SingleOrDefault(p => p.Name == externalProviderOptionName)
                   ?? throw new ArgumentException(nameof(externalProviderOptionName));
        }
    }
}