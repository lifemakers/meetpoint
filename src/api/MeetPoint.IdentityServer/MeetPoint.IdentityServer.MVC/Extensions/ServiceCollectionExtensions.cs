using System;
using IdentityServer4;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.IdentityServer.Core.Domain.Constants;
using MeetPoint.IdentityServer.Core.Domain.Interfaces;
using MeetPoint.IdentityServer.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.IdentityServer.Core.Domain.Models;
using MeetPoint.IdentityServer.Core.Domain.Options;
using MeetPoint.IdentityServer.Core.Domain.Services;
using MeetPoint.IdentityServer.DataAccess;
using MeetPoint.IdentityServer.DataAccess.Data;
using MeetPoint.IdentityServer.MVC.Options;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Participation.IntegratedServices.Options;
using MeetPoint.Participation.IntegratedServices.Producers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeetPoint.IdentityServer.MVC.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStringDefaultName = AppSettingsConstants.ConnectionStrings.DefaultConnection;
            var connectionStringDefault = configuration.GetConnectionString(connectionStringDefaultName);

            services.Configure<DatabaseOptions>(x => x.ConnectionString = connectionStringDefault);
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<CorsOptions>(configuration.GetSection(CorsOptions.Position));
            services.Configure<MessageBrokerOptions>(configuration.GetSection(MessageBrokerOptions.Position));
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IIdentityConfigService, IdentityConfigService>();
        }

        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddTransient<ISendEmailProducer, SendEmailProducer>();
            services.AddTransient<ICreateUserProducer, CreateUserProducer>();
        }

        public static void AddIdentityServices(this IServiceCollection services,
            Action<IServiceProvider, DbContextOptionsBuilder> dbContextConfigureAction)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();

            services.AddDbContext<DataContext>(dbContextConfigureAction);

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            services.AddIdentityServer(options =>
                {
                    options.IssuerUri = jwtAuthenticationOptions.IssuerUri;
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;

                    // see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
                    options.EmitStaticAudienceClaim = true;
                })

                .AddDeveloperSigningCredential()

                .AddConfigurationStore(options =>
                {
                    options.ResolveDbContextOptions = dbContextConfigureAction;
                })
                .AddOperationalStore(options =>
                {
                    options.ResolveDbContextOptions = dbContextConfigureAction;
                })
                .AddAspNetIdentity<ApplicationUser>();
        }

        public static void AddDataAccessServices(this IServiceCollection services)
        {
            services.AddScoped<IEfDbInitializer, EfDbInitializer>();
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint Identity Server API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddGoogleAuthentication(this IServiceCollection services)
        {
            var googleOptions = services
                .GetOptions<JwtAuthenticationOptions>()
                .GetExternalProviderOptions(AppSettingsConstants.ExternalProviders.Google);

            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    // register your IdentityServer with Google at https://console.developers.google.com
                    // enable the Google+ API
                    // set the redirect URI to https://localhost:6001/signin-google
                    options.ClientId = googleOptions.ClientId;
                    options.ClientSecret = googleOptions.ClientSecret;
                });
        }

        public static void AddMassTransitServices(this IServiceCollection services)
        {
            var messageBrokerOptions = services.GetOptions<MessageBrokerOptions>();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(messageBrokerOptions.ConnectionString, h =>
                    {
                        h.Username(messageBrokerOptions.Username);
                        h.Password(messageBrokerOptions.Password);
                    });

                    var sendEmailUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.SendEmail}");
                    var createUserUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.CreateUser}");

                    EndpointConvention.Map<ISendEmailContract>(sendEmailUri);
                    EndpointConvention.Map<ICreateUserContract>(createUserUri);

                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        private static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                        .BuildServiceProvider()
                        .GetService<IOptions<TOptions>>()
                    ?? throw new ArgumentException(nameof(TOptions)))
                .Value;
        }
    }
}