namespace MeetPoint.IdentityServer.MVC.Models
{
    public class ExternalProvider
    {
        public string DisplayName { get; set; }

        public string AuthenticationScheme { get; set; }
    }
}