using IdentityServer4.Models;
using MeetPoint.IdentityServer.MVC.ViewModels;

namespace MeetPoint.IdentityServer.MVC.Models
{
    public class ProcessConsentResult
    {
        public bool IsRedirect => RedirectUri != null;

        public string RedirectUri { get; set; }

        public Client Client { get; set; }

        public bool ShowView => ViewModel != null;

        public ConsentViewModel ViewModel { get; set; }

        public bool HasValidationError => ValidationError != null;

        public string ValidationError { get; set; }
    }
}
