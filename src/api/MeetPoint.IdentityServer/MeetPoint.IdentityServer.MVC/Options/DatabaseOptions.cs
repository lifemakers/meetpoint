namespace MeetPoint.IdentityServer.MVC.Options
{
    public class DatabaseOptions
    {
        public const string Position = "AzureBlobStorage";

        public string ConnectionString { get; set; }
    }
}