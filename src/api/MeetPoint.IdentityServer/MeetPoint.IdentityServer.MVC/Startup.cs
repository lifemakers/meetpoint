﻿using System;
using System.Reflection;
using MeetPoint.IdentityServer.Core.Domain.Interfaces;
using MeetPoint.IdentityServer.DataAccess;
using MeetPoint.IdentityServer.MVC.Extensions;
using MeetPoint.IdentityServer.MVC.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace MeetPoint.IdentityServer.MVC
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }

        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddAppConfigurations(this.Configuration);
            services.AddDomainServices();
            services.AddIntegratedServices();
            services.AddIdentityServices(this.DbContextConfigureAction);
            services.AddDataAccessServices();
            services.AddGoogleAuthentication();
            services.AddCors();

            services.AddMassTransitServices();
        }

        public void Configure(IApplicationBuilder app, IEfDbInitializer dbInitializer)
        {
            dbInitializer.Initialize();

            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();
            app.UseAllowedCors();
            app.UseIdentityServer();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }

        private void DbContextConfigureAction(IServiceProvider provider, DbContextOptionsBuilder builder)
        {
            var migrationsAssembly = typeof(DataContext).GetTypeInfo().Assembly.GetName().Name;
            var options = provider.GetRequiredService<IOptions<DatabaseOptions>>().Value;

            builder.UseNpgsql(options.ConnectionString,
                sql => sql.MigrationsAssembly(migrationsAssembly));
        }
    }
}