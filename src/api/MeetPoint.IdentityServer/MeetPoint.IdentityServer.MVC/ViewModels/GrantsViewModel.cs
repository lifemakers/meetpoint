using System;
using System.Collections.Generic;

namespace MeetPoint.IdentityServer.MVC.ViewModels
{
    public class GrantsViewModel
    {
        public IEnumerable<GrantViewModel> Grants { get; set; }
    }
}