using MeetPoint.IdentityServer.MVC.Models;

namespace MeetPoint.IdentityServer.MVC.ViewModels
{
    public class LogoutViewModel : LogoutInputModel
    {
        public bool ShowLogoutPrompt { get; set; } = true;
    }
}
