using System.ComponentModel.DataAnnotations;

namespace MeetPoint.IdentityServer.MVC.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Password required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirm password not matching")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "FirstName is required")]

        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]

        public string LastName { get; set; }
    }
}