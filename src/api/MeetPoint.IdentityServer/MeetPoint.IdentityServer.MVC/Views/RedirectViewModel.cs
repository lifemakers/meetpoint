namespace MeetPoint.IdentityServer.MVC.Views
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}