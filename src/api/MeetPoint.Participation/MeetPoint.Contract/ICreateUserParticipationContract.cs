using System;

namespace MeetPoint.Contract
{
    public interface ICreateUserParticipationContract
    {
        public Guid EventId { get; set; }

        public Guid ParticipationId { get; set; }

        public Guid UserId { get; set; }
    }
}