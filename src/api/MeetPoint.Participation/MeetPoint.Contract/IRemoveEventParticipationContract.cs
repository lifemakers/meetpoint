using System;

namespace MeetPoint.Contract
{
    public interface IRemoveEventParticipationContract
    {
        Guid EventId { get; set; }

        Guid ParticipationId { get; set; }
    }
}