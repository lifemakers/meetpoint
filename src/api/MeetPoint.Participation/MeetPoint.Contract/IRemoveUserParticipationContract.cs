using System;

namespace MeetPoint.Contract
{
    public interface IRemoveUserParticipationContract
    {
        Guid UserId { get; set; }

        Guid ParticipationId { get; set; }
    }
}