﻿using System;

namespace MeetPoint.Contract
{
    public interface ISendEmailContract
    {
        public string[] EmailsTo { get; set; }

        public string Subject { get; set; }

        public string Text { get; set; }
    }
}