using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MeetPoint.Participation.Core.Domain.DomainEvents;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Participation.Core.Domain.Models.Contracts;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Participation.Application.Handlers
{
    public class OnParticipationCreated
    {
        // TODO investigate pipeline, for each db, separate handler
        public class Handler : INotificationHandler<DomainEventNotification<ParticipationCreatedDomainEvent>>
        {
            private readonly ICreateUserParticipationProducer createUserParticipationProducer;
            private readonly ICreateEventParticipationProducer createEventParticipationProducer;
            private readonly ILogger<Handler> logger;

            public Handler(ILogger<Handler> logger,
                ICreateUserParticipationProducer createUserParticipationProducer,
                ICreateEventParticipationProducer createEventParticipationProducer)
            {
                this.logger = logger;
                this.createUserParticipationProducer = createUserParticipationProducer;
                this.createEventParticipationProducer = createEventParticipationProducer;
            }

            public async Task Handle(DomainEventNotification<ParticipationCreatedDomainEvent> notification, CancellationToken cancellationToken)
            {
                var domainEvent = notification.DomainEvent;
                var eventId = domainEvent.EventId;
                var participationId = domainEvent.ParticipationId;
                var participationRole = domainEvent.ParticipationRole;
                var userId = domainEvent.UserId;

                try
                {
                    this.logger.LogDebug($"Handling Domain Event. {domainEvent}");

                    // send integration event for event participation creating
                    var createEventParticipationContract = new CreateEventParticipationContract
                    {
                        EventId = eventId,
                        ParticipationId = participationId,
                        ParticipationRole = participationRole,
                        UserId = userId,
                    };
                    await createEventParticipationProducer.CreateEventParticipation(createEventParticipationContract);

                    // send integration event for user participation creating
                    var createUserParticipationContract = new CreateUserParticipationContract
                    {
                        EventId = eventId,
                        ParticipationId = participationId,
                        ParticipationRole = participationRole,
                        UserId = userId,
                    };
                    await createUserParticipationProducer.CreateUserParticipation(createUserParticipationContract);
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, $"Error Handling Domain Event. {domainEvent}");
                    throw;
                }
            }
        }
    }
}