using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MeetPoint.Participation.Core.Domain.DomainEvents;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Participation.Core.Domain.Models.Contracts;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Participation.Application.Handlers
{
    public class OnParticipationRemoved
    {
        // TODO investigate pipeline, for each db, separate handler
        public class Handler : INotificationHandler<DomainEventNotification<ParticipationRemovedDomainEvent>>
        {
            private readonly IRemoveUserParticipationProducer removeUserParticipationProducer;
            private readonly IRemoveEventParticipationProducer removeEventParticipationProducer;
            private readonly ILogger<Handler> logger;

            public Handler(ILogger<Handler> logger,
                IRemoveUserParticipationProducer removeUserParticipationProducer,
                IRemoveEventParticipationProducer removeEventParticipationProducer)
            {
                this.logger = logger;
                this.removeUserParticipationProducer = removeUserParticipationProducer;
                this.removeEventParticipationProducer = removeEventParticipationProducer;
            }

            public async Task Handle(DomainEventNotification<ParticipationRemovedDomainEvent> notification, CancellationToken cancellationToken)
            {
                var domainEvent = notification.DomainEvent;
                var eventId = domainEvent.EventId;
                var participationId = domainEvent.ParticipationId;
                var userId = domainEvent.UserId;

                try
                {
                    this.logger.LogDebug($"Handling Domain Event. {domainEvent}");

                    // send integration event for event participation creating
                    var removeEventParticipationContract = new RemoveEventParticipationContract
                    {
                        EventId = eventId,
                        ParticipationId = participationId,
                    };
                    await removeEventParticipationProducer.RemoveEventParticipation(removeEventParticipationContract);

                    // send integration event for user participation creating
                    var removeUserParticipationDtoContract = new RemoveUserParticipationContract
                    {
                        UserId = userId,
                        ParticipationId = participationId,
                    };
                    await removeUserParticipationProducer.RemoveUserParticipation(removeUserParticipationDtoContract);
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, $"Error Handling Domain Event. {domainEvent}");
                    throw;
                }
            }
        }
    }
}