using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.DomainEvents;
using MeetPoint.Participation.Core.Domain.Exceptions;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MeetPoint.Participation.Core.Domain.Interfaces.Db;
using MeetPoint.Participation.Core.Domain.Interfaces.Services;
using MeetPoint.Participation.Core.Domain.Mappings;
using MeetPoint.Participation.Core.Domain.Models;
using MeetPoint.Participation.Core.Domain.Models.Dtos;

namespace MeetPoint.Participation.Application.Services
{
    public class ParticipationService : IParticipationService
    {
        private readonly IParticipationRepository participationRepository;
        private readonly ICurrentDateProvider currentDateProvider;
        private readonly IDomainEventDispatcher dispatcher;

        public ParticipationService(IParticipationRepository participationRepository,
            ICurrentDateProvider currentDateProvider,
            IDomainEventDispatcher dispatcher)
        {
            this.participationRepository = participationRepository;
            this.currentDateProvider = currentDateProvider;
            this.dispatcher = dispatcher;
        }

        public async Task<IEnumerable<ParticipationForListDto>> GetAllParticipationsByUserIdAsync(Guid userId)
        {
            var participations = await this.participationRepository.GetAllParticipationsByUserIdAsync(userId);

            return participations.ToParticipationForListDto();
        }

        public async Task<IEnumerable<ParticipationForListDto>> GetAllParticipationsByEventIdAsync(Guid eventId)
        {
            var participations = await this.participationRepository.GetAllParticipationsByEventIdAsync(eventId);

            return participations.ToParticipationForListDto();
        }

        public async Task<ParticipationDto> CreateParticipationAsync(CreateParticipationDto createParticipationDto)
        {
            var participation = new Core.Domain.Aggregates.Participation.Participation(new ValidModel<CreateParticipationDto>(createParticipationDto),
                this.currentDateProvider.GetDate());

            await this.participationRepository.CreateAsync(participation);

            await this.participationRepository.SaveChangesAsync();

            var participationCreatedDomainEvent = new ParticipationCreatedDomainEvent(participation);
            await this.dispatcher.Dispatch(participationCreatedDomainEvent);

            return participation.ToParticipationDto();
        }

        public async Task<ParticipationDto> GetParticipationByIdAsync(Guid id)
        {
            var participation = await this.participationRepository.GetByIdAsync(id);

            if (participation == null)
            {
                throw new ParticipationWasNotFoundException();
            }

            return participation.ToParticipationDto();
        }

        public async Task EditParticipationAsync(Guid participationId, EditParticipationDto editParticipationDto)
        {
            var participation = await this.participationRepository.GetByIdAsync(participationId);

            if (participation == null)
            {
                throw new ParticipationWasNotFoundException();
            }

            participation.Edit(new ValidModel<EditParticipationDto>(editParticipationDto),
                this.currentDateProvider.GetDate());

            await this.participationRepository.SaveChangesAsync();
        }

        public async Task DeleteParticipationAsync(Guid participationId)
        {
            var participation = await this.participationRepository.GetByIdAsync(participationId);

            if (participation == null)
            {
                throw new ParticipationWasNotFoundException();
            }

            await this.participationRepository.DeleteAsync(participation);

            await this.participationRepository.SaveChangesAsync();

            var participationRemovedDomainEvent = new ParticipationRemovedDomainEvent(participation);
            await this.dispatcher.Dispatch(participationRemovedDomainEvent);
        }
    }
}