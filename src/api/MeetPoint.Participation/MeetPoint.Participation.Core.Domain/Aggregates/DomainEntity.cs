using System;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MongoDB.Bson.Serialization.Attributes;

namespace MeetPoint.Participation.Core.Domain.Aggregates
{
    public abstract class DomainEntity : IDomainEntity
    {
        public Guid Id { get; protected set; }

        public DateTime DatePosted { get; protected set; }

        public DateTime DateLastUpdated { get; protected set; }

        protected Guid CreateNewId()
        {
            return Guid.NewGuid();
        }
    }
}