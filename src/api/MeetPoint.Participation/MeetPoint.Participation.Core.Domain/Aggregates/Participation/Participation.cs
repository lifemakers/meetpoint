using System;
using MeetPoint.Participation.Core.Domain.DomainEvents;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MeetPoint.Participation.Core.Domain.Models.Dtos;

namespace MeetPoint.Participation.Core.Domain.Aggregates.Participation
{
    public class Participation : DomainEntity
    {
        public Guid UserId { get; private set; }

        public Guid EventId { get; private set; }

        public ParticipationRole ParticipationRole { get; private set; }

        public Participation(IValidModel<CreateParticipationDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = this.CreateNewId();
            this.UserId = validModel.Value.UserId;
            this.EventId = validModel.Value.EventId;
            this.ParticipationRole = ParticipationRole.Participant;
            this.DatePosted = currentDate;
            this.DateLastUpdated = currentDate;
        }

        protected Participation()
        {
        }

        public void Edit(IValidModel<EditParticipationDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.ParticipationRole = Enum.Parse<ParticipationRole>(validModel.Value.ParticipationRole);

            this.DateLastUpdated = currentDate;
        }
    }
}