namespace MeetPoint.Participation.Core.Domain.Aggregates.Participation
{
    public enum ParticipationRole
    {
        Participant = 0,
        Moderator = 1,
        Owner = 2,
    }
}