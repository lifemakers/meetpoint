namespace MeetPoint.Participation.Core.Domain.Constants
{
    public class MessageBrokerEndpointRouteConstants
    {
        public const string SendEmail = "send-email";
        public const string CreateEventParticipation = "create-event-participation";
        public const string CreateUserParticipation = "create-user-participation";
        public const string RemoveEventParticipation = "remove-event-participation";
        public const string RemoveUserParticipation = "remove-user-participation";
    }
}