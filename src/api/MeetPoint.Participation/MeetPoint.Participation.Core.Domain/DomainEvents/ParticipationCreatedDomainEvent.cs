using System;
using MeetPoint.Participation.Core.Domain.Interfaces;

namespace MeetPoint.Participation.Core.Domain.DomainEvents
{
    public class ParticipationCreatedDomainEvent : IDomainEvent
    {
        public Guid ParticipationId { get; private set; }

        public Guid EventId { get; private set; }

        public Guid UserId { get; private set; }

        public string ParticipationRole { get; private set; }

        public ParticipationCreatedDomainEvent(Guid participationId,
            Guid eventId,
            Guid userId,
            string participationRole)
        {
            ParticipationId = participationId;
            EventId = eventId;
            UserId = userId;
            ParticipationRole = participationRole;
        }

        public ParticipationCreatedDomainEvent(Aggregates.Participation.Participation participation) : this(
            participation.Id,
            participation.EventId,
            participation.UserId,
            participation.ParticipationRole.ToString())
        {
        }

        public override string ToString()
        {
            return $"Type: {this.GetType().Name}," +
                   $"ParticipationId: {this.ParticipationId}," +
                   $"EventId: {this.EventId}," +
                   $"UserId: {this.UserId}";
        }
    }
}