using System;
using MeetPoint.Participation.Core.Domain.Interfaces;

namespace MeetPoint.Participation.Core.Domain.DomainEvents
{
    public class ParticipationRemovedDomainEvent : IDomainEvent
    {
        public Guid ParticipationId { get; private set; }

        public Guid EventId { get; private set; }

        public Guid UserId { get; private set; }

        public ParticipationRemovedDomainEvent(Guid participationId,
            Guid eventId,
            Guid userId)
        {
            ParticipationId = participationId;
            EventId = eventId;
            UserId = userId;
        }

        public ParticipationRemovedDomainEvent(Aggregates.Participation.Participation participation) : this (
            participation.Id,
            participation.EventId,
            participation.UserId)
        {
        }

        public override string ToString()
        {
            return $"Type: {this.GetType().Name}," +
                   $"ParticipationId: {this.ParticipationId}," +
                   $"EventId: {this.EventId}," +
                   $"UserId: {this.UserId}";
        }
    }
}