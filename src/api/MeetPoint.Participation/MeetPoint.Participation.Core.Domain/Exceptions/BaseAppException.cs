using System;
using MeetPoint.Participation.Core.Domain.Enums;
using MeetPoint.Participation.Core.Domain.Extensions;

namespace MeetPoint.Participation.Core.Domain.Exceptions
{
    public class BaseAppException : Exception
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.Unknown;

        public virtual int ErrorCode => (int)ErrorCodeValue;

        public BaseAppException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}