using MeetPoint.Participation.Core.Domain.Enums;
using MeetPoint.Participation.Core.Domain.Extensions;

namespace MeetPoint.Participation.Core.Domain.Exceptions
{
    public class ParticipationWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Participation ErrorCodeValue = ErrorCodes.Participation.ParticipationWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public ParticipationWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public ParticipationWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}