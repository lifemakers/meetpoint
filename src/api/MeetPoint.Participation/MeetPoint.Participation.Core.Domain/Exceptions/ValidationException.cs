﻿using System.Collections.Generic;
using MeetPoint.Participation.Core.Domain.Enums;
using MeetPoint.Participation.Core.Domain.Extensions;
using MeetPoint.Participation.Core.Domain.Models;

namespace MeetPoint.Participation.Core.Domain.Exceptions
{
    public class ValidationException : BaseAppException
    {
        private const ErrorCodes.Validation ErrorCodeValue = ErrorCodes.Validation.ValidationError;

        public override int ErrorCode => (int)ErrorCodeValue;

        public IEnumerable<ValidationError> ValidationErrors { get; private set; }

        public ValidationException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public ValidationException(string message)
            : base(message)
        {
        }

        public ValidationException(IEnumerable<ValidationError> validationError)
            : this()
        {
            this.ValidationErrors = validationError;
        }
    }
}