﻿using MeetPoint.Participation.Core.Domain.Enums;
using MeetPoint.Participation.Core.Domain.Resources;

namespace MeetPoint.Participation.Core.Domain.Extensions
{
    public static class ErrorCodeExtensions
    {
        public static string GetErrorMessage(this int errorCode)
        {
            return ErrorMessages.ResourceManager.GetString(errorCode.ToString());
        }

        public static string GetErrorMessage(this int? errorCode)
        {
            return errorCode.HasValue ? ErrorMessages.ResourceManager.GetString($"{errorCode}") : string.Empty;
        }

        public static string GetErrorMessage(this ErrorCodes.Validation errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.Participation errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.Global errorCode) => GetErrorMessage((int)errorCode);
    }
}