using System;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MeetPoint.Participation.Core.Domain.Interfaces.Db
{
    public interface IMongoDbContext
    {
        IMongoCollection<T> GetCollection<T>(string name)
            where T : class, IDomainEntity;

        void AddCommand(Func<Task> func);

        Task<int> SaveChangesAsync();
    }
}