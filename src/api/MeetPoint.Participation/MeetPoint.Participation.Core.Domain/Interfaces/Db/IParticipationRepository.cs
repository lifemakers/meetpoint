using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MeetPoint.Participation.Core.Domain.Interfaces.Db
{
    public interface IParticipationRepository : IRepository<Aggregates.Participation.Participation>
    {
        Task<IEnumerable<Core.Domain.Aggregates.Participation.Participation>> GetAllParticipationsByUserIdAsync(Guid userId);

        Task<IEnumerable<Core.Domain.Aggregates.Participation.Participation>> GetAllParticipationsByEventIdAsync(Guid eventId);
    }
}