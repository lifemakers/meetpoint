using System;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Aggregates;

namespace MeetPoint.Participation.Core.Domain.Interfaces.Db
{
    public interface IRepository<T>
        where T : class, IDomainEntity
    {
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task SaveChangesAsync();
    }
}