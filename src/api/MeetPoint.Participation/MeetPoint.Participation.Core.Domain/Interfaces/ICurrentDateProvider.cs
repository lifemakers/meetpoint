using System;

namespace MeetPoint.Participation.Core.Domain.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}