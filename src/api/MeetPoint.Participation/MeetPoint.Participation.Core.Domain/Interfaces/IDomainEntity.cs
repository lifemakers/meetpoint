using System;
using System.Collections.Concurrent;

namespace MeetPoint.Participation.Core.Domain.Interfaces
{
    public interface IDomainEntity
    {
        Guid Id { get; }

        DateTime DatePosted { get; }

        DateTime DateLastUpdated { get; }
    }
}