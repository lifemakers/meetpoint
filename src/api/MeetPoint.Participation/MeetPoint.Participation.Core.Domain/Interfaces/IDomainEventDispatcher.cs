using System.Threading.Tasks;

namespace MeetPoint.Participation.Core.Domain.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}