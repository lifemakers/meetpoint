namespace MeetPoint.Participation.Core.Domain.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}