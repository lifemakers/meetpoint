using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Models.Contracts;

namespace MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices
{
    public interface IRemoveEventParticipationProducer
    {
        Task RemoveEventParticipation(RemoveEventParticipationContract removeEventParticipationContract);
    }
}