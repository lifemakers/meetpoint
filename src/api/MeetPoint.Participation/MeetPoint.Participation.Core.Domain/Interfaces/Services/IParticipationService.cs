using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Models.Dtos;

namespace MeetPoint.Participation.Core.Domain.Interfaces.Services
{
    public interface IParticipationService
    {
        Task<IEnumerable<ParticipationForListDto>> GetAllParticipationsByUserIdAsync(Guid userId);

        Task<IEnumerable<ParticipationForListDto>> GetAllParticipationsByEventIdAsync(Guid eventId);

        Task<ParticipationDto> CreateParticipationAsync(CreateParticipationDto createParticipationDto);

        Task<ParticipationDto> GetParticipationByIdAsync(Guid id);

        Task EditParticipationAsync(Guid participationId, EditParticipationDto editParticipationDto);

        Task DeleteParticipationAsync(Guid participationId);
    }
}