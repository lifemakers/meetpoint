using MeetPoint.Participation.Core.Domain.Models.Dtos;

namespace MeetPoint.Participation.Core.Domain.Mappings
{
    public static class ParticipationDtoMapping
    {
        public static ParticipationDto ToParticipationDto(this Aggregates.Participation.Participation model)
        {
            return new ParticipationDto
            {
                Id = model.Id,
                UserId = model.UserId,
                EventId = model.EventId,
                DatePosted = model.DatePosted,
                DateLastUpdated = model.DateLastUpdated,
                ParticipationRole = model.ParticipationRole.ToString(),
            };
        }
    }
}