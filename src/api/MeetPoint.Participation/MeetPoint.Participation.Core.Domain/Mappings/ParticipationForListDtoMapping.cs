using System.Collections.Generic;
using MeetPoint.Participation.Core.Domain.Models.Dtos;

namespace MeetPoint.Participation.Core.Domain.Mappings
{
    public static class ParticipationForListDtoMapping
    {
        public static ParticipationForListDto ToParticipationForListDto(this Aggregates.Participation.Participation model)
        {
            return new ParticipationForListDto
            {
                Id = model.Id,
                UserId = model.UserId,
                EventId = model.EventId,
                DatePosted = model.DatePosted,
                DateLastUpdated = model.DateLastUpdated,
                ParticipationRole = model.ParticipationRole.ToString(),
            };
        }

        public static IEnumerable<ParticipationForListDto> ToParticipationForListDto(this IEnumerable<Aggregates.Participation.Participation> models)
        {
            var result = new List<ParticipationForListDto>();

            foreach (var model in models)
            {
                result.Add(model.ToParticipationForListDto());
            }

            return result;
        }
    }
}