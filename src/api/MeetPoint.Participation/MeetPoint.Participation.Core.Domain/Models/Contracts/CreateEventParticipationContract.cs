using System;

namespace MeetPoint.Participation.Core.Domain.Models.Contracts
{
    public class CreateEventParticipationContract : Contract.ICreateEventParticipationContract
    {
        public Guid EventId { get; set; }

        public Guid ParticipationId { get; set; }

        public Guid UserId { get; set; }

        public string ParticipationRole { get; set; }
    }
}