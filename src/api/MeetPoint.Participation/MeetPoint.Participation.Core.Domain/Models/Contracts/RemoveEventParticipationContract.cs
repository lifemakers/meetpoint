using System;

namespace MeetPoint.Participation.Core.Domain.Models.Contracts
{
    public class RemoveEventParticipationContract : Contract.IRemoveEventParticipationContract
    {
        public Guid EventId { get; set; }

        public Guid ParticipationId { get; set; }
    }
}