using System;

namespace MeetPoint.Participation.Core.Domain.Models.Contracts
{
    public class RemoveUserParticipationContract : Contract.IRemoveUserParticipationContract
    {
        public Guid UserId { get; set; }

        public Guid ParticipationId { get; set; }
    }
}