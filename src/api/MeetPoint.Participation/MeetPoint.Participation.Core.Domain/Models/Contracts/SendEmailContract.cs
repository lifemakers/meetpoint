namespace MeetPoint.Participation.Core.Domain.Models.Contracts
{
    public class SendEmailContract : Contract.ISendEmailContract
    {
        public string[] EmailsTo { get; set; }

        public string Subject { get; set; }

        public string Text { get; set; }
    }
}