using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.Participation.Core.Domain.Constants;

namespace MeetPoint.Participation.Core.Domain.Models.Dtos
{
    public class CreateParticipationDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid UserId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid EventId { get; set; }
    }
}