using System.ComponentModel.DataAnnotations;
using MeetPoint.Participation.Core.Domain.Constants;

namespace MeetPoint.Participation.Core.Domain.Models.Dtos
{
    public class EditParticipationDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string ParticipationRole { get; set; }
    }
}