using System;

namespace MeetPoint.Participation.Core.Domain.Models.Dtos
{
    public class ParticipationForListDto
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid EventId { get; set; }

        public DateTime DatePosted { get; set; }

        public DateTime DateLastUpdated { get; set; }

        public string ParticipationRole { get; set; }
    }
}