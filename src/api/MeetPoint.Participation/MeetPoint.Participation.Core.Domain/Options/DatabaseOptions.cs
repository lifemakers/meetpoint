namespace MeetPoint.Participation.Core.Domain.Options
{
    public class DatabaseOptions
    {
        public const string Position = "Database";

        public string DatabaseName { get; set; }

        public string ConnectionString { get; set; }
    }
}