using System;
using MeetPoint.Participation.Core.Domain.Interfaces;

namespace MeetPoint.Participation.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}