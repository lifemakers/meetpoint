using MeetPoint.Participation.Core.Domain.Aggregates;
using MongoDB.Bson.Serialization;

namespace MeetPoint.Participation.DataAccess.Configuration
{
    public class DomainEntityMap
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<DomainEntity>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.MapIdMember(x => x.Id);
            });
        }
    }
}