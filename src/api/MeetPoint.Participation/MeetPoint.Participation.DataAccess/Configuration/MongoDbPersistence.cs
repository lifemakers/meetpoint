using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;

namespace MeetPoint.Participation.DataAccess.Configuration
{
    public static class MongoDbPersistence
    {
        public static void Configure()
        {
            DomainEntityMap.Configure();

            // Conventions
            var pack = new ConventionPack
            {
                new IgnoreExtraElementsConvention(true),
                new IgnoreIfDefaultConvention(true),
            };
            ConventionRegistry.Register("MeetPoint", pack, t => true);
        }
    }
}