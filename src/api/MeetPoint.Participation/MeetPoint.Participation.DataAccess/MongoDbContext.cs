using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MeetPoint.Participation.Core.Domain.Interfaces.Db;
using MeetPoint.Participation.Core.Domain.Options;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace MeetPoint.Participation.DataAccess
{
    public class MongoDbContext : IMongoDbContext
    {
        private readonly List<Func<Task>> commands;
        private readonly DatabaseOptions options;
        private IMongoDatabase database;
        private MongoClient mongoClient;

        public MongoDbContext(IOptions<DatabaseOptions> options)
        {
            this.options = options.Value;

            this.commands = new List<Func<Task>>();
        }

        private void ConfigureMongo()
        {
            if (this.mongoClient != null)
            {
                return;
            }

            // Configure mongo (You can inject the config, just to simplify)
            this.mongoClient = new MongoClient(this.options.ConnectionString);

            this.database = this.mongoClient.GetDatabase(this.options.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
            where T : class, IDomainEntity
        {
            this.ConfigureMongo();

            return this.database.GetCollection<T>(name);
        }

        public async Task<int> SaveChangesAsync()
        {
            this.ConfigureMongo();

            var commandTasks = this.commands.Select(c => c());

            await Task.WhenAll(commandTasks);

            return this.commands.Count;
        }

        public void AddCommand(Func<Task> func)
        {
            this.commands.Add(func);
        }
    }
}