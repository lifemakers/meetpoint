using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Interfaces.Db;
using MongoDB.Driver;

namespace MeetPoint.Participation.DataAccess.Repositories
{
    public class ParticipationRepository : Repository<Core.Domain.Aggregates.Participation.Participation>, IParticipationRepository
    {
        protected IMongoDbContext MongoDbContext { get; private set; }

        public ParticipationRepository(IMongoDbContext mongoDbContext) : base(mongoDbContext)
        {
            this.MongoDbContext = mongoDbContext;
        }

        public async Task<IEnumerable<Core.Domain.Aggregates.Participation.Participation>> GetAllParticipationsByUserIdAsync(Guid userId)
        {
            var filter = Builders<Core.Domain.Aggregates.Participation.Participation>
                .Filter
                .Eq(x => x.UserId, userId);
            var sort = Builders<Core.Domain.Aggregates.Participation.Participation>
                .Sort
                .Descending(x => x.DatePosted);

            return (await this.DbSet.FindAsync(filter, new FindOptions<Core.Domain.Aggregates.Participation.Participation, Core.Domain.Aggregates.Participation.Participation>
            {
                Sort = sort,
            })).ToList();
        }

        public async Task<IEnumerable<Core.Domain.Aggregates.Participation.Participation>> GetAllParticipationsByEventIdAsync(Guid eventId)
        {
            var filter = Builders<Core.Domain.Aggregates.Participation.Participation>
                .Filter
                .Eq(x => x.EventId, eventId);
            var sort = Builders<Core.Domain.Aggregates.Participation.Participation>
                .Sort
                .Descending(x => x.DatePosted);

            return (await this.DbSet.FindAsync(filter, new FindOptions<Core.Domain.Aggregates.Participation.Participation, Core.Domain.Aggregates.Participation.Participation>
            {
                Sort = sort,
            })).ToList();
        }
    }
}