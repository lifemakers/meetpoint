using System;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Aggregates;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MeetPoint.Participation.Core.Domain.Interfaces.Db;
using MongoDB.Driver;

namespace MeetPoint.Participation.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : class, IDomainEntity
    {
        protected IMongoDbContext MongoDbContext { get; private set; }

        protected IMongoCollection<T> DbSet { get; private set; }

        public Repository(IMongoDbContext mongoDbContext)
        {
            this.MongoDbContext = mongoDbContext;

            DbSet = this.MongoDbContext.GetCollection<T>(typeof(T).Name);
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await this.DbSet.FindAsync(Builders<T>.Filter.Eq("_id", id));
            return result.SingleOrDefault();
        }

        public Task CreateAsync(T item)
        {
            this.MongoDbContext
                .AddCommand(async () => await this.DbSet.InsertOneAsync(item));

            return Task.CompletedTask; // EF Core backward compatibility
        }

        public Task UpdateAsync(T item)
        {
            this.MongoDbContext
                .AddCommand(async () => await this.DbSet.ReplaceOneAsync(Builders<T>.Filter.Eq("_id", item.Id), item));

            return Task.CompletedTask; // EF Core backward compatibility
        }

        public Task DeleteAsync(T item)
        {
            this.MongoDbContext
                .AddCommand(async () => await this.DbSet.DeleteOneAsync(Builders<T>.Filter.Eq("_id", item.Id)));

            return Task.CompletedTask; // EF Core backward compatibility
        }

        public async Task SaveChangesAsync()
        {
            await this.MongoDbContext.SaveChangesAsync();
        }
    }
}