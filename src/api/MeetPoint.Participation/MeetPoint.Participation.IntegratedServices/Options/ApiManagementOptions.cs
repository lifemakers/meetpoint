using System.Collections.Generic;

namespace MeetPoint.Participation.IntegratedServices.Options
{
    public class ApiManagementOptions
    {
        public const string Position = "ApiManagement";

        public IEnumerable<HttpClientOptions> HttpClients { get; set; }
    }
}