namespace MeetPoint.Participation.IntegratedServices.Options
{
    public class HttpClientOptions
    {
        public string ClientName { get; set; }

        public string BaseAddress { get; set; }

        public string IdentityClientName { get; set; }
    }
}