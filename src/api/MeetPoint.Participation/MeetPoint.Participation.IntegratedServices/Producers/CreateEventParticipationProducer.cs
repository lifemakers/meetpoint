using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using CreateEventParticipationContract = MeetPoint.Participation.Core.Domain.Models.Contracts.CreateEventParticipationContract;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class CreateEventParticipationProducer : ICreateEventParticipationProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<CreateEventParticipationProducer> logger;

        public CreateEventParticipationProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<CreateEventParticipationProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task CreateEventParticipation(CreateEventParticipationContract createEventParticipationContract)
        {
            this.logger.LogInformation("Sending command for creating event participation");

            await sendEndpointProvider.Send<Contract.ICreateEventParticipationContract>(createEventParticipationContract);

            this.logger.LogInformation("Sent command successfully for creating event participation");
        }
    }
}