using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using CreateUserParticipationContract = MeetPoint.Participation.Core.Domain.Models.Contracts.CreateUserParticipationContract;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class CreateUserParticipationProducer : ICreateUserParticipationProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<CreateUserParticipationProducer> logger;

        public CreateUserParticipationProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<CreateUserParticipationProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task CreateUserParticipation(CreateUserParticipationContract createUserParticipationContract)
        {
            this.logger.LogInformation("Sending command for creating user participation");

            await sendEndpointProvider.Send<Contract.ICreateUserParticipationContract>(createUserParticipationContract);

            this.logger.LogInformation("Sent command successfully for creating user participation");
        }
    }
}