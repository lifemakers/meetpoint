using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using RemoveEventParticipationContract = MeetPoint.Participation.Core.Domain.Models.Contracts.RemoveEventParticipationContract;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class RemoveEventParticipationProducer : IRemoveEventParticipationProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<RemoveEventParticipationProducer> logger;

        public RemoveEventParticipationProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<RemoveEventParticipationProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task RemoveEventParticipation(RemoveEventParticipationContract removeEventParticipationContract)
        {
            this.logger.LogInformation("Sending command for removing event participation");

            await sendEndpointProvider.Send<Contract.IRemoveEventParticipationContract>(removeEventParticipationContract);

            this.logger.LogInformation("Sent command successfully for removing event participation");
        }
    }
}