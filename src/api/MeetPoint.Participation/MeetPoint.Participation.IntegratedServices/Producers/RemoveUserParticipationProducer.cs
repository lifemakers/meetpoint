using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using RemoveUserParticipationContract = MeetPoint.Participation.Core.Domain.Models.Contracts.RemoveUserParticipationContract;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class RemoveUserParticipationProducer : IRemoveUserParticipationProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<RemoveUserParticipationProducer> logger;

        public RemoveUserParticipationProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<RemoveUserParticipationProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task RemoveUserParticipation(RemoveUserParticipationContract removeUserParticipationContract)
        {
            this.logger.LogInformation("Sending command for removing user participation");

            await sendEndpointProvider.Send<Contract.IRemoveUserParticipationContract>(removeUserParticipationContract);

            this.logger.LogInformation("Sent command successfully for removing user participation");
        }
    }
}