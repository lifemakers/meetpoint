using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using Microsoft.Extensions.Logging;
using SendEmailContract = MeetPoint.Participation.Core.Domain.Models.Contracts.SendEmailContract;

namespace MeetPoint.Participation.IntegratedServices.Producers
{
    public class SendEmailProducer : ISendEmailProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<SendEmailProducer> logger;

        public SendEmailProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<SendEmailProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task SendEmail(SendEmailContract sendEmailContract)
        {
            this.logger
                .LogInformation("Sending command for sending email with subject: {Subject} to: {@EmailsTo}",
                    sendEmailContract.Subject,
                    sendEmailContract.EmailsTo);

            await sendEndpointProvider.Send<Contract.ISendEmailContract>(sendEmailContract);

            this.logger
                .LogInformation("Sent command for sending email with subject: {Subject} to: {@EmailsTo}",
                    sendEmailContract.Subject,
                    sendEmailContract.EmailsTo);
        }
    }
}