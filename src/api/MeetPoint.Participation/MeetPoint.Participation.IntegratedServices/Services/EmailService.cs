using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Participation.Core.Domain.Models.Contracts;
using Newtonsoft.Json;

namespace MeetPoint.Participation.IntegratedServices.Services
{
    public class EmailService : IEmailService
    {
        private readonly ISendEmailProducer sendEmailProducer;

        public EmailService(ISendEmailProducer sendEmailProducer)
        {
            this.sendEmailProducer = sendEmailProducer;
        }

        public async Task SendEmailByBus(SendEmailContract sendEmailContract)
        {
            await this.sendEmailProducer.SendEmail(sendEmailContract);
        }
    }
}