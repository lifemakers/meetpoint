using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Participation.Core.Domain.Exceptions;
using MeetPoint.Participation.Core.Domain.Interfaces.Services;
using MeetPoint.Participation.Core.Domain.Models;
using MeetPoint.Participation.Core.Domain.Models.Dtos;
using MeetPoint.Participation.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Participation.WebHost.Controllers
{
    /// <summary> Participations Management. </summary>
    [ApiController]
    [Authorize]
    [Route("api/v1/participations")]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class ParticipationsController : Controller
    {
        private readonly IParticipationService participationService;
        private readonly ILogger<ParticipationsController> logger;

        public ParticipationsController(IParticipationService participationService,
            ILogger<ParticipationsController> logger)
        {
            this.participationService = participationService;
            this.logger = logger;
        }

        /// <summary> Get all participations. </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ParticipationForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<ParticipationForListDto>>> GetAllParticipationsOfEventAsync(Guid? eventId, Guid? userId)
        {
            if (eventId.HasValue)
            {
                return this.Ok(await this.participationService.GetAllParticipationsByEventIdAsync(eventId.Value));
            }
            else if (userId.HasValue)
            {
                return this.Ok(await this.participationService.GetAllParticipationsByUserIdAsync(userId.Value));
            }
            else
            {
                throw new BaseAppException();
            }
        }

        /// <summary> Get participation by id. </summary>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(typeof(ParticipationDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ParticipationDto>> GetParticipationByIdAsync(Guid id)
        {
            try
            {
                return this.Ok(await this.participationService.GetParticipationByIdAsync(id));
            }
            catch (ParticipationWasNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary> Create new participation. </summary>
        [HttpPost]
        [ProducesResponseType(typeof(ParticipationDto), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ParticipationDto>> CreateParticipationAsync([FromForm] CreateParticipationDto request)
        {
            this.logger.LogInformation("CreateParticipationDto: {@CreateParticipationDto} requested for participation creating", request);

            var response = await this.participationService.CreateParticipationAsync(request);

            this.logger.LogInformation("Participation: {@Participation} response after participation created", response);

            return CreatedAtAction(nameof(GetParticipationByIdAsync), new { id = response.Id }, response.Id);
        }

        /// <summary> Update participation. </summary>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EditParticipationAsync(Guid id, [FromForm] EditParticipationDto request)
        {
            this.logger.LogInformation("EditParticipation: {@EditParticipationDto} requested for participation editing", request);

            try
            {
                await this.participationService.EditParticipationAsync(id, request);
            }
            catch (ParticipationWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.Ok();
        }

        /// <summary> Delete participation. </summary>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteParticipationAsync(Guid id)
        {
            this.logger.LogInformation("DeleteParticipation Id: {@Id} requested for participation deleting", id);

            try
            {
                await this.participationService.DeleteParticipationAsync(id);
            }
            catch (ParticipationWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.NoContent();
        }
    }
}