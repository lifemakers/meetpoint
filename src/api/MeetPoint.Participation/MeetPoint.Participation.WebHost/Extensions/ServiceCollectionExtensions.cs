using System;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using IdentityModel;
using IdentityModel.Client;
using MassTransit;
using MediatR;
using MeetPoint.Contract;
using MeetPoint.Participation.Application;
using MeetPoint.Participation.Application.Services;
using MeetPoint.Participation.Core.Domain.Constants;
using MeetPoint.Participation.Core.Domain.Interfaces;
using MeetPoint.Participation.Core.Domain.Interfaces.Db;
using MeetPoint.Participation.Core.Domain.Interfaces.IntegratedServices;
using MeetPoint.Participation.Core.Domain.Interfaces.Services;
using MeetPoint.Participation.Core.Domain.Options;
using MeetPoint.Participation.Core.Domain.Services;
using MeetPoint.Participation.DataAccess;
using MeetPoint.Participation.DataAccess.Configuration;
using MeetPoint.Participation.DataAccess.Repositories;
using MeetPoint.Participation.IntegratedServices.Options;
using MeetPoint.Participation.IntegratedServices.Producers;
using MeetPoint.Participation.IntegratedServices.Services;
using MeetPoint.Participation.WebHost.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Polly;

namespace MeetPoint.Participation.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccessServices(this IServiceCollection services)
        {
            MongoDbPersistence.Configure();

            services.AddScoped<IMongoDbContext, MongoDbContext>();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IParticipationRepository, ParticipationRepository>();
        }

        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IDomainEventDispatcher, MediatrDomainEventDispatcher>();
            services.AddMediatR(typeof(MediatrDomainEventDispatcher).GetTypeInfo().Assembly);

            services.AddScoped<IParticipationService, ParticipationService>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentDateProvider, CurrentDateProvider>();
            services.AddTransient<IEmailService, EmailService>();
        }

        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddTransient<ISendEmailProducer, SendEmailProducer>();
            services.AddTransient<ICreateEventParticipationProducer, CreateEventParticipationProducer>();
            services.AddTransient<ICreateUserParticipationProducer, CreateUserParticipationProducer>();
            services.AddTransient<IRemoveEventParticipationProducer, RemoveEventParticipationProducer>();
            services.AddTransient<IRemoveUserParticipationProducer, RemoveUserParticipationProducer>();
        }

        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DatabaseOptions>(configuration.GetSection(DatabaseOptions.Position));
            services.Configure<AzureBlobStorageOptions>(configuration.GetSection(AzureBlobStorageOptions.Position));
            services.Configure<JwtAuthenticationOptions>(configuration.GetSection(JwtAuthenticationOptions.Position));
            services.Configure<ApiManagementOptions>(configuration.GetSection(ApiManagementOptions.Position));
            services.Configure<MessageBrokerOptions>(configuration.GetSection(MessageBrokerOptions.Position));
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint Participation API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }

        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            var jwtAuthenticationOptions = services.GetOptions<JwtAuthenticationOptions>();

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = jwtAuthenticationOptions.Authority;
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                    };
                });
        }

        public static void AddJwtAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api.participation");
                });
            });
        }

        public static void AddMassTransitServices(this IServiceCollection services)
        {
            var messageBrokerOptions = services.GetOptions<MessageBrokerOptions>();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(messageBrokerOptions.ConnectionString, h =>
                    {
                        h.Username(messageBrokerOptions.Username);
                        h.Password(messageBrokerOptions.Password);
                    });

                    var sendEmailUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.SendEmail}");
                    var createEventParticipationUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.CreateEventParticipation}");
                    var createUserParticipationUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.CreateUserParticipation}");
                    var removeEventParticipationUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.RemoveEventParticipation}");
                    var removeUserParticipationUri = new Uri($"{messageBrokerOptions.ConnectionString}/{MessageBrokerEndpointRouteConstants.RemoveUserParticipation}");

                    EndpointConvention.Map<ISendEmailContract>(sendEmailUri);
                    EndpointConvention.Map<ICreateEventParticipationContract>(createEventParticipationUri);
                    EndpointConvention.Map<ICreateUserParticipationContract>(createUserParticipationUri);
                    EndpointConvention.Map<IRemoveEventParticipationContract>(removeEventParticipationUri);
                    EndpointConvention.Map<IRemoveUserParticipationContract>(removeUserParticipationUri);

                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        private static TOptions GetOptions<TOptions>(this IServiceCollection services)
            where TOptions : class, new()
        {
            return (services
                .BuildServiceProvider()
                .GetService<IOptions<TOptions>>()
                    ?? throw new NullReferenceException(nameof(TOptions)))
                .Value;
        }
    }
}