using System;

namespace MeetPoint.Contract
{
    public interface IRemoveTagContract
    {
        public Guid TagId { get; set; }
    }
}