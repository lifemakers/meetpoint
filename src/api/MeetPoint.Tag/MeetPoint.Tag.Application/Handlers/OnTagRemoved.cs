using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MeetPoint.Tag.Core.Domain.DomainEvents;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.Core.Domain.Models.Contracts;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Tag.Application.Handlers
{
    public class OnTagRemoved
    {
        public class Handler : INotificationHandler<DomainEventNotification<TagRemovedDomainEvent>>
        {
            private readonly IRemoveTagProducer removeTagProducer;
            private readonly ILogger<Handler> logger;

            public Handler(ILogger<Handler> logger,
                IRemoveTagProducer removeTagProducer)
            {
                this.logger = logger;
                this.removeTagProducer = removeTagProducer;
            }

            public async Task Handle(DomainEventNotification<TagRemovedDomainEvent> notification, CancellationToken cancellationToken)
            {
                var domainEvent = notification.DomainEvent;
                var tagId = domainEvent.TagId;

                try
                {
                    this.logger.LogDebug($"Handling Domain Event. {domainEvent}");

                    // send integration event for tag removing
                    var removeTagContract = new RemoveTagContract
                    {
                        TagId = tagId,
                    };
                    await this.removeTagProducer.RemoveTag(removeTagContract);
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, $"Error Handling Domain Event. {domainEvent}");
                    throw;
                }
            }
        }
    }
}