using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Exceptions;
using MeetPoint.Tag.Core.Domain.Interfaces;
using MeetPoint.Tag.Core.Domain.Interfaces.Repositories;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.Core.Domain.Mappings;
using MeetPoint.Tag.Core.Domain.Models;
using MeetPoint.Tag.Core.Domain.Models.Dtos;

namespace MeetPoint.Tag.Application.Services
{
    public class TagService : ITagService
    {
        private readonly ITagRepository tagRepository;
        private readonly ICurrentDateProvider currentDateProvider;

        public TagService(ITagRepository tagRepository,
            ICurrentDateProvider currentDateProvider)
        {
            this.tagRepository = tagRepository;
            this.currentDateProvider = currentDateProvider;
        }

        public async Task<IEnumerable<TagForListDto>> GetAllTagsAsync()
        {
            // TODO implement pagination model (probably with pagination token)
            return await this.tagRepository.GetAllAsync();
        }

        public async Task<TagDto> CreateTagAsync(CreateTagDto createTagDto)
        {
            var tag = new Core.Domain.Aggregates.Tag.Tag(new ValidModel<CreateTagDto>(createTagDto),
                this.currentDateProvider.GetDate());

            if (await this.tagRepository.GetByNameAsync(createTagDto.Name) != null)
            {
                throw new TagAlreadyExistsException();
            }

            await this.tagRepository.CreateAsync(tag);

            await this.tagRepository.SaveChangesAsync();

            var tagDtoResponse = tag.ToTagDto();

            return tagDtoResponse;
        }

        public async Task<TagDto> GetTagByIdAsync(Guid id)
        {
            var tag = await this.tagRepository.GetByIdAsync(id);

            if (tag == null)
            {
                throw new TagWasNotFoundException();
            }

            var tagDtoResponse = tag.ToTagDto();

            return tagDtoResponse;
        }

        public async Task EditTagAsync(Guid tagId, EditTagDto editTagDto)
        {
            var tag = await this.tagRepository.GetByIdAsync(tagId);

            if (tag == null)
            {
                throw new TagWasNotFoundException();
            }

            tag.Edit(new ValidModel<EditTagDto>(editTagDto),
                this.currentDateProvider.GetDate());

            await this.tagRepository.UpdateAsync(tag);

            await this.tagRepository.SaveChangesAsync();
        }

        public async Task DeleteTagAsync(Guid tagId)
        {
            var tag = await this.tagRepository.GetByIdAsync(tagId);

            if (tag == null)
            {
                throw new TagWasNotFoundException();
            }

            await this.tagRepository.DeleteAsync(tag);

            tag.PublishTagRemovedDomainEvent();

            await this.tagRepository.SaveChangesAsync();
        }
    }
}