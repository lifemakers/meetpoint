using System;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MeetPoint.Tag.Core.Domain.Interfaces;
using Newtonsoft.Json;

namespace MeetPoint.Tag.Core.Domain.Aggregates
{
    public abstract class DomainEntity : IDomainEntity
    {
        [JsonIgnore]
        [NotMapped]
        private readonly ConcurrentQueue<IDomainEvent> domainEvents = new ConcurrentQueue<IDomainEvent>();

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; protected set; }

        public DateTime DatePost { get; protected set; }

        public DateTime DateLastUpdated { get; protected set; }

        [JsonIgnore]
        [NotMapped]
        public IProducerConsumerCollection<IDomainEvent> DomainEvents => domainEvents;

        protected void PublishEvent(IDomainEvent @event)
        {
            domainEvents.Enqueue(@event);
        }

        protected Guid CreateNewId()
        {
            return Guid.NewGuid();
        }
    }
}