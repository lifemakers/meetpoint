using System;
using MeetPoint.Tag.Core.Domain.DomainEvents;
using MeetPoint.Tag.Core.Domain.Interfaces;
using MeetPoint.Tag.Core.Domain.Models.Dtos;

namespace MeetPoint.Tag.Core.Domain.Aggregates.Tag
{
    public class Tag : DomainEntity, IAggregateRoot
    {
        public string Name { get; private set; }

        protected Tag()
        {
        }

        public Tag(IValidModel<CreateTagDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = this.CreateNewId();
            this.Name = validModel.Value.Name;
            this.DatePost = currentDate;
            this.DateLastUpdated = currentDate;
        }

        public void Edit(IValidModel<EditTagDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Name = validModel.Value.Name;
            this.DateLastUpdated = currentDate;
        }

        public void PublishTagRemovedDomainEvent()
        {
            this.PublishEvent(new TagRemovedDomainEvent(this.Id));
        }
    }
}