namespace MeetPoint.Tag.Core.Domain.Constants
{
    public static class CacheKeyConstants
    {
        private const string Separator = ":";
        private const string ServiceName = "MeetPoint.Tag";

        public const string GetAllTags = ServiceName + Separator + "GetAllTags";
    }
}