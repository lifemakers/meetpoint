using System;
using MeetPoint.Tag.Core.Domain.Interfaces;

namespace MeetPoint.Tag.Core.Domain.DomainEvents
{
    public class TagRemovedDomainEvent : IDomainEvent
    {
        public Guid TagId { get; private set; }

        public TagRemovedDomainEvent(Guid tagId)
        {
            this.TagId = tagId;
        }

        public override string ToString()
        {
            return $"Type: {this.GetType().Name}," +
                   $"TagId: {this.TagId}";
        }
    }
}