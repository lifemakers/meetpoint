using System;
using MeetPoint.Tag.Core.Domain.Enums;
using MeetPoint.Tag.Core.Domain.Extensions;

namespace MeetPoint.Tag.Core.Domain.Exceptions
{
    public class BaseAppException : Exception
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.Unknown;

        public virtual int ErrorCode => (int)ErrorCodeValue;

        public BaseAppException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}