using MeetPoint.Tag.Core.Domain.Enums;
using MeetPoint.Tag.Core.Domain.Extensions;

namespace MeetPoint.Tag.Core.Domain.Exceptions
{
    public class TagAlreadyExistsException : BaseAppException
    {
        private const ErrorCodes.Tag ErrorCodeValue = ErrorCodes.Tag.TagAlreadyExists;

        public override int ErrorCode => (int)ErrorCodeValue;

        public TagAlreadyExistsException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public TagAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}