using MeetPoint.Tag.Core.Domain.Enums;
using MeetPoint.Tag.Core.Domain.Extensions;

namespace MeetPoint.Tag.Core.Domain.Exceptions
{
    public class TagWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.Tag ErrorCodeValue = ErrorCodes.Tag.TagWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public TagWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public TagWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}