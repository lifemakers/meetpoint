using System;

namespace MeetPoint.Tag.Core.Domain.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}