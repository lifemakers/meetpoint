using System.Threading.Tasks;

namespace MeetPoint.Tag.Core.Domain.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}