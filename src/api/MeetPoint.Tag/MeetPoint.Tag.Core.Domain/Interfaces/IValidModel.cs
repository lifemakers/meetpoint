namespace MeetPoint.Tag.Core.Domain.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}