using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Models.Dtos;

namespace MeetPoint.Tag.Core.Domain.Interfaces.Repositories
{
    public interface ITagRepository : IRepository<Aggregates.Tag.Tag>
    {
        Task<IEnumerable<TagForListDto>> GetAllAsync();

        Task<TagDto> GetByNameAsync(string name);
    }
}