namespace MeetPoint.Tag.Core.Domain.Interfaces.Services
{
    public interface ICacheService
    {
        T Get<T>(string key);

        T Set<T>(string key, T value, string profileName);
    }
}