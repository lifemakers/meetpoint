using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Models.Contracts;

namespace MeetPoint.Tag.Core.Domain.Interfaces.Services
{
    public interface IRemoveTagProducer
    {
        Task RemoveTag(RemoveTagContract removeTagContract);
    }
}