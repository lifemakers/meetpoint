using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Models.Dtos;

namespace MeetPoint.Tag.Core.Domain.Interfaces.Services
{
    public interface ITagService
    {
        Task<IEnumerable<TagForListDto>> GetAllTagsAsync();

        Task<TagDto> CreateTagAsync(CreateTagDto createTagDto);

        Task<TagDto> GetTagByIdAsync(Guid id);

        Task EditTagAsync(Guid tagId, EditTagDto editTagDto);

        Task DeleteTagAsync(Guid tagId);
    }
}