using System.Collections.Generic;
using MeetPoint.Tag.Core.Domain.Models.Dtos;

namespace MeetPoint.Tag.Core.Domain.Mappings
{
    public static class TagDtoMapping
    {
        public static List<TagDto> ToTagDto(this List<Aggregates.Tag.Tag> tags)
        {
            var tagDtos = new List<TagDto>();

            foreach (var tag in tags)
            {
                tagDtos.Add(@tag.ToTagDto());
            }

            return tagDtos;
        }

        public static TagDto ToTagDto(this Aggregates.Tag.Tag tag)
        {
            return new TagDto
            {
                Id = tag.Id,
                Name = tag.Name,
            };
        }
    }
}