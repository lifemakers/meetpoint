using System;
using MeetPoint.Contract;

namespace MeetPoint.Tag.Core.Domain.Models.Contracts
{
    public class RemoveTagContract : IRemoveTagContract
    {
        public Guid TagId { get; set; }
    }
}