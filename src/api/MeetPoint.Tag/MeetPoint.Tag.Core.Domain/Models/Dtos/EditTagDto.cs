using System.ComponentModel.DataAnnotations;
using MeetPoint.Tag.Core.Domain.Constants;

namespace MeetPoint.Tag.Core.Domain.Models.Dtos
{
    public class EditTagDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Name { get; set; }
    }
}