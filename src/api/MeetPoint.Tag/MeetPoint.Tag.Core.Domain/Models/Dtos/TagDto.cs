using System;

namespace MeetPoint.Tag.Core.Domain.Models.Dtos
{
    public class TagDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}