using System;

namespace MeetPoint.Tag.Core.Domain.Models.Dtos
{
    public class TagForListDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}