using System;
using MeetPoint.Tag.Core.Domain.Interfaces;

namespace MeetPoint.Tag.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}