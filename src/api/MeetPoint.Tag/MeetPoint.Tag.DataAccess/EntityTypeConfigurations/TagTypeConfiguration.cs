﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MeetPoint.Tag.DataAccess.EntityTypeConfigurations
{
    public class TagTypeConfiguration : IEntityTypeConfiguration<Core.Domain.Aggregates.Tag.Tag>
    { // TODO to end db schema configuration
        public void Configure(EntityTypeBuilder<Core.Domain.Aggregates.Tag.Tag> builder)
        {
            builder
                .Property(c => c.Name)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}