using System;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Aggregates;
using MeetPoint.Tag.Core.Domain.Constants;
using MeetPoint.Tag.Core.Domain.Interfaces.Repositories;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.Tag.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : DomainEntity
    {
        protected ICacheService CacheService { get; private set; }

        protected DataContext DataContext { get; private set; }

        public Repository(DataContext dataContext, ICacheService cacheService)
        {
            this.DataContext = dataContext;
            this.CacheService = cacheService;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await this.DataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task CreateAsync(T item)
        {
            await this.DataContext.Set<T>().AddAsync(item);
        }

        public async Task UpdateAsync(T item)
        {
            this.DataContext.Set<T>().Update(item);
        }

        public async Task DeleteAsync(T item)
        {
            this.DataContext.Set<T>().Remove(item);
        }

        public async Task SaveChangesAsync()
        {
            await this.DataContext.SaveChangesAsync();
        }
    }
}