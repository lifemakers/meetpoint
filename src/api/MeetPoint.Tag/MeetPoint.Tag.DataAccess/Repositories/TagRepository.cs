using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Constants;
using MeetPoint.Tag.Core.Domain.Interfaces.Repositories;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.Core.Domain.Models.Dtos;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.Tag.DataAccess.Repositories
{
    public class TagRepository : Repository<Core.Domain.Aggregates.Tag.Tag>, ITagRepository
    {
        public TagRepository(DataContext dataContext, ICacheService cacheService)
            : base(dataContext, cacheService)
        {
        }

        public async Task<IEnumerable<TagForListDto>> GetAllAsync()
        {
            var cacheKey = CacheKeyConstants.GetAllTags;
            var cached = this.CacheService.Get<IEnumerable<TagForListDto>>(cacheKey);

            if (cached != null)
            {
                return cached;
            }
            else
            {
                var result = await this.DataContext
                    .Tags
                    .AsNoTracking()
                    .Select(x => new TagForListDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                    })
                    .OrderByDescending(x => x.Name)
                    .ToListAsync();

                return this.CacheService
                    .Set<IEnumerable<TagForListDto>>(cacheKey, result, CacheProfileNameConstants.GetAllTags);
            }
        }

        public async Task<TagDto> GetByNameAsync(string name)
        {
            return await this.DataContext
                .Tags
                .AsNoTracking()
                .Where(x => x.Name == name)
                .Select(x => new TagDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                })
                .SingleOrDefaultAsync();
        }
    }
}