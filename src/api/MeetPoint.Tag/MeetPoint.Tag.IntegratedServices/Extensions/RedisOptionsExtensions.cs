using System;
using System.Linq;
using MeetPoint.Tag.IntegratedServices.Options;

namespace MeetPoint.Tag.IntegratedServices.Extensions
{
    public static class RedisOptionsExtensions
    {
        public static CacheProfileOptions GetCacheProfileOptions(this RedisOptions options, string name) =>
            options
                .CacheProfiles
                .SingleOrDefault(x => x.Name.Equals(name))
            ?? throw new ArgumentException(nameof(name));
    }
}