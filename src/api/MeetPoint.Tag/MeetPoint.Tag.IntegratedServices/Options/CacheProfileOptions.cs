using MeetPoint.Tag.Core.Domain.Constants;

namespace MeetPoint.Tag.IntegratedServices.Options
{
    public class CacheProfileOptions
    {
        public string Name { get; set; }

        public int? AbsoluteExpirationSeconds { get; set; }

        public int? SlidingExpiration { get; set; }
    }
}