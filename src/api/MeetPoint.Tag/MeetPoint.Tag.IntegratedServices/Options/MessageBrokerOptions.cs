namespace MeetPoint.Tag.IntegratedServices.Options
{
    public class MessageBrokerOptions
    {
        public const string Position = "MessageBroker";

        public string ConnectionString { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}