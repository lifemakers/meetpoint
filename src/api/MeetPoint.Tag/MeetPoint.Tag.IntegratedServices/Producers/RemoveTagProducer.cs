using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.Core.Domain.Models.Contracts;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Tag.IntegratedServices.Producers
{
    public class RemoveTagProducer : IRemoveTagProducer
    {
        private readonly ISendEndpointProvider sendEndpointProvider;
        private readonly ILogger<RemoveTagProducer> logger;

        public RemoveTagProducer(ISendEndpointProvider sendEndpointProvider,
            ILogger<RemoveTagProducer> logger)
        {
            this.sendEndpointProvider = sendEndpointProvider;
            this.logger = logger;
        }

        public async Task RemoveTag(RemoveTagContract removeTagContract)
        {
            this.logger.LogInformation("Sending command for removing tag");

            await this.sendEndpointProvider.Send<IRemoveTagContract>(removeTagContract);

            this.logger.LogInformation("Sent command successfully for removing tag");
        }
    }
}