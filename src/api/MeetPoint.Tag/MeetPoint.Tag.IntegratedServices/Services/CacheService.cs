using System;
using MassTransit.Configuration;
using MeetPoint.Tag.Core.Domain.Constants;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.IntegratedServices.Extensions;
using MeetPoint.Tag.IntegratedServices.Options;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace MeetPoint.Tag.IntegratedServices.Services
{
    public class CacheService : ICacheService
    {
        private readonly IDistributedCache cache;
        private readonly ILogger<CacheService> logger;
        private readonly RedisOptions redisOptions;

        public CacheService(IDistributedCache cache,
            IOptions<RedisOptions> redisOptions,
            ILogger<CacheService> logger)
        {
            this.cache = cache;
            this.redisOptions = redisOptions.Value;
            this.logger = logger;
        }

        public T Get<T>(string key)
        {
            var value = this.cache.GetString(key);

            if (value != null)
            {
                this.logger.LogDebug("Retrieved value from cache. Key: {@Key}", key);
                return JsonConvert.DeserializeObject<T>(value, new JsonSerializerSettings
                {
                    ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                    ContractResolver = new NonPublicPropertiesResolver(),
                });
            }

            this.logger.LogDebug("Not retrieved value from cache. Key: {@Key}", key);
            return default;
        }

        public T Set<T>(string key, T value, string profileName)
        {
            var options = this.GetDistributedCacheEntryOptions(profileName);

            this.cache.SetString(key, JsonConvert.SerializeObject(value), options);

            this.logger.LogDebug("Value has been cached. Key: {@Key}", key);

            return value;
        }

        private DistributedCacheEntryOptions GetDistributedCacheEntryOptions(string profileName)
        {
            var cacheProfileOptions = this.redisOptions.GetCacheProfileOptions(profileName);

            if (cacheProfileOptions == null)
            {
                throw new NullReferenceException(nameof(cacheProfileOptions));
            }

            var options = new DistributedCacheEntryOptions();

            if (cacheProfileOptions.AbsoluteExpirationSeconds.HasValue)
            {
                options.AbsoluteExpirationRelativeToNow = TimeSpan
                    .FromSeconds(cacheProfileOptions.AbsoluteExpirationSeconds.Value);
            }

            if (cacheProfileOptions.SlidingExpiration.HasValue)
            {
                options.SlidingExpiration = TimeSpan
                    .FromSeconds(cacheProfileOptions.SlidingExpiration.Value);
            }

            return options;
        }
    }
}