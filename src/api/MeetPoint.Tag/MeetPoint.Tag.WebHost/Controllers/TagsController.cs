using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Constants;
using MeetPoint.Tag.Core.Domain.Exceptions;
using MeetPoint.Tag.Core.Domain.Interfaces.Services;
using MeetPoint.Tag.Core.Domain.Models;
using MeetPoint.Tag.Core.Domain.Models.Dtos;
using MeetPoint.Tag.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.Tag.WebHost.Controllers
{
    /// <summary> Tags Management. </summary>
    [ApiController]
    [Authorize]
    [Route("api/v1/tags")]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class TagsController : Controller
    {
        private readonly ITagService tagService;
        private readonly ILogger<TagsController> logger;

        public TagsController(ITagService tagService,
            ILogger<TagsController> logger)
        {
            this.tagService = tagService;
            this.logger = logger;
        }

        /// <summary> Get all tags. </summary>
        [HttpGet]
        [ProducesResponseType(typeof(List<TagForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<TagForListDto>>> GetAllTagsAsync()
        {
            var response = await this.tagService.GetAllTagsAsync();

            return this.Ok(response);
        }

        /// <summary> Get tag by id. </summary>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(typeof(TagDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TagDto>> GetTagByIdAsync(Guid id)
        {
            try
            {
                return this.Ok(await this.tagService.GetTagByIdAsync(id));
            }
            catch (TagWasNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary> Create new tag. </summary>
        [HttpPost]
        [ProducesResponseType(typeof(TagDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TagDto>> CreateTagAsync([FromForm] CreateTagDto request)
        {
            this.logger.LogInformation("CreateTagDto: {@CreateTagDto} requested for tag creating", request);

            var response = await this.tagService.CreateTagAsync(request);

            this.logger.LogInformation("TagDto: {@TagDto} response after tag created", response);

            return CreatedAtAction(nameof(GetTagByIdAsync), new { id = response.Id }, response.Id);
        }

        /// <summary> Update tag. </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponseExtended), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EditTagAsync(Guid tagId, [FromForm] EditTagDto request)
        {
            this.logger.LogInformation("EditTagDto: {@EditTagDto} requested for tag editing", request);

            try
            {
                await this.tagService.EditTagAsync(tagId, request);
            }
            catch (TagWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.Ok();
        }

        /// <summary> Delete tag. </summary>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteTagAsync(Guid id)
        {
            this.logger.LogInformation("DeleteTag Id: {@id} requested for tag deleting", id);

            try
            {
                await this.tagService.DeleteTagAsync(id);
            }
            catch (TagWasNotFoundException)
            {
                return this.NotFound();
            }

            return this.NoContent();
        }
    }
}