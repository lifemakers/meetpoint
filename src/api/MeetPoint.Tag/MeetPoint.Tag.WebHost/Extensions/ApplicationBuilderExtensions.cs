using MeetPoint.Tag.WebHost.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace MeetPoint.Tag.WebHost.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}