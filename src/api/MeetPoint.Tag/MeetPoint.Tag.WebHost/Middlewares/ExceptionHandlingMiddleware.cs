using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using MeetPoint.Tag.Core.Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MeetPoint.Tag.WebHost.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context,
            ILogger<ExceptionHandlingMiddleware> logger,
            IWebHostEnvironment hostEnvironment)
        {
            try
            {
                await this.next(context);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    logger.LogWarning(ex,
                        "The response has already started, the http status code middleware will not be executed.");

                    throw;
                }

                context.Response.Clear();
                context.Response.ContentType = "application/json";

                string errorResponseText;
                int statusCode;
                var serializeSettings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore,
                };

                if (ex is ValidationException validation)
                {
                    var errorResponse = new ErrorResponseExtended();
                    statusCode = StatusCodes.Status422UnprocessableEntity;
                    errorResponse.ErrorMessage = validation.ValidationResult.ErrorMessage;
                    errorResponseText = JsonConvert.SerializeObject(errorResponse, serializeSettings);
                }
                else
                {
                    var errorResponse = new ErrorResponse();
                    statusCode = StatusCodes.Status500InternalServerError;

                    var isDevelopment = hostEnvironment.IsDevelopment();

                    errorResponse.ErrorMessage = isDevelopment ? ex.Message : "An error occurred during processing your request";
                    errorResponse.StackTrace = isDevelopment ? ex.StackTrace : null;
                    errorResponseText = JsonConvert.SerializeObject(errorResponse, serializeSettings);
                }

                context.Response.StatusCode = statusCode;

                await context.Response.WriteAsync(errorResponseText);
            }
        }
    }
}