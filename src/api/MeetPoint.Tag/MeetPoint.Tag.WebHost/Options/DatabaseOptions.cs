namespace MeetPoint.Tag.WebHost.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}