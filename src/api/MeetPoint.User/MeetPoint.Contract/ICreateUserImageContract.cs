using System;

namespace MeetPoint.Contract
{
    public interface ICreateUserImageContract
    {
        public Guid UserId { get; set; }

        public Guid ImageId { get; set; }

        public string Url { get; set; }
    }
}