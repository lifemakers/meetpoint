using System;

namespace MeetPoint.Contract
{
    public interface IRemoveUserImageContract
    {
        public Guid UserId { get; set; }

        public Guid ImageId { get; set; }
    }
}