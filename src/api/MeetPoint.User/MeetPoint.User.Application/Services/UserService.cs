using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Aggregates.User;
using MeetPoint.User.Core.Domain.Exceptions.User;
using MeetPoint.User.Core.Domain.Extensions.PostConfigureActions;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly ICurrentDateProvider currentDateProvider;
        private readonly IUriBuilderService uriBuilderService;

        public UserService(IUserRepository userRepository,
            ICurrentDateProvider currentDateProvider,
            IUriBuilderService uriBuilderService)
        {
            this.userRepository = userRepository;
            this.currentDateProvider = currentDateProvider;
            this.uriBuilderService = uriBuilderService;
        }

        /// <summary> Get all users. </summary>
        public async Task<IEnumerable<UserForListDto>> GetAllUsersAsync()
        {
            // TODO implement pagination model (probably with pagination token)
            return await this.userRepository.GetAllAsync();
        }

        /// <summary> Add a new user. </summary>
        public async Task CreateUserAsync(CreateUserDto createUserDto)
        {
            var user = new Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(createUserDto),
                this.currentDateProvider.GetDate());

            // create user
            await this.userRepository.CreateAsync(user);

            // commit transaction
            await this.userRepository.SaveChangesAsync();
        }

        public async Task<UserDto> GetUserByIdAsync(Guid id)
        {
            // get user by id
            var user = await this.userRepository.GetByIdAsync(id);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            // prepare response
            var userResponse = user
                .ToUserDto();

            return userResponse;
        }

        public async Task EditUserAsync(Guid userId, EditUserDto editUserDto)
        {
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            user.Edit(new ValidModel<EditUserDto>(editUserDto),
                this.currentDateProvider.GetDate());

            await this.userRepository.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(Guid userId)
        {
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            await this.userRepository.DeleteAsync(user);

            await this.userRepository.SaveChangesAsync();
        }

        public async Task AddImageAsync(CreateUserImageDto createUserImageDto)
        {
            var userImage = new UserImage(new ValidModel<CreateUserImageDto>(createUserImageDto),
                this.currentDateProvider.GetDate());
            var imageId = createUserImageDto.ImageId;
            var userId = createUserImageDto.UserId;
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            if (user.ContainsImage(imageId))
            {
                throw new UserImageAlreadyExistsException();
            }

            user.AddImage(userImage, this.currentDateProvider.GetDate());

            await this.userRepository.SaveChangesAsync();
        }

        public async Task RemoveImageAsync(Guid userId, Guid imageId)
        {
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            var removed = !user.RemoveImage(imageId, this.currentDateProvider.GetDate());

            if (!removed)
            {
                throw new UserImageHasNotBeenDeletedException();
            }

            await this.userRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserImageForListDto>> GetAllImagesAsync(Guid userId)
        {
            return (await this.userRepository.GetAllImagesAsync(userId))
                .PostConfigureImages(x => x.Url = this.uriBuilderService.BuildUri(x.Url));
        }

        public async Task AddParticipationAsync(CreateUserParticipationDto createUserParticipationDto)
        {
            var userParticipation = new UserParticipation(new ValidModel<CreateUserParticipationDto>(createUserParticipationDto),
                this.currentDateProvider.GetDate());
            var participationId = createUserParticipationDto.ParticipationId;
            var userId = createUserParticipationDto.UserId;
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            if (user.ContainsParticipation(participationId))
            {
                throw new UserParticipationAlreadyExistsException();
            }

            user.AddParticipation(userParticipation, this.currentDateProvider.GetDate());

            await this.userRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserParticipationForListDto>> GetAllParticipationsAsync(Guid userId)
        {
            return await this.userRepository.GetAllParticipationsAsync(userId);
        }

        public async Task RemoveParticipationAsync(Guid userId, Guid participationId)
        {
            var user = await this.userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            var removed = !user.RemoveParticipation(participationId, this.currentDateProvider.GetDate());

            if (!removed)
            {
                throw new UserParticipationHasNotBeenDeletedException();
            }

            await this.userRepository.SaveChangesAsync();
        }
    }
}