using MeetPoint.User.Core.Domain.Interfaces;

namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public class AdditionalInfo : IValueObject
    {
        public string Phone { get; private set; }

        public string Email { get; private set; }

        public int Age { get; private set; }

        protected AdditionalInfo()
        {
        }

        public AdditionalInfo(string phone, string email, int age)
        {
            this.Phone = phone;
            this.Email = email;
            this.Age = age;
        }

        public void Update(string phone, string email, int age)
        {
            this.Phone = phone;
            this.Email = email;
            this.Age = age;
        }
    }
}