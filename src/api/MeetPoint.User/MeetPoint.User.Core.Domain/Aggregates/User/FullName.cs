using MeetPoint.User.Core.Domain.Interfaces;

namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public class FullName : IValueObject
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Value => $"{this.FirstName} {this.LastName}";

        protected FullName()
        {
        }

        public FullName(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public void Update(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}