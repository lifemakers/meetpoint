namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public enum ParticipationRole
    {
        Participant = 0,
        Moderator = 1,
        Owner = 2,
    }
}