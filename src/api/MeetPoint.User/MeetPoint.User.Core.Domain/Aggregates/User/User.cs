using System;
using System.Collections.Generic;
using System.Linq;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public class User : DomainEntity, IAggregateRoot
    {
        private readonly List<UserImage> userImages = new List<UserImage>();

        private readonly List<UserParticipation> userParticipations = new List<UserParticipation>();

        public string Username { get; private set; }

        public virtual FullName FullName { get; private set; }

        public virtual AdditionalInfo AdditionalInfo { get; private set; }

        public virtual IEnumerable<UserImage> UserImages => this.userImages;

        public virtual IEnumerable<UserParticipation> UserParticipations => this.userParticipations;

        public User(IValidModel<CreateUserDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = validModel.Value.UserId; // global user id
            this.Username = validModel.Value.Username;
            this.FullName = new FullName(validModel.Value.FirstName, validModel.Value.LastName);
            this.AdditionalInfo = new AdditionalInfo(validModel.Value.Phone, validModel.Value.Email, validModel.Value.Age);

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.DatePosted = currentDate;
            this.DateLastUpdated = DateLastUpdated;
        }

        protected User()
        {
        }

        public void Edit(IValidModel<EditUserDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.FullName.Update(validModel.Value.FirstName, validModel.Value.LastName);
            this.AdditionalInfo.Update(validModel.Value.Phone, validModel.Value.Email, validModel.Value.Age);
            this.DateLastUpdated = currentDate;
        }

        public void AddImage(UserImage userImage, DateTime currentDate)
        {
            this.userImages.Add(userImage);
            this.DateLastUpdated = currentDate;
        }

        public bool ContainsImage(Guid imageId)
        {
            return this.userImages
                .FirstOrDefault(x => x.Id == imageId) != null;
        }

        public bool RemoveImage(Guid imageId, DateTime currentDate)
        {
            var count = this.userImages
                .RemoveAll(x => x.Id == imageId);

            if (count == 1)
            {
                this.DateLastUpdated = currentDate;
                return true;
            }

            return false;
        }

        public void AddParticipation(UserParticipation userParticipation, DateTime currentDate)
        {
            this.userParticipations.Add(userParticipation);
            this.DateLastUpdated = currentDate;
        }

        public bool ContainsParticipation(Guid participationId)
        {
            return this.userParticipations
                .FirstOrDefault(x => x.Id == participationId) != null;
        }

        public bool RemoveParticipation(Guid participationId, DateTime currentDate)
        {
            var count = this.userParticipations
                .RemoveAll(x => x.Id == participationId);

            if (count == 1)
            {
                this.DateLastUpdated = currentDate;
                return true;
            }

            return false;
        }
    }
}