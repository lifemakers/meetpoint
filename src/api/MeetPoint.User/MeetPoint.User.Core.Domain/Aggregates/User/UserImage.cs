using System;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public class UserImage : DomainEntity
    {
        public Guid UserId { get; private set; }

        public string Url { get; private set; }

        public UserImage(IValidModel<CreateUserImageDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = validModel.Value.ImageId;
            this.Url = validModel.Value.Url;
            this.DatePosted = currentDate;
            this.DateLastUpdated = DateLastUpdated;
        }

        protected UserImage()
        {
        }
    }
}