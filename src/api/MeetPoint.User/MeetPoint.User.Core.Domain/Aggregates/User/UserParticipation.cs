using System;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Aggregates.User
{
    public class UserParticipation : DomainEntity
    {
        public Guid UserId { get; private set; }

        public Guid EventId { get; private set; }

        public ParticipationRole ParticipationRole { get; private set; }

        public UserParticipation(IValidModel<CreateUserParticipationDto> validModel, DateTime currentDate)
        {
            if (validModel == null)
            {
                throw new ArgumentNullException(nameof(validModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(nameof(currentDate));
            }

            this.Id = validModel.Value.ParticipationId;
            this.UserId = validModel.Value.UserId;
            this.EventId = validModel.Value.EventId;
            this.ParticipationRole = Enum.Parse<ParticipationRole>(validModel.Value.ParticipationRole);

            this.DatePosted = currentDate;
            this.DateLastUpdated = currentDate;
        }

        protected UserParticipation()
        {
        }
    }
}