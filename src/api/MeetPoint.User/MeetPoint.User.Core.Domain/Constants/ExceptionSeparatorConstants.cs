namespace MeetPoint.User.Core.Domain.Constants
{
    public static class ExceptionSeparatorConstants
    {
        public const string ErrorMessageSeparator = " --> ";
    }
}