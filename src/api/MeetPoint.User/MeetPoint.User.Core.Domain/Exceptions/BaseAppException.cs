using System;
using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions
{
    public class BaseAppException : Exception
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.Unknown;

        public virtual int ErrorCode => (int)ErrorCodeValue;

        public BaseAppException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}