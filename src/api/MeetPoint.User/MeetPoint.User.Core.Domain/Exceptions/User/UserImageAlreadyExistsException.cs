using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions.User
{
    public class UserImageAlreadyExistsException : BaseAppException
    {
        private const ErrorCodes.User ErrorCodeValue = ErrorCodes.User.UserImageAlreadyExists;

        public override int ErrorCode => (int)ErrorCodeValue;

        public UserImageAlreadyExistsException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public UserImageAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}