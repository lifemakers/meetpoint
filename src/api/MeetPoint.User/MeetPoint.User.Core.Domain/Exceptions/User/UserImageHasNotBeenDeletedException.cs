using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions.User
{
    public class UserImageHasNotBeenDeletedException : BaseAppException
    {
        private const ErrorCodes.User ErrorCodeValue = ErrorCodes.User.UserImageHasNotBeenDeleted;

        public override int ErrorCode => (int)ErrorCodeValue;

        public UserImageHasNotBeenDeletedException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public UserImageHasNotBeenDeletedException(string message)
            : base(message)
        {
        }
    }
}