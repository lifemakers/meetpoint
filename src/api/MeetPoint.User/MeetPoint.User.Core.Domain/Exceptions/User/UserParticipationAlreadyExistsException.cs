using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions.User
{
    public class UserParticipationAlreadyExistsException : BaseAppException
    {
        private const ErrorCodes.User ErrorCodeValue = ErrorCodes.User.UserParticipationAlreadyExists;

        public override int ErrorCode => (int)ErrorCodeValue;

        public UserParticipationAlreadyExistsException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public UserParticipationAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}