using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions.User
{
    public class UserParticipationHasNotBeenDeletedException : BaseAppException
    {
        private const ErrorCodes.User ErrorCodeValue = ErrorCodes.User.UserParticipationHasNotBeenDeleted;

        public override int ErrorCode => (int)ErrorCodeValue;

        public UserParticipationHasNotBeenDeletedException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public UserParticipationHasNotBeenDeletedException(string message)
            : base(message)
        {
        }
    }
}