using MeetPoint.User.Core.Domain.Enums;
using MeetPoint.User.Core.Domain.Extensions;

namespace MeetPoint.User.Core.Domain.Exceptions.User
{
    public class UserWasNotFoundException : BaseAppException
    {
        private const ErrorCodes.User ErrorCodeValue = ErrorCodes.User.UserWasNotFound;

        public override int ErrorCode => (int)ErrorCodeValue;

        public UserWasNotFoundException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public UserWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}