using System;
using System.Collections.Generic;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Extensions.PostConfigureActions
{
    public static class UserResponseExtensions
    {
        public static IEnumerable<UserImageForListDto> PostConfigureImages(this IEnumerable<UserImageForListDto> userImageForListDtos,
            Action<UserImageForListDto> configureAction)
        {
            foreach (var userImageForListDto in userImageForListDtos)
            {
                configureAction(userImageForListDto);
            }

            return userImageForListDtos;
        }
    }
}