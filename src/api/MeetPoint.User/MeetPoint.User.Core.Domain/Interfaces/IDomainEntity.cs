using System;
using System.Collections.Concurrent;

namespace MeetPoint.User.Core.Domain.Interfaces
{
    public interface IDomainEntity
    {
        Guid Id { get; }

        DateTime DatePosted { get; }

        DateTime DateLastUpdated { get; }

        IProducerConsumerCollection<IDomainEvent> DomainEvents { get; }
    }
}