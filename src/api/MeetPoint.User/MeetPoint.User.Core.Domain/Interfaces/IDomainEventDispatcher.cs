using System.Threading.Tasks;

namespace MeetPoint.User.Core.Domain.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}