namespace MeetPoint.User.Core.Domain.Interfaces.Repositories
{
    public interface IEfDbInitializer
    {
        void Initialize();

        void Clean();
    }
}