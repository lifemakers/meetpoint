using System;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Aggregates;

namespace MeetPoint.User.Core.Domain.Interfaces.Repositories
{
    public interface IRepository<T>
        where T : DomainEntity
    {
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task SaveChangesAsync();
    }
}