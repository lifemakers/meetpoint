using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<Aggregates.User.User>
    {
        Task<IEnumerable<UserForListDto>> GetAllAsync();

        Task<IEnumerable<UserImageForListDto>> GetAllImagesAsync(Guid userId);

        Task<IEnumerable<UserParticipationForListDto>> GetAllParticipationsAsync(Guid userId);
    }
}