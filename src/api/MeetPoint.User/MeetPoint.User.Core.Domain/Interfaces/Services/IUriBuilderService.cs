namespace MeetPoint.User.Core.Domain.Interfaces.Services
{
    public interface IUriBuilderService
    {
        string BuildUri(string url);
    }
}