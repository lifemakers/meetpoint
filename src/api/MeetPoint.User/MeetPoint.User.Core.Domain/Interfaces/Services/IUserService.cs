using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Interfaces.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserForListDto>> GetAllUsersAsync();

        Task CreateUserAsync(CreateUserDto createUserDto);

        Task<UserDto> GetUserByIdAsync(Guid id);

        Task EditUserAsync(Guid userId, EditUserDto editUserDto);

        Task DeleteUserAsync(Guid userId);

        Task AddImageAsync(CreateUserImageDto createUserImageDto);

        Task<IEnumerable<UserImageForListDto>> GetAllImagesAsync(Guid userId);

        Task RemoveImageAsync(Guid userId, Guid imageId);

        Task AddParticipationAsync(CreateUserParticipationDto createUserParticipationDto);

        Task<IEnumerable<UserParticipationForListDto>> GetAllParticipationsAsync(Guid userId);

        Task RemoveParticipationAsync(Guid userId, Guid participationId);
    }
}