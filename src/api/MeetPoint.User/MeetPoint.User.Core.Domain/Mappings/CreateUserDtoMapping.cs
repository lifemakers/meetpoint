using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class CreateUserDtoMapping
    {
        public static CreateUserDto ToCreateUserDto(this ICreateUserContract model)
        {
            return new CreateUserDto
            {
                UserId = model.UserId,
                Username = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Phone = model.Phone,
                Email = model.Email,
                Age = model.Age,
            };
        }
    }
}