using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class CreateUserImageDtoMapping
    {
        public static CreateUserImageDto ToCreateUserImageDto(this ICreateUserImageContract model)
        {
            return new CreateUserImageDto
            {
                UserId = model.UserId,
                ImageId = model.ImageId,
                Url = model.Url,
            };
        }
    }
}