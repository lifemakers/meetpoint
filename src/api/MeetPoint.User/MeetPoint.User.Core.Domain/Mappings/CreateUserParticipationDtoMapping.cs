using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class CreateUserParticipationDtoMapping
    {
        public static CreateUserParticipationDto ToCreateUserParticipationDto(this ICreateUserParticipationContract model)
        {
            return new CreateUserParticipationDto
            {
                EventId = model.EventId,
                ParticipationId = model.ParticipationId,
                ParticipationRole = model.ParticipationRole,
                UserId = model.UserId,
            };
        }
    }
}