using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class RemoveUserParticipationDtoMapping
    {
        public static RemoveUserParticipationDto ToRemoveUserParticipationDto(this IRemoveUserParticipationContract model)
        {
            return new RemoveUserParticipationDto
            {
                ParticipationId = model.ParticipationId,
                UserId = model.UserId,
            };
        }
    }
}