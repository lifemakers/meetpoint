using System.Collections.Generic;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class UserDtoMapping
    {
        public static IEnumerable<UserDto> ToUserDto(this IEnumerable<Aggregates.User.User> users)
        {
            var userDto = new List<UserDto>();

            foreach (var user in users)
            {
                userDto.Add(user.ToUserDto());
            }

            return userDto;
        }

        public static UserDto ToUserDto(this Aggregates.User.User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.FullName.FirstName,
                LastName = user.FullName.LastName,
                Phone = user.AdditionalInfo.Phone,
                Email = user.AdditionalInfo.Email,
                Age = user.AdditionalInfo.Age,
                UserImages = user.UserImages.ToUserImageDto(),
            };
        }
    }
}