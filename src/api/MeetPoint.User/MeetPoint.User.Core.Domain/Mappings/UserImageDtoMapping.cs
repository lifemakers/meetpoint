using System.Collections.Generic;
using MeetPoint.User.Core.Domain.Aggregates.User;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.Core.Domain.Mappings
{
    public static class UserImageDtoMapping
    {
        public static IEnumerable<UserImageForListDto> ToUserImageDto(this IEnumerable<UserImage> userImages)
        {
            var userImageDtos = new List<UserImageForListDto>();

            foreach (var userImage in userImages)
            {
                userImageDtos.Add(userImage.ToUserImageDto());
            }

            return userImageDtos;
        }

        public static UserImageForListDto ToUserImageDto(this UserImage userImage)
        {
            return new UserImageForListDto
            {
                UserId = userImage.UserId,
                ImageId = userImage.Id,
                Url = userImage.Url,
            };
        }
    }
}