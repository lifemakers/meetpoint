using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.User.Core.Domain.Constants;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class CreateUserImageDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid UserId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid ImageId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Url { get; set; }
    }
}