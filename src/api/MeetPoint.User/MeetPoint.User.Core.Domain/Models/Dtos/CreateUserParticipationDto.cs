using System;
using System.ComponentModel.DataAnnotations;
using MeetPoint.User.Core.Domain.Constants;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class CreateUserParticipationDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid EventId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid ParticipationId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public Guid UserId { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string ParticipationRole { get; set; }
    }
}