using System.ComponentModel.DataAnnotations;
using MeetPoint.User.Core.Domain.Constants;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class EditUserDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string LastName { get; set; }

        public string Phone { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Email { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public int Age { get; set; }
    }
}