using System;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class RemoveUserParticipationDto
    {
        public Guid UserId { get; set; }

        public Guid ParticipationId { get; set; }
    }
}