using System;
using System.Collections.Generic;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public IEnumerable<UserImageForListDto> UserImages { get; set; }
    }
}