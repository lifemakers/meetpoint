using System;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class UserForListDto
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string FullName { get; set; }
    }
}