using System;

namespace MeetPoint.User.Core.Domain.Models.Dtos
{
    public class UserImageForListDto
    {
        public Guid UserId { get; set; }

        public Guid ImageId { get; set; }

        public string Url { get; set; }
    }
}