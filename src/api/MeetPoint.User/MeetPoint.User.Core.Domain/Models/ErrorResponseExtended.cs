using System.Collections.Generic;

namespace MeetPoint.User.Core.Domain.Models
{
    public class ErrorResponseExtended : ErrorResponse
    {
        public IEnumerable<ValidationError> ValidationErrors { get; set; }
    }
}