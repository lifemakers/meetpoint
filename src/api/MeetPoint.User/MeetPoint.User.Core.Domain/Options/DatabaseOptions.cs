namespace MeetPoint.User.Core.Domain.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}