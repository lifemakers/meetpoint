namespace MeetPoint.User.Core.Domain.Options
{
    public class FileServerOptions
    {
        public const string Position = "FileServer";

        public string FileServerUri { get; set; }
    }
}