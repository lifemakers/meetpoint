namespace MeetPoint.User.Core.Domain.Options
{
    public class JwtAuthenticationOptions
    {
        public const string Position = "JwtAuthentication";

        public string Authority { get; set; }
    }
}