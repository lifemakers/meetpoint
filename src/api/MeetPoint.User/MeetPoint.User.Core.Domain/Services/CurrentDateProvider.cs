using System;
using MeetPoint.User.Core.Domain.Interfaces;

namespace MeetPoint.User.Core.Domain.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}