using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Options;
using Microsoft.Extensions.Options;

namespace MeetPoint.User.Core.Domain.Services
{
    public class UriBuilderService : IUriBuilderService
    {
        private readonly FileServerOptions options;

        public UriBuilderService(IOptions<FileServerOptions> options)
        {
            this.options = options.Value;
        }

        public string BuildUri(string url)
        {
            return $"{this.options.FileServerUri}/{url}";
        }
    }
}