using MeetPoint.User.Core.Domain.Interfaces.Repositories;

namespace MeetPoint.User.DataAccess.Data
{
    public class EfDbInitializer : IEfDbInitializer
    {
        private readonly DataContext dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Initialize()
        {
            this.dataContext.Database.EnsureDeleted();
            this.dataContext.Database.EnsureCreated();

            // this.dataContext.Users.AddRange(FakeDataFactory.Users);
            // this.dataContext.SaveChanges();
        }

        public void Clean()
        {
            this.dataContext.Database.EnsureDeleted();
        }
    }
}