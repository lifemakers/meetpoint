using System;
using System.Collections.Generic;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Core.Domain.Aggregates.User.User> Users => GetUsers();

        private static IEnumerable<Core.Domain.Aggregates.User.User> GetUsers()
        {
            var users = new List<Core.Domain.Aggregates.User.User>();

            var createUserDto1 = new CreateUserDto()
            {
                Username = "ivanov",
                FirstName = "Ivan",
                LastName = "Ivanov",
                Phone = "+380999999999",
                Email = "test111@gmail.com",
                Age = 23,
            };
            var createUserDto2 = new CreateUserDto()
            {
                Username = "stepanov",
                FirstName = "Stepan",
                LastName = "Stepanov",
                Phone = "+380222222222",
                Email = "test112@gmail.com",
                Age = 24,
            };
            var createUserDto3 = new CreateUserDto()
            {
                Username = "sidorov",
                FirstName = "Sidor",
                LastName = "Sidorov",
                Phone = "+380444444444",
                Email = "test333@gmail.com",
                Age = 25,
            };
            var createUserDto4 = new CreateUserDto()
            {
                Username = "petrov",
                FirstName = "Petro",
                LastName = "Petrov",
                Phone = "+380999999999",
                Email = "test666@gmail.com",
                Age = 33,
            };

            users.Add(new Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(createUserDto1), DateTime.Now));
            users.Add(new Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(createUserDto2), DateTime.Now));
            users.Add(new Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(createUserDto3), DateTime.Now));
            users.Add(new Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(createUserDto4), DateTime.Now));

            return users;
        }
    }
}