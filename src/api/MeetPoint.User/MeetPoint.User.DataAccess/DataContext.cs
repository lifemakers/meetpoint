using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Aggregates.User;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.DataAccess.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.User.DataAccess
{
    public class DataContext : DbContext
    {
        private readonly IDomainEventDispatcher dispatcher;

        public DbSet<Core.Domain.Aggregates.User.User> Users { get; set; }

        public DbSet<UserImage> UserImages { get; set; }

        public DbSet<UserParticipation> UserParticipations { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options,
            IDomainEventDispatcher dispatcher)
            : base(options)
        {
            this.dispatcher = dispatcher;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
        }

        public override int SaveChanges()
        {
            this.PreSaveChanges().GetAwaiter().GetResult();
            var res = base.SaveChanges();
            return res;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await this.PreSaveChanges();
            var res = await base.SaveChangesAsync(cancellationToken);
            return res;
        }

        private async Task PreSaveChanges()
        {
            await this.DispatchDomainEvents();
        }

        private async Task DispatchDomainEvents()
        {
            var domainEventEntities = ChangeTracker.Entries<IDomainEntity>()
                .Select(po => po.Entity)
                .Where(po => po.DomainEvents.Any())
                .ToArray();

            foreach (var entity in domainEventEntities)
            {
                IDomainEvent dev;
                while (entity.DomainEvents.TryTake(out dev))
                {
                    await dispatcher.Dispatch(dev);
                }
            }
        }
    }
}