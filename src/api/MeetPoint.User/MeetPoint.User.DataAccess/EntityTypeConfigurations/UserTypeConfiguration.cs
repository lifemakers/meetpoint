using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MeetPoint.User.DataAccess.EntityTypeConfigurations
{
    public class UserTypeConfiguration : IEntityTypeConfiguration<Core.Domain.Aggregates.User.User>
    { // TODO to end db schema configuration
        public void Configure(EntityTypeBuilder<Core.Domain.Aggregates.User.User> builder)
        {
            builder
                .HasMany(x => x.UserImages)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany(x => x.UserParticipations)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .OwnsOne(x => x.FullName);

            builder
                .OwnsOne(x => x.AdditionalInfo);
        }
    }
}