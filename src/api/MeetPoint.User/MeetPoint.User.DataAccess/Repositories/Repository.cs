using System;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Aggregates;
using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.User.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : DomainEntity
    {
        protected DataContext DataContext { get; private set; }

        public Repository(DataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await this.DataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task CreateAsync(T item)
        {
            await this.DataContext.Set<T>().AddAsync(item);
        }

        public async Task UpdateAsync(T item)
        {
            this.DataContext.Set<T>().Update(item);
        }

        public async Task DeleteAsync(T item)
        {
            this.DataContext.Set<T>().Remove(item);
        }

        public async Task SaveChangesAsync()
        {
            await this.DataContext.SaveChangesAsync();
        }
    }
}