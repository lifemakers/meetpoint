﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using MeetPoint.User.Core.Domain.Models.Dtos;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.User.DataAccess.Repositories
{
    public class UserRepository : Repository<Core.Domain.Aggregates.User.User>, IUserRepository
    {
        protected DataContext DataContext { get; private set; }

        public UserRepository(DataContext dataContext) : base(dataContext)
        {
            this.DataContext = dataContext;
        }

        public async Task<IEnumerable<UserForListDto>> GetAllAsync()
        {
            return await this.DataContext
                .Users
                .AsNoTracking()
                .Select(x => new UserForListDto
                {
                    Id = x.Id,
                    Username = x.Username,
                    FullName = x.FullName.Value,
                })
                .OrderByDescending(x => x.Id)
                .ToListAsync();
        }

        public async Task<IEnumerable<UserParticipationForListDto>> GetAllParticipationsAsync(Guid userId)
        {
            return await this.DataContext
                .UserParticipations
                .AsNoTracking()
                .Where(x => x.UserId == userId)
                .Select(x => new UserParticipationForListDto
                {
                    ParticipationId = x.Id,
                    UserId = userId,
                    EventId = x.EventId,
                    DatePosted = x.DatePosted,
                    ParticipationRole = x.ParticipationRole.ToString(),
                })
                .OrderByDescending(x => x.DatePosted)
                .ToListAsync();
        }

        public async Task<IEnumerable<UserImageForListDto>> GetAllImagesAsync(Guid userId)
        {
            return await this.DataContext
                .UserImages
                .AsNoTracking()
                .Where(x => x.UserId == userId)
                .Select(x => new UserImageForListDto
                {
                    UserId = userId,
                    Url = x.Url,
                    ImageId = x.Id,
                })
                .ToListAsync();
        }
    }
}
