using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.IntegratedServices.Consumers
{
    public class CreateUserConsumer : IConsumer<ICreateUserContract>
    {
        private readonly IUserService userService;
        private readonly ILogger<CreateUserConsumer> logger;

        public CreateUserConsumer(IUserService userService,
            ILogger<CreateUserConsumer> logger)
        {
            this.userService = userService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<ICreateUserContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@ICreateUserContract}",
                nameof(ICreateUserContract));

            var message = context.Message;

            await this.userService.CreateUserAsync(message.ToCreateUserDto());
        }
    }
}