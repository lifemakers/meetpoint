using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.IntegratedServices.Consumers
{
    public class CreateUserImageConsumer : IConsumer<ICreateUserImageContract>
    {
        private readonly IUserService eventService;
        private readonly ILogger<CreateUserImageConsumer> logger;

        public CreateUserImageConsumer(IUserService eventService,
            ILogger<CreateUserImageConsumer> logger)
        {
            this.eventService = eventService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<ICreateUserImageContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@ICreateUserImageContract}",
                nameof(ICreateUserImageContract));

            var message = context.Message;

            await this.eventService.AddImageAsync(message.ToCreateUserImageDto());
        }
    }
}