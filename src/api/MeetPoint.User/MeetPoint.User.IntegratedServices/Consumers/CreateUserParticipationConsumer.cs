using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.IntegratedServices.Consumers
{
    public class CreateUserParticipationConsumer : IConsumer<ICreateUserParticipationContract>
    {
        private readonly IUserService userService;
        private readonly ILogger<CreateUserParticipationConsumer> logger;

        public CreateUserParticipationConsumer(IUserService userService,
            ILogger<CreateUserParticipationConsumer> logger)
        {
            this.userService = userService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<ICreateUserParticipationContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@ICreateUserParticipationContract}",
                nameof(ICreateUserParticipationContract));

            var message = context.Message;

            await this.userService.AddParticipationAsync(message.ToCreateUserParticipationDto());
        }
    }
}