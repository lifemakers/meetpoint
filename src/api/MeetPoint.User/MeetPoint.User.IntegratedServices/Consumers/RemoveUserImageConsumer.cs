using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.IntegratedServices.Consumers
{
    public class RemoveUserImageConsumer : IConsumer<IRemoveUserImageContract>
    {
        private readonly IUserService userService;
        private readonly ILogger<RemoveUserImageConsumer> logger;

        public RemoveUserImageConsumer(IUserService userService,
            ILogger<RemoveUserImageConsumer> logger)
        {
            this.userService = userService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<IRemoveUserImageContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@IRemoveUserImageContract}",
                nameof(IRemoveUserImageContract));

            var message = context.Message;

            await this.userService.RemoveImageAsync(message.UserId, message.ImageId);
        }
    }
}