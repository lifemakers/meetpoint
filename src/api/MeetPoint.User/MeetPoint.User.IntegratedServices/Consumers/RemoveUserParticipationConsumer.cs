using System.Threading.Tasks;
using MassTransit;
using MeetPoint.Contract;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Mappings;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.IntegratedServices.Consumers
{
    public class RemoveUserParticipationConsumer : IConsumer<IRemoveUserParticipationContract>
    {
        private readonly IUserService userService;
        private readonly ILogger<RemoveUserParticipationConsumer> logger;

        public RemoveUserParticipationConsumer(IUserService userService,
            ILogger<RemoveUserParticipationConsumer> logger)
        {
            this.userService = userService;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<IRemoveUserParticipationContract> context)
        {
            this.logger.LogInformation("Message received with contract type: {@IRemoveUserParticipationContract}",
                nameof(IRemoveUserParticipationContract));

            var message = context.Message;

            await this.userService.RemoveParticipationAsync(message.UserId, message.ParticipationId);
        }
    }
}