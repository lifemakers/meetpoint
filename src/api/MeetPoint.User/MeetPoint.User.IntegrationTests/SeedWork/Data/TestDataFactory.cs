using System;
using System.Collections.Generic;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.IntegrationTests.SeedWork.Data
{
    public static class TestDataFactory
    {
        public static IEnumerable<User.Core.Domain.Aggregates.User.User> Users => GetUsers();

        private static IEnumerable<User.Core.Domain.Aggregates.User.User> GetUsers()
        {
            var users = new List<User.Core.Domain.Aggregates.User.User>();

            users.Add(new User.Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(GetCreateUserDto()), DateTime.Now));

            return users;
        }

        public static CreateUserDto GetCreateUserDto()
        {
            return new CreateUserDto()
            {
                Username = "petrov",
                FirstName = "Petro",
                LastName = "Petrov",
                Phone = "+380999999999",
                Email = "test666@gmail.com",
                Age = 33,
            };
        }
    }
}