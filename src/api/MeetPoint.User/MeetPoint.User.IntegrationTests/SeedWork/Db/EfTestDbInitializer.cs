using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using MeetPoint.User.DataAccess;
using MeetPoint.User.IntegrationTests.SeedWork.Data;

namespace MeetPoint.User.IntegrationTests.SeedWork.Db
{
    public class EfTestDbInitializer : IEfDbInitializer
    {
        private readonly DataContext dataContext;

        public EfTestDbInitializer(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Initialize()
        {
            this.dataContext.Users.AddRange(TestDataFactory.Users);
            this.dataContext.SaveChanges();
        }

        public void Clean()
        {
            this.dataContext.Database.EnsureDeleted();
        }
    }
}