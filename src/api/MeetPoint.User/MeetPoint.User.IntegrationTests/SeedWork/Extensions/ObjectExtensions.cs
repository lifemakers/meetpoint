using System.Collections.Generic;
using System.Net.Http;

namespace MeetPoint.User.IntegrationTests.SeedWork.Extensions
{
    public static class ObjectExtensions
    {
        public static FormUrlEncodedContent ToFormUrlEncodedContent(this object model)
        {
            var formItems = new List<KeyValuePair<string, string>>();

            foreach (var property in model.GetType().GetProperties())
            {
                var name = property.Name;
                var value = property.GetValue(model)?.ToString();

                if (value != null)
                {
                    formItems.Add(new KeyValuePair<string, string>(name, value));
                }
            }

            return new FormUrlEncodedContent(formItems);
        }
    }
}