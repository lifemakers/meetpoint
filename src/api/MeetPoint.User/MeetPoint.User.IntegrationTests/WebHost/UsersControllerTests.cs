using System;
using System.Net.Http;
using MeetPoint.User.IntegrationTests.SeedWork;
using MeetPoint.User.WebHost;
using Xunit;

namespace MeetPoint.User.IntegrationTests.WebHost
{
    [Obsolete("Keep as example.")]
    public class UsersControllerTests : IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly TestWebApplicationFactory<Startup> factory;
        private readonly HttpClient client;

        public UsersControllerTests(TestWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
            this.client = this.factory.CreateClient();
        }

        // [Fact]
        // public async Task CreateUserAsync_PassValidModel_ShouldCreateExpectedUser()
        // {
        //     var request = TestDataFactory.GetCreateUserDto();
        //     var formContent = request.ToFormUrlEncodedContent();
        //
        //     var response = await this.client.PostAsync("/api/v1/users", formContent);
        //
        //     response.IsSuccessStatusCode.Should().BeTrue();
        //     response.StatusCode.Should().Be(HttpStatusCode.Created);
        //
        //     var createdUserString = await client.GetStringAsync(response.Headers.Location);
        //     var createdUser = JsonConvert.DeserializeObject<UserDto>(createdUserString);
        //
        //     createdUser.Id.Should().NotBeEmpty();
        //     createdUser.Username.Should().Be(request.Username);
        //     createdUser.FirstName.Should().Be(request.FirstName);
        //     createdUser.LastName.Should().Be(request.LastName);
        //     createdUser.Phone.Should().Be(request.Phone);
        //     createdUser.Email.Should().Be(request.Email);
        //     createdUser.Age.Should().Be(request.Age);
        // }

        // [Fact]
        // public async Task DeleteUserAsync_PassExistentUserId_ShouldDeleteUser()
        // {
        //     var request = TestDataFactory.GetCreateUserDto();
        //     var formContent = request.ToFormUrlEncodedContent();
        //
        //     var response = await this.client.PostAsync("/api/v1/users", formContent);
        //
        //     response.IsSuccessStatusCode.Should().BeTrue();
        //     response.StatusCode.Should().Be(HttpStatusCode.Created);
        //
        //     var createdUserString = await client.GetStringAsync(response.Headers.Location);
        //     var createdUser = JsonConvert.DeserializeObject<UserDto>(createdUserString);
        //     var createdUserId = createdUser.Id;
        //
        //     createdUserId.Should().NotBeEmpty();
        //
        //     response = await this.client.DeleteAsync($"/api/v1/users/{createdUserId.ToString()}");
        //
        //     response.IsSuccessStatusCode.Should().BeTrue();
        //     response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        // }
    }
}