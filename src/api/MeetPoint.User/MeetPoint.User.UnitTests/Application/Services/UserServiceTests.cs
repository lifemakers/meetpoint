using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using MeetPoint.User.Application.Services;
using MeetPoint.User.Core.Domain.Exceptions;
using MeetPoint.User.Core.Domain.Exceptions.User;
using MeetPoint.User.Core.Domain.Interfaces;
using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;
using MeetPoint.User.UnitTests.SeedWork.DtoExtensions;
using MeetPoint.User.UnitTests.SeedWork.MockSetup;
using Moq;
using Xunit;

namespace MeetPoint.User.UnitTests.Application.Services
{
    // TODO create tests for EditUserAsync
    // TODO create tests for DeleteUserAsync
    public class UserServiceTests
    {
        private readonly IFixture fixture;
        private readonly Mock<IUserRepository> userRepositoryMock;
        private readonly Mock<ICurrentDateProvider> currentDateProviderMock;

        private readonly IUserService userService;
        private readonly User.Core.Domain.Aggregates.User.User user;

        private CreateUserDto createUserDto;

        public UserServiceTests()
        {
            this.fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            // mocks
            this.userRepositoryMock = this.fixture
                .Freeze<Mock<IUserRepository>>();
            this.currentDateProviderMock = this.fixture
                .Freeze<Mock<ICurrentDateProvider>>();

            this.currentDateProviderMock.SetupBehavior();

            // testing target
            this.userService = this.fixture.Build<UserService>().Create();

            // dto auto feed setup
            this.createUserDto = this.fixture.Create<CreateUserDto>();

            // domain model
            this.user = new User.Core.Domain.Aggregates.User.User(new ValidModel<CreateUserDto>(this.createUserDto),
                this.currentDateProviderMock.Object.GetDate());
        }

        [Fact]
        private async Task CreateUserAsync_PassValidModel_ShouldCallGetDate()
        {
            await this.userService.CreateUserAsync(this.createUserDto);

            this.currentDateProviderMock
                .Verify(x => x.GetDate(), Times.AtLeastOnce);
        }

        [Fact]
        public async Task CreateUserAsync_PassValidModel_ShouldCallSaveChangesAsync()
        {
            await this.userService.CreateUserAsync(this.createUserDto);

            this.userRepositoryMock
                .Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task CreateUserAsync_PassValidModel_ShouldCallCreateAsync()
        {
            await this.userService.CreateUserAsync(this.createUserDto);

            this.userRepositoryMock
                .Verify(x => x.CreateAsync(It.IsAny<User.Core.Domain.Aggregates.User.User>()), Times.Once);
        }

        [Fact]
        public async Task CreateUserAsync_PassInvalidModel_ShouldThrowValidationException()
        {
            this.createUserDto.MakeInvalid();

            Func<Task> act = async () => { await this.userService.CreateUserAsync(this.createUserDto); };

            await act
                .Should()
                .ThrowAsync<ValidationException>();
        }

        [Fact]
        public async Task CreateUserAsync_PassDefaultDate_ShouldThrowValidationException()
        {
            this.currentDateProviderMock
                .SetupDefaultDate();

            Func<Task> act = async () => { await this.userService.CreateUserAsync(this.createUserDto); };

            await act
                .Should()
                .ThrowAsync<ArgumentException>();
        }

        [Fact]
        public async Task CreateUserAsync_PassNullInsteadOfDto_ShouldThrowArgumentNullException()
        {
            this.createUserDto = null;

            Func<Task> act = async () => { await this.userService.CreateUserAsync(this.createUserDto); };

            await act
                .Should()
                .ThrowAsync<ArgumentNullException>();
        }

        [Fact]
        private async Task GetAllUsersAsync_PassDefaultParams_ShouldReturnValidUserForListDto()
        {
            this.userRepositoryMock.SetupBehaviorGetAllUsers(this.fixture);

            var users = await this.userService.GetAllUsersAsync();

            users
                .Should().NotBeEmpty()
                .And.ContainItemsAssignableTo<UserForListDto>()
                .And.OnlyHaveUniqueItems();
        }

        [Fact]
        public async Task GetAllUsersAsync_PassDefaultParams_ShouldCallGetAllAsync()
        {
            this.userRepositoryMock.SetupBehaviorGetAllUsers(this.fixture);

            await this.userService.GetAllUsersAsync();

            this.userRepositoryMock
                .Verify(x => x.GetAllAsync(), Times.Once);
        }

        [Fact]
        private async Task GetUserByIdAsync_PassValidId_ShouldReturnValidUserDto()
        {
            var userId = Guid.NewGuid();
            this.userRepositoryMock
                .Setup(x => x.GetByIdAsync(userId))
                .Returns(async () => this.user);

            var user = await this.userService.GetUserByIdAsync(userId);

            user.Should().NotBeNull();
            user.Id.Should().NotBeEmpty();
            user.Username.Should().Be(this.createUserDto.Username);
            user.FirstName.Should().Be(this.createUserDto.FirstName);
            user.LastName.Should().Be(this.createUserDto.LastName);
            user.Phone.Should().Be(this.createUserDto.Phone);
            user.Email.Should().Be(this.createUserDto.Email);
            user.Age.Should().Be(this.createUserDto.Age);
            user.UserImages.Should().NotBeNull();
        }

        [Fact]
        public async Task GetUserByIdAsync_PassValidId_ShouldCallGetByIdAsync()
        {
            var userId = Guid.NewGuid();
            this.userRepositoryMock
                .Setup(x => x.GetByIdAsync(userId))
                .Returns(async () => this.user);

            await this.userService.GetUserByIdAsync(userId);

            this.userRepositoryMock
                .Verify(x => x.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        private async Task GetUserByIdAsync_PassInvalidId_ShouldReturnUserWasNotFoundException()
        {
            this.userRepositoryMock
                .Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(async () => null);

            Func<Task> act = async () =>
            {
                await this.userService.GetUserByIdAsync(Guid.Empty);
            };

            await act
                .Should()
                .ThrowAsync<UserWasNotFoundException>();
        }
    }
}