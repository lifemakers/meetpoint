namespace MeetPoint.User.UnitTests.SeedWork.Constants
{
    public static class DomainEntityConstants
    {
        public const int InvalidIdValue = 0;
        public const int DefaultIdValue = 1;
    }
}