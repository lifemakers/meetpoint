using MeetPoint.User.Core.Domain.Models.Dtos;

namespace MeetPoint.User.UnitTests.SeedWork.DtoExtensions
{
    public static class CreateUserDtoExtensions
    {
        public static CreateUserDto MakeInvalid(this CreateUserDto createUserDto)
        {
            createUserDto.Username = null;

            return createUserDto;
        }
    }
}