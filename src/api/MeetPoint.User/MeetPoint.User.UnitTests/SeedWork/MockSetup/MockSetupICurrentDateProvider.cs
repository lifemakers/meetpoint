using System;
using MeetPoint.User.Core.Domain.Interfaces;
using Moq;

namespace MeetPoint.User.UnitTests.SeedWork.MockSetup
{
    public static class MockSetupICurrentDateProvider
    {
        public static Mock<ICurrentDateProvider> SetupBehavior(this Mock<ICurrentDateProvider> mock)
        {
            mock
                .Setup(x => x.GetDate())
                .Returns(DateTime.Now);

            return mock;
        }

        public static Mock<ICurrentDateProvider> SetupDefaultDate(this Mock<ICurrentDateProvider> mock)
        {
            mock
                .Setup(x => x.GetDate())
                .Returns(DateTime.MinValue);

            return mock;
        }
    }
}