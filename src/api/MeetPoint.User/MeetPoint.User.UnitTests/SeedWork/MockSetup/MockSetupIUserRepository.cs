using AutoFixture;
using MeetPoint.User.Core.Domain.Interfaces.Repositories;
using MeetPoint.User.Core.Domain.Models.Dtos;
using Moq;

namespace MeetPoint.User.UnitTests.SeedWork.MockSetup
{
    public static class MockSetupIUserRepository
    {
        public static Mock<IUserRepository> SetupBehaviorGetAllUsers(
            this Mock<IUserRepository> mock,
            IFixture fixture)
        {
            mock
                .Setup(x => x.GetAllAsync())
                .Returns(async () => fixture
                    .CreateMany<UserForListDto>(5));

            return mock;
        }
    }
}