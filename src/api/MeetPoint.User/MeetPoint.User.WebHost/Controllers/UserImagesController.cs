using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Constants;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;
using MeetPoint.User.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.WebHost.Controllers
{
    /// <summary> User Images Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Users)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class UserImagesController : Controller
    {
        private readonly IUserService userService;

        public UserImagesController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("{userId:Guid}/images")]
        [ResponseCache(CacheProfileName = CacheProfileNameConstants.Default)]
        [ProducesResponseType(typeof(IEnumerable<UserImageForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<UserImageForListDto>>> GetAllUserImages(Guid userId)
        {
            var images = await this.userService.GetAllImagesAsync(userId);

            return Ok(images);
        }
    }
}