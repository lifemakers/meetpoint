using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Constants;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;
using MeetPoint.User.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.WebHost.Controllers
{
    /// <summary> User Participations Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Users)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class UserParticipationsController : Controller
    {
        private readonly IUserService userService;

        public UserParticipationsController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("{userId:Guid}/participations")]
        [ResponseCache(CacheProfileName = CacheProfileNameConstants.Default)]
        [ProducesResponseType(typeof(IEnumerable<UserParticipationForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<UserParticipationForListDto>>> GetAllUserParticipations(Guid userId)
        {
            var participations = await this.userService.GetAllParticipationsAsync(userId);

            return Ok(participations);
        }
    }
}