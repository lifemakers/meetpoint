using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.User.Core.Domain.Constants;
using MeetPoint.User.Core.Domain.Interfaces.Services;
using MeetPoint.User.Core.Domain.Models;
using MeetPoint.User.Core.Domain.Models.Dtos;
using MeetPoint.User.WebHost.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MeetPoint.User.WebHost.Controllers
{
    /// <summary> User Management. </summary>
    [ApiController]
    [Authorize]
    [Route(BaseRoutePrefixConstants.Users)]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary> Get all users. </summary>
        [HttpGet]
        [ResponseCache(CacheProfileName = CacheProfileNameConstants.Default)]
        [ProducesResponseType(typeof(IEnumerable<UserForListDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<UserForListDto>>> GetAllUsersAsync()
        {
            var response = await this.userService.GetAllUsersAsync();

            return this.Ok(response);
        }

        /// <summary> Get user by id. </summary>
        [HttpGet("{id:Guid}")]
        [ResponseCache(CacheProfileName = CacheProfileNameConstants.Default)]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserDto>> GetUserByIdAsync(Guid id)
        {
            var response = await this.userService.GetUserByIdAsync(id);

            return this.Ok(response);
        }
    }
}