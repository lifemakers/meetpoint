using MeetPoint.User.Core.Domain.Constants;
using Microsoft.AspNetCore.Mvc;

namespace MeetPoint.User.WebHost.Extensions
{
    public static class MvcOptionsExtensions
    {
        public static void AddCacheProfiles(this MvcOptions options)
        {
            options.CacheProfiles.Add(CacheProfileNameConstants.Default, new CacheProfile
            {
                Duration = 10,
                Location = ResponseCacheLocation.Any,
                VaryByHeader = "User-Agent",
            });
        }
    }
}