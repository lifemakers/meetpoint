namespace MeetPoint.Core.Constants
{
    public class ClientNameConstants
    {
        public const string Interactive = "Interactive";
        public const string Machine2Machine = "Machine2Machine";
    }
}