using MeetPoint.Core.Enums;
using MeetPoint.Core.Extensions;

namespace MeetPoint.Core.Exceptions
{
    public class InvalidTakeValueException : BaseAppException
    {
        private const ErrorCodes.Global ErrorCodeValue = ErrorCodes.Global.InvalidTakeCount;

        public override int ErrorCode => (int)ErrorCodeValue;

        public InvalidTakeValueException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public InvalidTakeValueException(string message)
            : base(message)
        {
        }
    }
}