using System.Collections.Generic;
using MeetPoint.Core.Enums;
using MeetPoint.Core.Extensions;
using MeetPoint.Core.Models;

namespace MeetPoint.Core.Exceptions
{
    public class ValidationException : BaseAppException
    {
        private const ErrorCodes.Validation ErrorCodeValue = ErrorCodes.Validation.ValidationError;

        public override int ErrorCode => (int)ErrorCodeValue;

        public IEnumerable<ValidationError> ValidationErrors { get; private set; }

        public ValidationException()
            : base(ErrorCodeValue.GetErrorMessage())
        {
        }

        public ValidationException(string message)
            : base(message)
        {
        }

        public ValidationException(IEnumerable<ValidationError> validationError)
            : this()
        {
            this.ValidationErrors = validationError;
        }
    }
}