using MeetPoint.Core.Enums;
using MeetPoint.Core.Resources;

namespace MeetPoint.Core.Extensions
{
    public static class ErrorCodeExtensions
    {
        public static string GetErrorMessage(this int errorCode)
        {
            return ErrorMessages.ResourceManager.GetString(errorCode.ToString());
        }

        public static string GetErrorMessage(this int? errorCode)
        {
            return errorCode.HasValue ? ErrorMessages.ResourceManager.GetString($"{errorCode}") : string.Empty;
        }

        public static string GetErrorMessage(this ErrorCodes.Validation errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.User errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.Tag errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.Event errorCode) => GetErrorMessage((int)errorCode);

        public static string GetErrorMessage(this ErrorCodes.Global errorCode) => GetErrorMessage((int)errorCode);
    }
}