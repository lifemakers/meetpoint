using System;

namespace MeetPoint.Core.Interfaces
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}