using System;
using System.Collections.Concurrent;

namespace MeetPoint.Core.Interfaces
{
    public interface IDomainEntity
    {
        Guid Id { get; }

        DateTime DatePost { get; }

        DateTime DateLastUpdated { get; }

        IProducerConsumerCollection<IDomainEvent> DomainEvents { get; }
    }
}