using System.Threading.Tasks;

namespace MeetPoint.Core.Interfaces
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent domainEvent);
    }
}