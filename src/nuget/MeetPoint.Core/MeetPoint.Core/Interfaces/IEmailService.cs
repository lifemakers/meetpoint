using System.Threading.Tasks;
using MeetPoint.Core.Models.Dto;

namespace MeetPoint.Core.Interfaces
{
    public interface IEmailService
    {
        Task SendEmail(SendEmailDto sendEmailDto);
    }
}