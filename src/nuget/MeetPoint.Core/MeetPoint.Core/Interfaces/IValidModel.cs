namespace MeetPoint.Core.Interfaces
{
    public interface IValidModel<TModel>
    {
        TModel Value { get; }
    }
}