using System.ComponentModel.DataAnnotations;
using MeetPoint.Core.Constants;

namespace MeetPoint.Core.Models.Dto
{
    public class SendEmailDto
    {
        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string[] EmailsTo { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Subject { get; set; }

        [Required(ErrorMessage = ValidationMessageRefConstants.FieldIsRequired)]
        public string Text { get; set; }
    }
}