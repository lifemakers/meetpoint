using MeetPoint.Core.Extensions;

namespace MeetPoint.Core.Models
{
    public class ValidationError
    {
        public int? ErrorCode { get; private set; }

        public string FieldName { get; private set; }

        public string Message { get; private set; }

        protected ValidationError()
        {
        }

        public ValidationError(int? errorCode, string name)
        {
            this.ErrorCode = errorCode;
            this.FieldName = name;
            this.Message = errorCode.GetErrorMessage();
        }

        public ValidationError(int? errorCode, string name, string message)
        {
            this.ErrorCode = errorCode;
            this.FieldName = name;
            this.Message = message;
        }
    }
}