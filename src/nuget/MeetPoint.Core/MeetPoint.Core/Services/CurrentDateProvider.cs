using System;
using MeetPoint.Core.Interfaces;

namespace MeetPoint.Core.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}