import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

const AppFooter = () => {
    const classes = useStyles();

    return (
        <footer className={classes.footer}>
            <Typography variant="h6" align="center" gutterBottom>
                MeetPoint
            </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                <strong>OTUS study project</strong> delivered with .NET Core microservice architecture and react framework
            </Typography>
            <Copyright />
        </footer>
    )
};

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://gitlab.com/lifemakers/meetpoint/">
                Repository link
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default AppFooter;