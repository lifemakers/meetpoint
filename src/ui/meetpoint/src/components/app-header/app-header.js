import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    content: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    buttons: {
        marginTop: theme.spacing(4),
    },
    link: {
        color: 'inherit',
        '&:hover, &:focus': {
          color: 'inherit'
        }
    }
}));

const AppHeader = () => {
    const classes = useStyles();

    return (
        <div className={classes.content}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                MeetPoint
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
                Platform that allows you to organize joint leisure with your friends,
                colleagues, mates, whoever you want - we will organize!
            </Typography>
            <div className={classes.buttons}>
                <Grid container spacing={2} justifyContent="center">
                    <Grid item>
                        <Button variant="contained" color="primary">
                            <Link to="/events" className={classes.link}>
                                View Popular Events
                            </Link>
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button variant="outlined" color="primary">
                            <Link to="/events/create" className={classes.link}>
                                Create Event
                            </Link>
                        </Button>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
};

export default AppHeader;