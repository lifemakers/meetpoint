import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PeopleIcon from '@material-ui/icons/People';
import AssignmentIcon from '@material-ui/icons/Assignment';
import EventIcon from '@material-ui/icons/Event';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import HomeIcon from '@material-ui/icons/Home';
import MessageIcon from '@material-ui/icons/Message';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import AddIcon from '@material-ui/icons/Add';
import { Link } from "react-router-dom";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      },
      toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
      },
      link: {
          color: 'inherit',
          '&:hover, &:focus': {
            color: 'inherit'
          }
      }
}));

export default function AppMenuDrawer(props) {
  const classes = useStyles();
  const {openDrawer, onCloseDrawer} = props;

  return (
    <Drawer
    variant="permanent"
    classes={{
      paper: clsx(classes.drawerPaper, !openDrawer && classes.drawerPaperClose),
    }}
    open={openDrawer}
  >
    <div className={classes.toolbarIcon}>
      <IconButton onClick={onCloseDrawer}>
        <ChevronLeftIcon />
      </IconButton>
    </div>
    <Divider />
    <List>
    <div>
        <Link to="/" className={classes.link}>
            <ListItem button>
                <ListItemIcon>
                    <HomeIcon />
                </ListItemIcon>
                <ListItemText primary="Home" />
            </ListItem>
        </Link>
        <Link to="/events" className={classes.link}>
            <ListItem button>
                <ListItemIcon>
                    <EventAvailableIcon />
                </ListItemIcon>
                <ListItemText primary="My Events" />
            </ListItem>
        </Link>
        <Link to="/events" className={classes.link}>
            <ListItem button>
                <ListItemIcon>
                    <EventIcon />
                </ListItemIcon>
                <ListItemText primary="Popular Events" />
            </ListItem>
        </Link>
        <Link to="/events/create" className={classes.link}>
            <ListItem button>
                <ListItemIcon>
                    <AddIcon />
                </ListItemIcon>
                <ListItemText primary="Create Event" />
            </ListItem>
        </Link>
        <ListItem button>
            <ListItemIcon>
                <CalendarTodayIcon />
            </ListItemIcon>
            <ListItemText primary="Today" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary="Friends" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <MessageIcon />
            </ListItemIcon>
            <ListItemText primary="Messages" />
        </ListItem>
    </div>
    </List>
    <Divider />
    <List>
        <div>
            <ListSubheader inset>Saved events</ListSubheader>
            <ListItem button>
                <ListItemIcon>
                    <AssignmentIcon />
                </ListItemIcon>
                <ListItemText primary="Event #1" />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <AssignmentIcon />
                </ListItemIcon>
                <ListItemText primary="Event #2" />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <AssignmentIcon />
                </ListItemIcon>
                <ListItemText primary="Event #3" />
            </ListItem>
        </div>
    </List>
  </Drawer>
  );
}
