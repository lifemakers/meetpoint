import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from "@material-ui/core/CssBaseline";
import { Component } from 'react';
import AppMenu from '../app-menu';
import { ThemeProvider } from '@material-ui/core/styles';
import { withTheme, withStyles } from '@material-ui/core/styles';
import AppMenuDrawer from '../app-menu-drawer';
import EventListPage from '../../pages/event-list-page';
import EventCreatePage from '../../pages/event-create-page';
import HomePage from '../../pages/home-page';
import UserProfilePage from '../../pages/user-profile-page';
import EventInfoPage from '../../pages/event-info-page';
import theme from './theme';
import Paper from '@material-ui/core/Paper';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { createEvent } from '@testing-library/react';
import eventInfoPage from '../../pages/event-info-page';

const styles = theme => ({
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(5),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
});

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openDrawer: false
        };
    }

    onOpenDrawer = () => this.setState({ openDrawer: true });
    onCloseDrawer = () => this.setState({ openDrawer: false });

    render() {
        const { classes } = this.props;
        const { openDrawer } = this.state;

        return (
            <Router>
                <ThemeProvider theme={theme}>
                    <div className="d-flex">
                        <CssBaseline />
                        <AppMenu openDrawer={openDrawer} onOpenDrawer={this.onOpenDrawer} />
                        <AppMenuDrawer openDrawer={openDrawer} onCloseDrawer={this.onCloseDrawer} />
                        <main className={classes.content}>
                            <div className={classes.appBarSpacer} />
                            <Container maxWidth="lg" className={classes.container}>
                                <Switch>
                                    <Route path="/user-profile" component={UserProfilePage} />
                                    <Route path="/events/create" exact component={EventCreatePage} />
                                    <Route path="/events" exact component={EventListPage} />

                                    <Route path="/events/:id" render={({match}) => {
                                        const {id} = match;

                                        return <EventInfoPage id={id} />
                                    }} />

                                    <Route path="/" component={HomePage} />
                                </Switch>
                            </Container>
                        </main>
                    </div>
                </ThemeProvider>
            </Router>
        );
    }
}

export default withTheme(withStyles(styles)(App));