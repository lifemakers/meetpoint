import { createTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';

const theme = createTheme({
  palette: {
    primary: {
      main: blue[700],
    },
    secondary: {
      main: pink[500],
    },
    common: {
        black: '#000',
        white: '#fff'
    },
    background: {
        default: "#fff"
    }
  },
});

export default theme;