import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    content: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
        textAlign: "left"
    }
}));

const EventCreateFormHeader = () => {
    const classes = useStyles();

    return (
        <div className={classes.content}>
            <Typography component="h1" variant="h2" align="left" color="textPrimary" gutterBottom>
                Create event
            </Typography>
        </div>
    )
};

export default EventCreateFormHeader;