import React, { Component } from "react";
import { withTheme, withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import TagSelector from '../tag-selector';
import EventCreateFormHeader from '../event-create-form-header';
import moment from 'moment';
import 'date-fns';

const colors = [
    "#2db7f5",
    "#f50",
    "#87d068",
    "#108ee9",
    "#42ddf5",
    "#cccf3c"
]
const options = [
    { label: "pub", value: colors[0] }, 
    { label: "beer", value: colors[1] }, 
    { label: "friends", value: colors[2] }, 
    { label: "day off", value: colors[3] }, 
    { label: "hang out", value: colors[4] }, 
    { label: "old friends", value: colors[5] },
];
const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
            width: '100%',
        },
    },
    btnGroup: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    tags: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2),
    },
});

class EventCreateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            description: "",
            beginDate: new Date('2021-10-10T10:00:00'),
            endDate: new Date('2021-10-10T16:00:00'),
            tagsSelected: [options[0].value, options[1].value]
        };
    }

    onChangeTitle = (e) => this.setState({ title: e.target.value });
    onChangeDescription = (e) => this.setState({ description: e.target.value });
    onChangeBeginDate = (date) => this.setState({ beginDate: date });
    onChangeEndDate = (date) => this.setState({ endDate: date });
    onChangeTagsSelected = (tagsSelected) => this.setState({tagsSelected: tagsSelected});
    onSubmit = (e) => {
        e.preventDefault();

        const { onSubmitCreateForm } = this.props;
        const { title, description, beginDate, endDate, tagsSelected } = this.state;

        const beginDateFormatted = moment(beginDate).format();
        const endDateFormatted = moment(endDate).format();

        const createEvent = { 
            title,
            description,
            beginDate: beginDateFormatted,
            endDate: endDateFormatted,
            tags: tagsSelected 
        };

        onSubmitCreateForm(createEvent);
    }

    render() {
        const { title, description, beginDate, endDate, tagsSelected } = this.state;
        const { classes } = this.props;
        console.log('rerender with tags selected:', this.state.tagsSelected);
        return (
            <ValidatorForm
                ref="form"
                onSubmit={this.onSubmit}
                onError={errors => console.log(errors)}
                className={classes.root}
                autoComplete="off">
                <div>
                    <EventCreateFormHeader/>
                </div>
                <div>
                    <TextValidator
                        required
                        id="title"
                        name="title"
                        label="Title"
                        onChange={this.onChangeTitle}
                        value={title}
                        variant="outlined"
                        validators={
                            [
                                'minStringLength:6',
                                'maxStringLength:30']
                        }
                        errorMessages={
                            [
                                'Title is too short. Minimum length is 6',
                                'Title is too long. Maximum length is 30']}
                    />
                </div>
                <div>
                    <TextValidator
                        required
                        id="description"
                        name="description"
                        label="Description"
                        onChange={this.onChangeDescription}
                        value={description}
                        variant="outlined"
                        validators={
                            ['minStringLength:6',
                            'maxStringLength:100']
                        }
                        errorMessages={
                            ['Description is too short. Minimum length is 6',
                             'Description is too long. Maximum length is 100']}
                    />
                </div>
                <div>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <KeyboardDatePicker
                                    required
                                    margin="normal"
                                    id="beginDate"
                                    label="Begin date"
                                    format="yyyy-MM-dd"
                                    value={beginDate}
                                    onChange={this.onChangeBeginDate}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <KeyboardTimePicker
                                    required
                                    margin="normal"
                                    id="beginTime"
                                    label="Begin time"
                                    value={beginDate}
                                    onChange={this.onChangeBeginDate}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </MuiPickersUtilsProvider>
                </div>
                <div>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <KeyboardDatePicker
                                    required
                                    margin="normal"
                                    id="endDate"
                                    label="End date"
                                    format="yyyy-MM-dd"
                                    value={endDate}
                                    onChange={this.onChangeEndDate}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <KeyboardTimePicker
                                    required
                                    margin="normal"
                                    id="endTime"
                                    label="End time"
                                    value={endDate}
                                    onChange={this.onChangeEndDate}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </MuiPickersUtilsProvider>
                </div>
                <div className={classes.tags}>
                    <TagSelector
                                options={options} 
                                onChangeTagsSelected={this.onChangeTagsSelected}
                                value={tagsSelected} />
                </div>
                <div className={classes.btnGroup}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary">
                        Create
                    </Button>
                </div>
            </ValidatorForm>
        );
    }
}

export default withTheme(withStyles(styles)(EventCreateForm));