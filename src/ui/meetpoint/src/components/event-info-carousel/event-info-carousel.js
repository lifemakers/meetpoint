import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
import './event-info-carousel.scss'

const EventInfoCarousel = ({items}) => <UncontrolledCarousel items={items} autoPlay={true}/>;

export default EventInfoCarousel;