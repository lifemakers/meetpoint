import { Component } from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import PeopleIcon from '@material-ui/icons/People';
import DateRangeIcon from '@material-ui/icons/DateRange';
import IconButton from '@material-ui/core/IconButton';

const gridItemLeft = {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-around'
};
const gridItemRight = {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-around'
};
const paragraph = {
    paddingTop: 3
};
export default class EventInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        const {beginDate, endDate, participantsCount} = this.props;

        return (
            <Grid item container spacing={3}>
                <Grid item xs={3}>
                    <Grid container>
                        <Grid item xs={3} lg={2} style={gridItemLeft}>
                            <PeopleIcon />
                        </Grid>
                        <Grid item xs={6} lg={4} style={gridItemLeft}>
                            <Typography variant="caption" align="left" color="textPrimary" paragraph style={paragraph}>
                                Participants: {participantsCount}
                            </Typography>
                        </Grid>
                        <Grid item xs={3} lg={6} />
                    </Grid>
                </Grid>
                <Grid item xs={5} />
                <Grid item xs={4}>
                    <Grid container>
                        <Grid item lg={4} />
                        <Grid item xs={2} lg={1} style={gridItemRight}>
                            <DateRangeIcon />
                        </Grid>
                        <Grid item xs={10} lg={7} style={gridItemRight}>
                            <Typography variant="caption" align="right" color="textPrimary" paragraph style={paragraph}>
                                {beginDate} - {endDate}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}