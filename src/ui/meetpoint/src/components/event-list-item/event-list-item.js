import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import EventTagList from "../event-tag-list";
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '3px',
        flexGrow: 1,
        width: '100%'
    },
    paper: {
        padding: theme.spacing(1),
        width: '100%',
        margin: "auto",
    },
    listItem: {
        width: '100%'
    },
    image: {
        height: 128
    },
    img: {
        margin: "auto",
        display: "block",
        maxWidth: "100%",
        maxHeight: "100%",
        borderRadius: '6px'
    }
}));

export default function EventListItem(props) {
    const classes = useStyles();
    const {id,
        title,
        beginDate,
        tags,
    } = props;
    const navigationRoute = `/events/${id}`;

    return (
        <ListItemLink className={classes.root} component={Link} to={navigationRoute}>
            <Paper className={classes.paper} elevation={3}>
                <Grid container spacing={2}>
                    <Grid item>
                        <ButtonBase className={classes.image}>
                            <img
                                className={classes.img}
                                alt="complex"
                                src="https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
                            />
                        </ButtonBase>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs={6} container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography variant="subtitle1" color="textSecondary">
                                    {title}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item container xs={6} direction="column" spacing={2}>
                            <Grid item xs>
                                <div className="d-flex align-self-start flex-wrap justify-content-end">
                                    <EventTagList items={tags}/>
                                </div>
                            </Grid>
                            <Grid item>
                                <div className="d-flex align-self-start justify-content-end">
                                    <Typography
                                        variant="body2"
                                        color="textSecondary">
                                        {beginDate}
                                    </Typography>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </ListItemLink>
    );
}

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}