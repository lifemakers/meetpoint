import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import EventListItem from '../event-list-item';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function EventList({events}) {
    const classes = useStyles();

    const elements = events.map(item => {
        return (
            <EventListItem
                key={item.id}
                {...item} />
        )
    })

    return (
        <div className={classes.root}>
            <List component="nav" aria-label="secondary">
                {elements}
            </List>
        </div>
    );
}