import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';

const link = {
    color: 'inherit',
    '&:hover, &:focus': {
      color: 'inherit'
    }
}
export default function EventParticipationListItem(props) {
    const {firstName,
        lastName,
        datePosted,
        previewImageUrl} = props;

    const primaryText = firstName + ' ' + lastName;
    const secondaryText = `date subscribed:  ${datePosted}`;

    return (
        <ListItemLink style={link}>
            <ListItemAvatar>
                <Avatar alt="Event preview image" src={previewImageUrl} />
            </ListItemAvatar>
            <ListItemText
                primary={primaryText}
                secondary={secondaryText}
            />
        </ListItemLink>
    )
}

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}