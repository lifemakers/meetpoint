import { Tag } from 'antd';

const colors = [
    "#2db7f5",
    "#f50",
    "#87d068",
    "#108ee9",
    "#42ddf5",
    "#cccf3c"
];

const EventTagList = ({items}) => {
    const tagElements = items.map((item, i) => {
        return <Tag key={i} color={getRandomColor()}>{item}</Tag>
    })

    return tagElements;
}

export default EventTagList;

function getRandomColor() {
    const colorIndex = Math.floor(Math.random() * colors.length);
    return colors[colorIndex];
}
