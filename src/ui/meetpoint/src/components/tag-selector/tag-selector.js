import { Select, Tag } from 'antd';
import { makeStyles } from '@material-ui/core/styles';

function TagRender(props) {
  const { label, value, closable, onClose } = props;
  const onPreventMouseDown = event => {
    event.preventDefault();
    event.stopPropagation();
  };
  return (
    <Tag
      color={value}
      onMouseDown={onPreventMouseDown}
      closable={closable}
      onClose={onClose}
      style={{ marginRight: 3 }}
    >
      {label}
    </Tag>
  );
}
const useStyles = makeStyles((theme) => ({
    select: {
        height: "100%",
        width: "100%",
    },
}));

export default function TagSelector(props) {
    const classes = useStyles();
    const {onChangeTagsSelected, options, value} = props; 

    return (
        <Select
            className={classes.select}
            mode="multiple"
            showArrow
            tagRender={TagRender}
            defaultValue={value}
            style={{ width: '100%' }}
            onChange={onChangeTagsSelected}
            options={options}
        />
    );
}