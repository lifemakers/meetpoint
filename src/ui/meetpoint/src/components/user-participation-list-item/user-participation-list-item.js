import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import EventIcon from '@material-ui/icons/Event';
import CancelIcon from '@material-ui/icons/Cancel';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from 'react-router-dom';
import { Modal, Button, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { confirm } = Modal;
const link = {
    color: 'inherit',
    '&:hover, &:focus': {
      color: 'inherit'
    }
}
export default function UserParticipationListItem(props) {
    const {id,
        eventName, 
        eventBeginDate, 
        eventId,
        previewImageUrl,
        onRemoveParticipation} = props;
    const navigationRoute = `/events/${eventId}`;

    return (
        <ListItemLink style={link} component={Link} to={navigationRoute} >
            <ListItemAvatar>
                <Avatar alt="Event preview image" src={previewImageUrl} />
            </ListItemAvatar>
            <ListItemText
                primary={eventName}
                secondary={eventBeginDate}
            />
            <ListItemSecondaryAction onClick={() => showConfirm(() => onRemoveParticipation(id))}>
                <Tooltip title="Сancel participation in the event" aria-label="remove">
                    <IconButton
                            edge="end" 
                            aria-label="remove">
                        <CancelIcon />
                    </IconButton>
                </Tooltip>
            </ListItemSecondaryAction>
        </ListItemLink>
    )
}

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}

function showConfirm(onOk) {
    confirm({
      centered: true,
      title: 'Do you Want to unsubscribe from this event?',
      icon: <ExclamationCircleOutlined />,
      content: 'This event will be removed from your participation list',
      onOk,
      onCancel() {
        console.log('Cancel');
      },
    });
}