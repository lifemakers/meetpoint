import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import UserParticipationListItem from '../user-participation-list-item';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    }
}));

export default function UserParticipationList(props) {
    const classes = useStyles();
    const {onRemoveParticipation, items = []} = props;
    const elements = items.map(item => {
        return (
            <UserParticipationListItem
                                    key={item.id}
                                    onRemoveParticipation={() => onRemoveParticipation(item.id)}
                                    className={classes.link}
                                    {...item} />
        )
    })

    return (
        <div className={classes.root}>
            <List component="nav" aria-label="secondary mailbox folders">
                {elements}
            </List>
        </div>
    );
}