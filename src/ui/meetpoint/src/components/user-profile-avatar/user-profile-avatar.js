import React, { useState } from 'react';
import { Image } from 'antd';

const UserProfileAvatar = ({images = []}) => {
  const [visible, setVisible] = useState(false);
  const defaultUserAvatarUrl = "#";
  const previewImage = images.length === 0 ? defaultUserAvatarUrl : images[0];
  const elements = images.map(item => {
      return <Image key={item.id} src={item.src} />
  })

  return (
    <>
      <Image
        preview={{ visible: false }}
        width={200}
        src={previewImage.src}
        onClick={() => setVisible(true)}
      />
      <div style={{ display: 'none' }}>
        <Image.PreviewGroup preview={{ visible, onVisibleChange: vis => setVisible(vis) }}>
          {elements}
        </Image.PreviewGroup>
      </div>
    </>
  );
};

export default UserProfileAvatar;