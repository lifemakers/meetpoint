import React, { Component } from "react";
import { withTheme, withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
      '& .MuiTextField-root': {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: '100%',
      },
    },
    btnGroup: {
        display: 'flex',
        justifyContent: 'flex-end'
    }
  });

class UserProfileForm extends Component {
    constructor(props) {
        super(props);

        const {username, firstName, lastName, email, phone} = props.userInfo;
        this.state = { username, firstName, lastName, email, phone };
    }

    onChangeFirstName = (e) => this.setState({ firstName: e.target.value });
    onChangeLastName = (e) => this.setState({ lastName: e.target.value });
    onChangeEmail = (e) => this.setState({ email: e.target.value });
    onChangePhone = (e) => this.setState({ phone: e.target.value });

    onSubmit = (e) => {
        e.preventDefault();

        const {onSubmitUpdateUserInfo} = this.props;
        const {firstName, lastName, email, phone} = this.state;
        const userToUpdate = {firstName, lastName, email, phone};
        onSubmitUpdateUserInfo(userToUpdate);
    }

    render() {
        const {username, firstName, lastName, email, phone} = this.state;
        const {classes} = this.props;

        return (
            <ValidatorForm 
                        ref="form"
                        onSubmit={this.onSubmit}
                        onError={errors => console.log(errors)}
                        className={classes.root}
                        autoComplete="off">
                <div>
                    <TextValidator
                            disabled
                            id="username"
                            name="username"
                            label="username"
                            value={username}
                            variant="outlined"
                    />
                </div>
                <div>
                    <TextValidator
                            id="firstName"
                            name="firstName"
                            label="First name"
                            onChange={this.onChangeFirstName}
                            value={firstName}
                            variant="outlined"
                            validators={
                                ['required',
                                 'minStringLength:2',
                                 'maxStringLength:16']
                            }
                            errorMessages={
                                ['this field is required',
                                 'First name is too short. Minimum length is 2',
                                 'First name is too long. Maximum length is 16']}
                    />
                </div>
                <div>
                    <TextValidator
                            id="lastName"
                            name="lastName"
                            label="Last name"
                            onChange={this.onChangeLastName}
                            value={lastName}
                            variant="outlined"
                            validators={
                                ['required',
                                 'minStringLength:2',
                                 'maxStringLength:16']
                            }
                            errorMessages={
                                ['this field is required',
                                 'Last name is too short. Minimum length is 2',
                                 'Last name is too long. Maximum length is 16']}
                    />
                </div>
                <div>
                    <TextValidator
                            id="phone"
                            name="phone"
                            label="Phone"
                            onChange={this.onChangePhone}
                            value={phone}
                            variant="outlined"
                            validators={
                                ['required',
                                 'matchRegexp:^[+]?[0-9]{8,14}$']
                            }
                            errorMessages={
                                ['this field is required',
                                 'phone is not valid']}
                    />
                </div>
                <div>
                    <TextValidator
                            id="email"
                            name="email"
                            label="Email"
                            onChange={this.onChangeEmail}
                            value={email}
                            variant="outlined"
                            validators={['required', 'isEmail']}
                            errorMessages={['this field is required', 'email is not valid']}
                    />
                </div>
                <div className={classes.btnGroup}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary">
                        Update
                    </Button>
                </div>
            </ValidatorForm>
        );
    }
}

export default withTheme(withStyles(styles)(UserProfileForm));