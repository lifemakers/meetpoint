import { Component } from "react";
import Typography from '@material-ui/core/Typography';
import './user-profile-header.scss';

export default class UserProfileHeader extends Component {
    render() {
        const {firstName, lastName} = this.props;

        return (
            <div className="user-profile-header">
                <Typography variant="h2">
                    {firstName} {lastName}
                </Typography>
                <Typography variant="caption" display="block" gutterBottom>
                    Here, you can update you data. Be sure, all your data is used with all data privacy rules followed.
                </Typography>
            </div>
        );
    };
}