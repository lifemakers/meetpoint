import { Component } from 'react';
import { Result } from 'antd';

export default class ErrorPage extends Component { 
    render() {
        const {errorCode} = this.props;

        let commonError = (
            <Result className="error-page" status="500" title="500" subTitle="Sorry, something went wrong." />
        )

        //let invalidEmailOrPasswordError = (
        //    <Result
        //        className="error-page"
        //        status="error"
        //        title="Invalid email or Password!"
        //        subTitle="Please, verify email and password and try again"
        //        extra={[
        //            <NavLink key="signin" href="/signin"><Button className="error-page__btn">Try Again</Button> </NavLink>
        //        ]} />
        //);

        switch (errorCode) {
            default:
                return commonError;
        };
    }
}