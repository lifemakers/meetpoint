import { Component } from "react";
import EventCreateForm from '../../components/event-create-form';
import Grid from '@material-ui/core/Grid';
import EventService from '../../services/eventService';
import EventServiceFake from '../../services/fakeServices/eventServiceFake';
import ErrorPage from '../../pages/error-page';
import Spinner from '../../components/spinner';
import Paper from '@material-ui/core/Paper';
import { withTheme, withStyles } from '@material-ui/core/styles';

const paperElevation = 2;
const styles = (theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(1),
    },
    paper: {
        margin: theme.spacing(1),
        padding: theme.spacing(2),
        paddingBottom: theme.spacing(3),
        textAlign: "center",
        color: theme.palette.text.secondary
    }
});
class EventCreatePage extends Component {
    constructor(props) {
        super(props);

        this.eventService = new EventServiceFake();
        this.state = {
            loading: false,
            error: false
        }
    }

    onSubmitCreateForm = (data) => {
        this.setState({ loading: true });

        this.eventService.createEvent(data)
            .then(this.onSuccess)
            .catch(this.onError);
    }

    onSuccess = () => this.setState({ loading: false });
    onError = (error) => { this.setState({ error: true, loading: false }) };

    componentDidCatch() {
        this.setState({ error: true, loading: false });
    }

    render() {
        const {loading, error} = this.state;
        const {classes} = this.props;

        if (error) return <ErrorPage />;
        if (loading) return <Spinner />;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item container xs={12}>
                            <Grid item xs={2}></Grid>
                            <Grid item xs={8}>
                                <EventCreateForm onSubmitCreateForm={this.onSubmitCreateForm} />
                            </Grid>
                            <Grid item xs={2}></Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }
}

export default withTheme(withStyles(styles)(EventCreatePage));