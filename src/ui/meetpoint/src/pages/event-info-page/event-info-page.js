import { Component } from "react";
import EventInfo from '../../components/event-info';
import Grid from '@material-ui/core/Grid';
import EventInfoHeader from "../../components/event-info-header";
import EventInfoCarousel from "../../components/event-info-carousel";
import EventParticipationList from "../../components/event-participation-list";
import EventTagList from "../../components/event-tag-list";
// import EventService from '../../services/eventService';
import EventServiceFake from '../../services/fakeServices/eventServiceFake';
import ErrorPage from '../../pages/error-page';
import Spinner from '../../components/spinner';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { withTheme, withStyles } from '@material-ui/core/styles';

const paperElevation = 2;
const styles = (theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(1),
    },
    paper: {
      margin: theme.spacing(1),
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    }
});
class EventInfoPage extends Component {
    constructor(props) {
        super(props);

        this.eventService = new EventServiceFake();
        this.state = {
            eventInfo: {},
            eventParticipations: [],
            loading: true,
            error: false
        }
    }

    onMultipleDataLoaded = (values) => this.setState({ eventInfo: values[0], eventParticipations: values[1], loading: false });

    onEventInfoLoaded = (eventInfo) => {
        this.setState({
            eventInfo: eventInfo,
            loading: false
        });
    }

    onSuccess = () => this.setState({ loading: false });
    onError = (error) => { this.setState({ error: true, loading: false })};

    componentDidMount() {
        const {id} = this.props;

        Promise.all([
            this.eventService.getEventInfo(id),
            this.eventService.getEventParticipations(id)])
        .then(this.onMultipleDataLoaded)
        .catch(this.onError);
    }

    componentDidCatch() {
        this.setState({ error: true, loading: false });
    }

    render() {
        const {title, description, tags, participantsCount, beginDate, endDate, carouselItems} = this.state.eventInfo;
        const {loading, error, eventParticipations} = this.state;
        const {classes} = this.props

        if(error) return <ErrorPage/>;
        if(loading) return <Spinner/>;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid item container xs={12}>
                        <EventInfoHeader title={title} subtitle={description} />
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid item container xs={12}>
                        <EventTagList items={tags} />
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid item container xs={12}>
                        <EventInfoCarousel items={carouselItems} />
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid item container xs={12}>
                        <EventInfo
                            beginDate={beginDate}
                            endDate={endDate}
                            participantsCount={participantsCount} />
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid item container xs={12}>
                        <Grid item xs={5}>
                            <Grid item xs={12}>
                                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                                    Comments
                                </Typography>
                            </Grid>
                            <Grid item xs={12} />
                        </Grid>
                        <Grid item xs={2}/>
                        <Grid item container xs={5}>
                                <Grid item xs={12}>
                                    <Typography variant="h5" align="center" color="textSecondary" paragraph>
                                        Participants
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <EventParticipationList items={eventParticipations} />
                                </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }
}

export default withTheme(withStyles(styles)(EventInfoPage));