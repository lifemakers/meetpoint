import { Component } from "react";
import EventList from '../../components/event-list';
import Grid from '@material-ui/core/Grid';
import EventService from '../../services/eventService';
import EventServiceFake from '../../services/fakeServices/eventServiceFake';
import ErrorPage from '../../pages/error-page';
import Spinner from '../../components/spinner';

export default class EventListPage extends Component {
    constructor(props) {
        super(props);

        this.eventService = new EventServiceFake();
        this.state = {
            events: [],
            loading: true,
            error: false
        }
    }

    onEventsLoaded = (events) => {
        this.setState({ 
            events: events,
            loading: false
        });
    }

    onSuccess = () => this.setState({ loading: false });
    onError = (error) => { this.setState({ error: true, loading: false })};

    componentDidMount() {
        this.eventService.getAllEvents()
            .then(this.onEventsLoaded)
            .catch(this.onError);        
    }

    componentDidCatch() {
        this.setState({ error: true, loading: false });
    }

    render() {
        const {events, loading, error} = this.state;

        if(error) return <ErrorPage/>;
        if(loading) return <Spinner/>;

        return (
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <EventList events={events} />
                </Grid>
            </Grid>
        )
    }
}