import { Component } from "react";
import Grid from '@material-ui/core/Grid';
import AppHeader from '../../components/app-header';
import AppFooter from '../../components/app-footer';
import Paper from '@material-ui/core/Paper';
import { withTheme, withStyles } from '@material-ui/core/styles';

const paperElevation = 2;
const styles = (theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(1),
    },
    paper: {
        margin: theme.spacing(1),
        padding: theme.spacing(2),
        paddingBottom: theme.spacing(3),
        textAlign: "center",
        color: theme.palette.text.secondary
    }
});
class HomePage extends Component {
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <AppHeader />
                        </Grid>
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <AppFooter />
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }
}

export default withTheme(withStyles(styles)(HomePage));