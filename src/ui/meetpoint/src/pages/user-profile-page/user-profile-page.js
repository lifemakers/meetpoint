import React, { Component } from "react";
import Paper from '@material-ui/core/Paper';
import UserProfileAvatar from '../../components/user-profile-avatar';
import UserProfileForm from '../../components/user-profile-form';
import UserProfileHeader from '../../components/user-profile-header';
import UserParticipationList from '../../components/user-participation-list';
import Grid from "@material-ui/core/Grid";
import { withTheme, withStyles } from '@material-ui/core/styles';
import UserService from '../../services/userService';
import ParticipationService from '../../services/participationService';
import UserServiceFake from '../../services/fakeServices/userServiceFake';
import ParticipationServiceFake from '../../services/fakeServices/participationServiceFake';
import ErrorPage from '../../pages/error-page';
import Spinner from '../../components/spinner';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(1),
  },
  paper: {
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
});
const paperElevation = 2;
class UserProfilePage extends Component {
    constructor(props) {
        super(props);

        this.userId = "fgstvghyreytegver";
        this.userService = new UserServiceFake();
        this.participationService = new ParticipationServiceFake();
        this.state = {
            userInfo: {},
            userParticipations: [],
            loading: true,
            error: false,
        }
    }

    onMultipleDataLoaded = (values) => this.setState({ userInfo: values[0], userParticipations: values[1], loading: false });
    
    onSuccess = () => this.setState({ loading: false }); 
    onError = (error) => { this.setState({ error: true, loading: false })};
    
    onSubmitUpdateUserInfo = (data) => {
        this.setState({loading: true});

        this.userService.updateUserInfo(data)
            .then(this.onSuccess)
            .catch(this.onError);
    }
    
    onRemoveParticipation = (id) => {
        this.setState({loading: true});

        console.log(`participation with id: ${id} has been removed`);
        this.participationService.removeParticipation(id)
            .then(this.onSuccess)
            .catch(this.onError);
    }
    
    componentDidMount() {
            Promise.all([
                this.userService.getUserInfo(this.userId),
                this.userService.getUserParticipations(this.userId)])
            .then(this.onMultipleDataLoaded)
            .catch(this.onError);
    }
    
    componentDidCatch() {
        this.setState({ error: true, loading: false });
    }

    render() {
        const {classes} = this.props;
        const {userInfo, userParticipations, loading, error} = this.state;

        if(error) return <ErrorPage/>;
        if(loading) return <Spinner/>;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={4}>
                            <UserProfileHeader firstName={userInfo.firstName} lastName={userInfo.lastName} />
                        </Grid>
                        <Grid item xs={4}>
                            <UserProfileAvatar images={userInfo.images} />
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <UserProfileForm userInfo={userInfo} onSubmitUpdateUserInfo={this.onSubmitUpdateUserInfo}/>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>
                </Paper>
                <Paper className={classes.paper} elevation={paperElevation}>
                    <Grid container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <UserParticipationList items={userParticipations} onRemoveParticipation={this.onRemoveParticipation}/>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

export default withTheme(withStyles(styles)(UserProfilePage));