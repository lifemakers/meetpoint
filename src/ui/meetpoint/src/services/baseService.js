export default class BaseService {
    constructor() {
        this._apiBase = 'https://localhost:5001/api/v1';
    }

    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}/${url}`);
    
        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` +
            `, received ${res.status}`);
        }

        return await res.json();
    }

    postResource = async (url, data) => {
        let headers = { 'Content-Type': 'application/json' };
        
        const res = await fetch(`${this._apiBase}/${url}`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        });

        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` 
          + `, received ${res.status}`);
        }

        return await res;
    }

    putResource = async (url, data) => {
        let headers = { 'Content-Type': 'application/json' };
        
        const res = await fetch(`${this._apiBase}/${url}`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(data)
        });

        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` 
          + `, received ${res.status}`);
        }

        return await res;
    }

    deleteResource = async (url) => {
        const res = await fetch(`${this._apiBase}/${url}`, {
            method: 'PUT'
        });
    
        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` +
            `, received ${res.status}`);
        }

        return await res.json();
    }
}