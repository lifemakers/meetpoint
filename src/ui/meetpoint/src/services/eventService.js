import BaseService from "./baseService";

export default class EventService extends BaseService {
    getAllEvents = async () => await this.getResource(`events`);

    getEventInfo = async (id) => this._transformEvent(await this.getResource(`events/${id}`));

    createEvent = async (event) => await this.postResource("events", event);

    getEventParticipations = async (id) => this._transformEventParticipations(await this.getResource(`events/${id}/participations`));

    _isSet(data) {
        if (data) {
            return data
        } else {
            return 'no data...'
        }
    }

    _transformEvents(events = []) {
        return events.map(item => {
            return {
                id: item.id,
                title: this._isSet(item.title),
                beginDate: this._isSet(item.beginDate),
                tags: item.tags,
                previewImageUrl: item.previewImageUrl
            }
        });
    }

    _transformEvent(event) {
        return {
            id: event.id,
            title: this._isSet(event.title),
            description: this._isSet(event.description),
            beginDate: this._isSet(event.beginDate),
            endDate: this._isSet(event.endDate),
            tags: event.tags,
            images: this._transformImages(event.images)
        }
    }

    _transformImages(images = []) {
        return images.map(item => {
            return {
                src: item.url,
                key: item.id,
                caption: ""
            }
        })
    }

    _transformEventParticipations(participations = []) {
        return participations.map(item => {
            return {
                id: item.id,
                userId: item.userId,
                firstName: this._isSet(item.firstName),
                lastName: this._isSet(item.lastName),
                eventId: item.eventId,
                datePosted: this._isSet(item.datePosted),
                participationRole: item.participationRole,
                previewImageUrl: item.previewImageUrl
            }
        })
    }
}