export default class EventServiceFake {
    delay = 300;

    getAllEvents = async () => await new Promise(resolve => { setTimeout(() => { resolve(this.fakeEvents()); }, this.delay) });

    getEventInfo = async (id) => await new Promise(resolve => { setTimeout(() => { resolve(this.fakeEvent()); }, this.delay) });

    createEvent = async (event) => await new Promise(resolve => { setTimeout(() => { resolve(); }, this.delay) });

    getEventParticipations = async (id) => await new Promise(resolve => { setTimeout(() => { resolve(this.fakeParticipations()); }, this.delay) });

    fakeImages() {
        return [
            { 
                src: "https://gw.alipayobjects.com/zos/antfincdn/LlvErxo8H9/photo-1503185912284-5271ff81b9a8.webp",
                key: 1,
                caption: ""
            },
            { 
                src: "https://gw.alipayobjects.com/zos/antfincdn/cV16ZqzMjW/photo-1473091540282-9b846e7965e3.webp",
                key: 2,
                caption: ""
            },                
            { 
                src: "https://gw.alipayobjects.com/zos/antfincdn/x43I27A55%26/photo-1438109491414-7198515b166b.webp",
                key: 3,
                caption: ""
            },
        ]
    }

    fakeEvent() {
        return {
            id: "gbieubgerg",
            title: "Meet friends in the PUB tonight",
            description: "Meet old friends to drink a beer and vodka! Location: pub Verona, dress code is not restricted. You can wear whatever you want! See you mate!",
            participantsCount: 6,
            beginDate: "2020-11-23 19:00",
            endDate: "2020-11-24 06:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
            carouselItems: [
                {
                  caption: "",
                  src: 'https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg',
                  key: '1'
                },
                {
                  caption: "",
                  src: 'https://gw.alipayobjects.com/zos/antfincdn/cV16ZqzMjW/photo-1473091540282-9b846e7965e3.webp',
                  key: '2'
                },
                {
                  caption: "",
                  src: 'https://gw.alipayobjects.com/zos/antfincdn/x43I27A55%26/photo-1438109491414-7198515b166b.webp',
                  key: '3'
                }
              ]
        }
    }

    fakeEvents() {
        return [{
            id: "gbieubgerg",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        },
        {
            id: "gbirytrbn",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        },
        {
            id: "gbirytree",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        },
        {
            id: "gbirytrgg",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        },
        {
            id: "gbieeeee",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        },
        {
            id: "gbiruyuyuyuy",
            title: "Pub with friends",
            beginDate: "2020-11-23 19:00",
            tags: ["pub", "beer", "friends", "old friends", "day off", "hang out"],
            previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
        }
        ];
    }

    fakeParticipations() {
        return [
            {
                id: "miofdgndfong",
                userId: "gmidofgjr",
                firstName: "Ivan",
                lastName: "Ternovyi",
                eventId: "rtwgnwiouth",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://www.film.ru/sites/default/files/filefield_paths/shutterstock_9669042a.jpg",
                participationRole: "user"
            },
            {
                id: "miofdgtrg",
                userId: "gmihghfgjr",
                firstName: "Ivan",
                lastName: "Ternovyi",
                eventId: "rt54jyth",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/09/27/08/jennifer-lawrence.jpg?width=982&height=726&auto=webp&quality=75",
                participationRole: "user"
            },
            {
                id: "mio5gg",
                userId: "gmidtreu7jr",
                firstName: "Ivan",
                lastName: "Ternovyi",
                eventId: "rtryuth",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://i0.wp.com/post.healthline.com/wp-content/uploads/2021/02/Female_Portrait_1296x728-header-1296x729.jpg?w=1155&h=2268",
                participationRole: "user"
            },
            {
                id: "miofdgrdhdrudbdrong",
                userId: "gmghhfjftygjr",
                eventId: "r5hrftyiouth",
                firstName: "Ivan",
                lastName: "Ternovyi",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2015/06/06/15/Chris-Pratt.jpg?width=982&height=726&auto=webp&quality=75",
                participationRole: "user"
            },
            {
                id: "tertebmltuong",
                userId: "tryukuil",
                firstName: "Ivan",
                lastName: "Ternovyi",
                eventId: "gfdgdgdfbdf",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://api.time.com/wp-content/uploads/2014/05/166259035.jpg?w=824&quality=70",
                participationRole: "user"
            },
            {
                id: "terhrtjktyituong",
                userId: "tjrytjtykl,il",
                firstName: "Ivan",
                lastName: "Ternovyi",
                eventId: "gffsejuyldfbdf",
                datePosted: "2020-11-03 19:34",
                previewImageUrl: "https://m.spletnik.ru/img/2020/12/20201202-gomez-post.jpg",
                participationRole: "user"
            },
        ]
    }
}