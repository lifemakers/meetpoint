export default class ParticipationServiceFake {
    delay = 300;

    addParticipation = async (data) => await new Promise(resolve => { setTimeout(() => { resolve(); }, this.delay) });
    
    removeParticipation = async (id) => await new Promise(resolve => { setTimeout(() => { resolve(); }, this.delay) });
}