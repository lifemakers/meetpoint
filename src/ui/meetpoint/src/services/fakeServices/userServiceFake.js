export default class UserServiceFake {
    delay = 300;

    getUserInfo = async (id) => await new Promise(resolve => { setTimeout(() => { resolve(this.fakeUserInfo()); }, this.delay) });

    getUserParticipations = async (id) => await new Promise(resolve => { setTimeout(() => { resolve(this.fakeUserParticipations()); }, this.delay) });

    updateUserInfo = async (id, data) => await new Promise(resolve => { setTimeout(() => { resolve(); }, this.delay) });

    fakeUserInfo() {
        return {
            id: "gmeoirhytoen",
            username: "ternovyi",
            firstName: "Ivan",
            lastName: "Ternovyi",
            email: "bo43333@gmail.com",
            phone: "+380992439465",
            images: this.fakeUserImages()
        };
    }

    fakeUserImages() {
        return [
            {
                id: "gmosngosi",
                src: "https://gw.alipayobjects.com/zos/antfincdn/LlvErxo8H9/photo-1503185912284-5271ff81b9a8.webp"
            },
            {
                id: "reyhrery",
                src: "https://gw.alipayobjects.com/zos/antfincdn/cV16ZqzMjW/photo-1473091540282-9b846e7965e3.webp"
            },
            {
                id: "tryemmpy",
                src: "https://gw.alipayobjects.com/zos/antfincdn/x43I27A55%26/photo-1438109491414-7198515b166b.webp"
            }
        ]
    }

    fakeUserParticipations() {
        return [
            {
                id: "miofdgndfong",
                userId: "gmidofgjr",
                eventId: "rtwgnwiouth",
                eventName: "Friends Monday Pub",
                eventBeginDate: "2021-12-23 12:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
            {
                id: "miofdgtrg",
                userId: "gmihghfgjr",
                eventId: "rt54jyth",
                eventName: "Zoo",
                eventBeginDate: "2021-04-22 22:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
            {
                id: "mio5gg",
                userId: "gmidtreu7jr",
                eventId: "rtryuth",
                eventName: "Restaraunt with friends",
                eventBeginDate: "2021-07-01 18:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
            {
                id: "miofdgrdhdrudbdrong",
                userId: "gmghhfjftygjr",
                eventId: "r5hrftyiouth",
                eventName: "Coffee with colleagues",
                eventBeginDate: "2021-11-12 16:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
            {
                id: "tertebmltuong",
                userId: "tryukuil",
                eventId: "gfdgdgdfbdf",
                eventName: "Birthday",
                eventBeginDate: "2021-12-15 20:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
            {
                id: "terhrtjktyituong",
                userId: "tjrytjtykl,il",
                eventId: "gffsejuyldfbdf",
                eventName: "Family meeting",
                eventBeginDate: "2021-12-01 18:00",
                previewImageUrl: "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg",
                participationRole: "user"
            },
        ]
    }
}