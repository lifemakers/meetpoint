import BaseService from "./baseService";

export default class ParticipationService extends BaseService {
    addParticipation = async (data) => await this.postResource(`participations`, data); 
    
    removeParticipation = async (id) => await this.deleteResource(`participations/${id}`); 
}