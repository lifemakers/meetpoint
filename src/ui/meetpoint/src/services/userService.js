import BaseService from "./baseService";

export default class UserService extends BaseService {
    getUserInfo = async (id) => this._transformUser(await this.getResource(`users/${id}`));

    updateUserInfo = async (id, data) => await this.putResource(`users/${id}`, data);

    getUserParticipations = async (id) => this._transformParticipations(await this.getResource(`users/${id}/participations`));

    _isSet(data) {
        if (data) {
            return data
        } else {
            return 'no data...'
        }
    }

    _transformUser(user) {
        return {
            id: user.id,
            username: this._isSet(user.username),
            firstName: this._isSet(user.firstName),
            lastName: this._isSet(user.lastName),
            email: this._isSet(user.email),
            phone: this._isSet(user.phone),
            images: this._transformUserImages(user.userImages)
        }
    }

    _transformUserImages(images = []) {
        return images.map(item => {
            return {
                src: item.url,
                id: item.imageId
            }
        })
    }

    _transformUserParticipations(participations = []) {
        return participations.map(item => {
            return {
                id: item.id,
                userId: item.userId,
                eventId: item.eventId,
                eventName: item.eventName,
                eventBeginDate: item.eventBeginDate,
                participationRole: item.participationRole,
                previewImageUrl: item.previewImageUrl
            }
        })
    }
}