$ErrorActionPreference = "Stop"

$rootThumbprint = "633703eb267ba64f40a0e3d1f43be689825401f9"

$certCNs = "", ""
$certPath = "./email.pfx"

# retrieve root cert by thumbprint
$alreadyExistingCertsRoot = Get-ChildItem -Path Cert:\LocalMachine\Root -Recurse | Where-Object {$_.Thumbprint -eq "$rootThumbprint"}

# check if root cert exists
if ($alreadyExistingCertsRoot.Count -eq 1) {
    Write-Output "RootCA certificate has been found."
    $rootCACert = [Microsoft.CertificateServices.Commands.Certificate] $alreadyExistingCertsRoot[0]
} else {
    Write-Error "RootCA certificate has not been found."
    Exit
}

$cert = New-SelfSignedCertificate -DnsName $certCNs -Signer $rootCACert -CertStoreLocation Cert:\LocalMachine\My

# Export it for docker container to pick up later.
$password = ConvertTo-SecureString -String "password" -Force -AsPlainText

Export-PfxCertificate -Cert $cert -FilePath "$certPath" -Password $password | Out-Null